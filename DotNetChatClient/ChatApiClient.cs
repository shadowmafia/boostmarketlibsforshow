﻿using System;
using NetCoreChatClient.Settings;

namespace NetCoreChatClient
{
    public class ChatApiClient
    {
        private readonly ChatApiClientSettings _settings;

        public ChatApiClient(ChatApiClientSettings settings)
        {
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        //public async Task<IActionResult> CreateOrUpdateChannel(ChatChannel channel)
        //{
        //    await _chatService.CreateOrUpdateChannel(channel);

        //    return Ok();
        //}

        //public async Task<IActionResult> AddMemberToChannel(string channelId, ChatMember member)
        //{
        //    await _chatService.AddMemberToChannel(channelId, member);

        //    return Ok();
        //}

        //public async Task<IActionResult> RemoveMemberFromChannel(string channelId, ChatMember member)
        //{
        //    await _chatService.RemoveMemberFromChannel(channelId, member);

        //    return Ok();
        //}
    }

}
