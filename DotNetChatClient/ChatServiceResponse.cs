﻿namespace NetCoreChatClient
{
    public class ChatServiceResponse<T>
    {
        public T Data { get; set; }
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
    }

    public enum ResponseStatus
    {
        Success = 200,
        BadRequest = 400,
        Unauthorized = 401,
        InitialServerError = 500,
    }
}
