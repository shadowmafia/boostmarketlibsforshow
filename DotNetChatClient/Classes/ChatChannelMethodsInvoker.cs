﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using NetCoreChatClient.Models;
using NetCoreChatClient.Models.Types;
using NetCoreChatClient.Settings;

namespace NetCoreChatClient.Classes
{
    public class ChatChannelMethodsInvoker
    {
        private readonly HubConnection _connection;
        private readonly ChatHubClientSettings _settings;

        public ChatChannelMethodsInvoker(HubConnection connection,ChatHubClientSettings settings)
        {
            _connection = connection ?? throw  new ArgumentNullException(nameof(connection));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public async Task<ChatUserChannelsModel> GetMyChannelsInfo(int? channelsLoadingCount = null)
        {
            if (channelsLoadingCount > _settings.MaxTakeChannelsCount)
            {
                throw new ArgumentOutOfRangeException(nameof(_settings.MaxTakeMessagesCount));
            }

            ThrowExceptionIfClientNotConnected();

            return await _connection.InvokeCoreAsync<ChatUserChannelsModel>("GetMyChannelsInfo", new object[] { channelsLoadingCount });
        }

        public async Task<List<ChatChannelModel>> GetMyChannelsInfoByType(ChatChannelType type, DateTime? elderThen = null, int? count = null)
        {
            if (count > _settings.MaxTakeChannelsCount)
            {
                throw new ArgumentOutOfRangeException(nameof(_settings.MaxTakeMessagesCount));
            }

            ThrowExceptionIfClientNotConnected();

            return await _connection.InvokeCoreAsync<List<ChatChannelModel>>(" GetMyChannelsInfoByType", new object[] { type, elderThen, count });
        }

        public async Task<ChatChannelModel> GetPrivateChannelWithMember(ChatMember receiver)
        {
            if (string.IsNullOrEmpty(receiver.MemberId))
            {
                throw new ArgumentNullException(nameof(receiver.MemberId));
            }

            ThrowExceptionIfClientNotConnected();

            return await _connection.InvokeCoreAsync<ChatChannelModel>("GetPrivateChannelWithMember", new object[] { receiver });
        }

        public async Task<ChatChannelModel> GetChannelInfo(string channelId)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            ThrowExceptionIfClientNotConnected();

            return await _connection.InvokeCoreAsync<ChatChannelModel>("GetChannelInfo", new object[] { channelId });
        }

        public async Task<ChatChannelModel> CreatePrivateChannelWithMember(ChatMember receiver)
        {
            if (string.IsNullOrEmpty(receiver.MemberId))
            {
                throw new ArgumentNullException(nameof(receiver.MemberId));
            }

            ThrowExceptionIfClientNotConnected();

            return await _connection.InvokeCoreAsync<ChatChannelModel>("CreatePrivateChannelWithMember", new object[] { receiver });
        }

        public async Task JoinToChannel(string channelId)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            ThrowExceptionIfClientNotConnected();

            await _connection.InvokeCoreAsync("JoinToChannel", new object[] { channelId });
        }

        public async Task LeaveFromChannel(string channelId)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            ThrowExceptionIfClientNotConnected();

            await _connection.InvokeCoreAsync("LeaveFromChannel", new object[] { channelId });
        }

        #region private methods

        private void ThrowExceptionIfClientNotConnected()
        {
            if (_connection.State != HubConnectionState.Connected)
            {
                throw new Exception("Client not connected. You can't call hub endpoints methods when client not connected!");
            }
        }

        #endregion
    }
}
