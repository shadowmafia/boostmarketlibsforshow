﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using NetCoreChatClient.Models;
using NetCoreChatClient.Settings;

namespace NetCoreChatClient.Classes
{
    public class ChatMessagesMethodsInvoker
    {
        private readonly HubConnection _connection;
        private readonly ChatHubClientSettings _settings;

        public ChatMessagesMethodsInvoker(HubConnection connection, ChatHubClientSettings settings)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public async Task<List<ChatMessageModel>> GetMessagesFromChannel(string channelId, DateTime? elderThen = null, int? count = null)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (count > _settings.MaxTakeMessagesCount)
            {
                throw new ArgumentOutOfRangeException(nameof(_settings.MaxTakeMessagesCount));
            }

            ThrowExceptionIfClientNotConnected();

            return await _connection.InvokeCoreAsync<List<ChatMessageModel>>("GetMessagesFromChannel", new object[] { channelId, elderThen , count });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="messageMarker">You can create unique indifier for message then you can find out the sent message in callBack event</param>
        /// <param name="content"></param>
        /// <param name="attachedImgUrl"></param>
        /// <returns></returns>
        public async Task SendMessage(string channelId, string messageMarker, string content, string attachedImgUrl)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (content.Length > _settings.MaxContentLength || content.Length < _settings.MinContentLength)
            {
                throw new ArgumentOutOfRangeException(nameof(_settings.MaxTakeMessagesCount));
            }

            ThrowExceptionIfClientNotConnected();

            await _connection.InvokeCoreAsync("SendMessage", new object[] { channelId, messageMarker, content, attachedImgUrl });
        }

        private void ThrowExceptionIfClientNotConnected()
        {
            if (_connection.State != HubConnectionState.Connected)
            {
                throw new Exception("Client not connected. You can't call hub endpoints methods when client not connected!");
            }
        }
    }
}
