﻿namespace NetCoreChatClient.Settings
{
    public class ChatHubClientSettings
    {
        public string AuthToken { get; set; }
        public string HubUrl { get; set; }
        public int MaxTakeChannelsCount { get; set; }
        public int MaxTakeMessagesCount { get; set; }
        public int MaxContentLength { get; set; }
        public int MinContentLength { get; set; }
    }
}
