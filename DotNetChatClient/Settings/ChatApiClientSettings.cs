﻿namespace NetCoreChatClient.Settings
{
    public class ChatApiClientSettings
    {
        public string AuthToken { get; set; }
        public string ApiUrl { get; set; }
    }
}
