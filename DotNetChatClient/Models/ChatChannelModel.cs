﻿using System.Collections.Generic;
using NetCoreChatClient.Models.Types;

namespace NetCoreChatClient.Models
{
    public class ChatChannelModel
    {
        public string Id { get; set; }
        public string LinkedObjectId { get; set; }
        public ChatChannelType ChannelType { get; set; }
        public List<ChatMemberModel> ChannelMembers { get; set; }
        public ChatMessageModel LastMessage { get; set; }
        public long UnreadMessagesCount { get; set; }
    }
}
