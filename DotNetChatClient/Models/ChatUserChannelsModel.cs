﻿using System.Collections.Generic;

namespace NetCoreChatClient.Models
{
    public class ChatUserChannelsModel
    {
        public List<ChatChannelModel> Channels { get; set; }
        public long TotalUnreadMessagesCount { get; set; }
    }
}
