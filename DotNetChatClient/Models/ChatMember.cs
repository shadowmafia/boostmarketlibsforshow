﻿using System;
using System.Collections.Generic;
using System.Text;
using NetCoreChatClient.Models.Types;

namespace NetCoreChatClient.Models
{
    public class ChatMember
    {
        public string MemberId { get; set; }
        public ChatMemberType MemberType { get; set; }
    }
}