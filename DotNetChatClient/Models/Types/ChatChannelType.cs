﻿namespace NetCoreChatClient.Models.Types
{
    public enum ChatChannelType
    {
        Private,
        Order,
        All,
        Lfg
    }
}
