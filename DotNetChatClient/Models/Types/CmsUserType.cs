﻿namespace NetCoreChatClient.Models.Types
{
    public enum CmsUserType
    {
        Booster,
        Manager,
        Admin,
        SuperAdmin
    }
}