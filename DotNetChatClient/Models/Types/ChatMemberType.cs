﻿namespace NetCoreChatClient.Models.Types
{
    public enum ChatMemberType
    {
        CmsUser,
        Customer
    }
}
