﻿namespace NetCoreChatClient.Models.Types
{
    public enum DeviceType
    {
        Web,
        Desktop,
        Mobile
    }
}
