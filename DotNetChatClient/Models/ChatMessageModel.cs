﻿using System;

namespace NetCoreChatClient.Models
{
    public class ChatMessageModel
    {
        public string Id { get; set; }
        public string ChannelId { get; set; }
        public ChatMemberModel Sender { get; set; }
        public string Content { get; set; }
        public DateTime? Timestamp { get; set; }
        public string AttachedImageUrl { get; set; }
        public string MessageMarker { get; set; }
    }
}
