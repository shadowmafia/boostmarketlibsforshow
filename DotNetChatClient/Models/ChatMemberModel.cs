﻿using NetCoreChatClient.Models.Types;

namespace NetCoreChatClient.Models
{
    public class ChatMemberModel
    {
        public string MemberId { get; set; }
        public ChatMemberType MemberType { get; set; }
        public CmsUserType? CmsUserType { get; set; }
        public string NickName { get; set; }
        public string AvatarUrl { get; set; }
    }
}
