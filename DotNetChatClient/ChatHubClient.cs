﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using NetCoreChatClient.Models;
using NetCoreChatClient.Models.Types;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using NetCoreChatClient.Classes;
using NetCoreChatClient.Settings;

namespace NetCoreChatClient
{
    public class ChatHubClient
    {
        public event Action<ChatMessageModel> OnMessageReceive;
        public event Action<OnlineUserModel> OnUserConnected;
        public event Action<List<OnlineUserModel>> OnUsersDisconnected;

        public event Action<string> OnReconnected;
        public event Action<Exception> OnReconnecting;
        public event Action<Exception> OnConnectionClosed;

        public readonly ChatChannelMethodsInvoker Channels;
        public readonly ChatMessagesMethodsInvoker Messages;
        
        private readonly HubConnection _connection;
        private readonly ChatHubClientSettings _settings;

        public ChatHubClient(ChatHubClientSettings settings)
        {
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
            
            _connection = new HubConnectionBuilder()
                .WithAutomaticReconnect().WithUrl(_settings.HubUrl, options =>
                {
                    options.Headers = new AttributeDictionary()
                    {
                        {"Authorization", $"Bearer {_settings.AuthToken}"},
                    };
                })
                .Build();

            _connection.On<ChatMessageModel>("ReceiveMessage", (message) =>
            {
                OnMessageReceive?.Invoke(message);
            });

            _connection.On<OnlineUserModel>("UserConnected", (user) =>
            {
                OnUserConnected?.Invoke(user);
            });

            _connection.On<List<OnlineUserModel>>("UsersDisconnected", (users) =>
            {
                OnUsersDisconnected?.Invoke(users);
            });

            _connection.Closed += ConnectionClosed;
            _connection.Reconnecting += Reconnecting;
            _connection.Reconnected += Reconnected;

            Channels = new ChatChannelMethodsInvoker(_connection, _settings);
            Messages = new ChatMessagesMethodsInvoker(_connection, _settings);
        }

        public async Task Start()
        {
            await _connection.StartAsync();
        }

        public async Task Stop()
        {
            await _connection.StopAsync();
        }

        public async Task<List<OnlineUserModel>> GetOnlineUsers()
        {
            return await _connection.InvokeCoreAsync<List<OnlineUserModel>>("GetOnlineUsers", new object[]{});
        }

        #region private methods
        private async Task Reconnected(string arg)
        {
            OnReconnected?.Invoke(arg);
        }

        private async Task Reconnecting(Exception arg)
        {
            OnReconnecting?.Invoke(arg);
        }

        private async Task ConnectionClosed(Exception arg)
        {
            OnConnectionClosed?.Invoke(arg);
        }

        #endregion
    }
}
