﻿using Nest;
using System;

namespace DataAccessLayerImplementation
{
    public abstract class ElasticsearchClientContainer
    {
        protected const string _elasticInnerIdFieldName = "_id";
        protected const int _elasticMaxTakeValue = 10000;
        protected readonly ElasticClient _client;
        protected readonly string _indexName;

        protected ElasticsearchClientContainer(ElasticClient client, string indexName)
        {
            if (string.IsNullOrEmpty(indexName))
            {
                throw new ArgumentNullException(nameof(indexName));
            }

            _client = client ?? throw new ArgumentNullException(nameof(client));
            _indexName = indexName;
        }
    }
}