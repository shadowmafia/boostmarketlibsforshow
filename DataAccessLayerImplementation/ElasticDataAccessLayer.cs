﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.ChatData;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.GameData;
using DataAccessLayerAbstraction.GameData.Mapping;
using DataAccessLayerAbstraction.MarketData;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerImplementation._ElasticIndexSettings;
using Elasticsearch.Net;
using Nest;
using System;

namespace DataAccessLayerImplementation
{
    public class ElasticDataAccessLayer : IDataAccessLayer
    {
        public IMarketData MarketData { get; }
        public IMarketSystemData MarketSystemData { get; }
        public IGameData GameData { get; }
        public IChatData ChatData { get; }

        public ElasticDataAccessLayer(ElasticDataAccessLayerSettings settings)
        {
            CheckSetting(settings);

            var uri = new Uri(settings.ElasticsearchNode);
            var connectionPool = new SingleNodeConnectionPool(uri, DateTimeProvider.Default);
            var connectionSettings = new ConnectionSettings(connectionPool);
            connectionSettings.BasicAuthentication(settings.Login, settings.Password);
            connectionSettings.RequestTimeout(TimeSpan.FromMilliseconds(settings.TimeOut));
            connectionSettings.DefaultIndex(settings.IndexesSettings.DefaultIndexName);

#if DEBUG
            connectionSettings.DisableDirectStreaming();
#endif

            SetDefaultMapping(connectionSettings, settings.IndexesSettings);

            var client = new ElasticClient(connectionSettings);

            MarketData = new MarketData.MarketData(client, settings.IndexesSettings.MarketDataIndexSettings);
            MarketSystemData = new MarketSystemData.MarketSystemData(client, settings.IndexesSettings.MarketSystemDataIndexSettings);
            GameData = new GameData.GameData(client, settings.IndexesSettings.GameDataIndexSettings);
            ChatData = new ChatData.ChatData(client, settings.IndexesSettings.ChatDataIndexSettings);
        }

        #region Mapping

        private static void SetDefaultMapping(ConnectionSettings connectionSettings, IndexesSettings indexesSettings)
        {
            SetChatDataMapping(connectionSettings, indexesSettings.ChatDataIndexSettings);
            SetGameDataMapping(connectionSettings, indexesSettings.GameDataIndexSettings);
            SetMarketDataMapping(connectionSettings, indexesSettings.MarketDataIndexSettings);
            SetMarketSystemDataMapping(connectionSettings, indexesSettings.MarketSystemDataIndexSettings);
        }

        private static void SetChatDataMapping(ConnectionSettings connectionSettings, ChatDataIndexSettings settings)
        {
            connectionSettings.DefaultMappingFor<ChatChannel>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.ChatChannelIndexName)
            );

            connectionSettings.DefaultMappingFor<ChatMessage>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName)
            );
        }

        private static void SetMarketSystemDataMapping(ConnectionSettings connectionSettings, MarketSystemDataIndexSettings settings)
        {
            connectionSettings.DefaultMappingFor<Transaction>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.TransactionIndexName)
            );

            connectionSettings.DefaultMappingFor<ActionLog>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.ActionLogIndexName)
            );

            connectionSettings.DefaultMappingFor<MarketLanguage>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.MarketLanguageIndexName)
            );

            connectionSettings.DefaultMappingFor<LanguageSticker>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.LanguageStickerIndexName)
            );
        }

        private static void SetGameDataMapping(ConnectionSettings connectionSettings, GameDataIndexSettings settings)
        {
            connectionSettings.DefaultMappingFor<BoosterGameAnalytics>(m => m
                .IdProperty(p => p.BoosterId)
                .Ignore(p => p.BoosterId)
                .IndexName(settings.BoosterGameAnalyticsIndexName)
            );

            connectionSettings.DefaultMappingFor<BoosterMatch>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.BoosterMatchIndexName)
            );

            connectionSettings.DefaultMappingFor<OrderMatchHistory>(m => m
                .IdProperty(p => p.OrderId)
                .Ignore(p => p.OrderId)
                .IndexName(settings.OrderMatchHistoryIndexName)
            );
        }

        private static void SetMarketDataMapping(ConnectionSettings connectionSettings, MarketDataIndexSettings settings)
        {
            connectionSettings.DefaultMappingFor<CmsUser>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.CmsUserIndexName)
            );

            connectionSettings.DefaultMappingFor<BoosterProfile>(m => m
                .IdProperty(p => p.CmsUserId)
                .Ignore(p => p.CmsUserId)
                .IndexName(settings.BoosterProfileIndexName)
            );

            connectionSettings.DefaultMappingFor<Customer>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.CustomerIndexName)
            );

            connectionSettings.DefaultMappingFor<Order>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.Id)
                .IndexName(settings.OrderIndexName)
                .Ignore(p => p.Details)
            );

            connectionSettings.DefaultMappingFor<OrderFeedback>(m => m
                .IdProperty(p => p.OrderId)
                .Ignore(p => p.OrderId)
                .IndexName(settings.OrderFeedbackIndexName)
            );
        }

        #endregion

        #region CheckSettings

        private static void CheckSetting(ElasticDataAccessLayerSettings settings)
        {
            CheckRootSettings(settings);
            CheckChatDataIndexSettings(settings.IndexesSettings.ChatDataIndexSettings);
            CheckGameDataIndexSettings(settings.IndexesSettings.GameDataIndexSettings);
            CheckMarketDataSettings(settings.IndexesSettings.MarketDataIndexSettings);
            CheckSystemDataIndexSettings(settings.IndexesSettings.MarketSystemDataIndexSettings);
        }

        private static void CheckChatDataIndexSettings(ChatDataIndexSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (string.IsNullOrEmpty(settings.ChatChannelIndexName))
            {
                throw new ArgumentNullException(nameof(settings.ChatChannelIndexName));
            }

            if (string.IsNullOrEmpty(settings.ChatMessageIndexSettings.ChatMessageIndexName))
            {
                throw new ArgumentNullException(nameof(settings.ChatMessageIndexSettings.ChatMessageIndexName));
            }

            if (settings.ChatMessageIndexSettings.DurationOfStorageActualMessages < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(settings.ChatMessageIndexSettings.DurationOfStorageActualMessages)} cant be less than 1");
            }
        }

        private static void CheckSystemDataIndexSettings(MarketSystemDataIndexSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (string.IsNullOrEmpty(settings.ActionLogIndexName))
            {
                throw new ArgumentNullException(nameof(settings.ActionLogIndexName));
            }

            if (string.IsNullOrEmpty(settings.LanguageStickerIndexName))
            {
                throw new ArgumentNullException(nameof(settings.LanguageStickerIndexName));
            }

            if (string.IsNullOrEmpty(settings.MarketLanguageIndexName))
            {
                throw new ArgumentNullException(nameof(settings.MarketLanguageIndexName));
            }

            if (string.IsNullOrEmpty(settings.TransactionIndexName))
            {
                throw new ArgumentNullException(nameof(settings.TransactionIndexName));
            }
        }

        private static void CheckRootSettings(ElasticDataAccessLayerSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (string.IsNullOrEmpty(settings.ElasticsearchNode))
            {
                throw new ArgumentNullException(nameof(settings.ElasticsearchNode));
            }

            if (string.IsNullOrEmpty(settings.Login))
            {
                throw new ArgumentNullException(nameof(settings.Login));
            }

            if (string.IsNullOrEmpty(settings.Password))
            {
                throw new ArgumentNullException(nameof(settings.Password));
            }

            if (settings.TimeOut <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(settings.TimeOut));
            }

            if (settings.IndexesSettings == null)
            {
                throw new ArgumentNullException(nameof(settings.IndexesSettings));
            }

            if (string.IsNullOrEmpty(settings.IndexesSettings.DefaultIndexName))
            {
                throw new ArgumentNullException(nameof(settings.IndexesSettings.DefaultIndexName));
            }
        }

        private static void CheckMarketDataSettings(MarketDataIndexSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (string.IsNullOrEmpty(settings.BoosterProfileIndexName))
            {
                throw new ArgumentNullException(nameof(settings.BoosterProfileIndexName));
            }

            if (string.IsNullOrEmpty(settings.CmsUserIndexName))
            {
                throw new ArgumentNullException(nameof(settings.CmsUserIndexName));
            }

            if (string.IsNullOrEmpty(settings.CustomerIndexName))
            {
                throw new ArgumentNullException(nameof(settings.CustomerIndexName));
            }

            if (string.IsNullOrEmpty(settings.OrderFeedbackIndexName))
            {
                throw new ArgumentNullException(nameof(settings.OrderFeedbackIndexName));
            }

            if (string.IsNullOrEmpty(settings.OrderIndexName))
            {
                throw new ArgumentNullException(nameof(settings.OrderIndexName));
            }

            if (string.IsNullOrEmpty(settings.OrderIndexName))
            {
                throw new ArgumentNullException(nameof(settings.OrderIndexName));
            }
        }

        private static void CheckGameDataIndexSettings(GameDataIndexSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (string.IsNullOrEmpty(settings.BoosterGameAnalyticsIndexName))
            {
                throw new ArgumentNullException(nameof(settings.BoosterGameAnalyticsIndexName));
            }

            if (string.IsNullOrEmpty(settings.BoosterMatchIndexName))
            {
                throw new ArgumentNullException(nameof(settings.BoosterMatchIndexName));
            }

            if (string.IsNullOrEmpty(settings.OrderMatchHistoryIndexName))
            {
                throw new ArgumentNullException(nameof(settings.OrderMatchHistoryIndexName));
            }
        }

        #endregion
    }
}