﻿using DataAccessLayerAbstraction.ChatData.DataManager;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerImplementation._ElasticIndexSettings;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace DataAccessLayerImplementation.ChatData.DataManagers
{
    public class ChatMessageDataManager : ElasticsearchEntityManagerBase<ChatMessage>, IChatMessageDataManager
    {
        private readonly string _minIndexNameForActualMessageStorage;
        private readonly ChatMessageIndexSettings _settings;

        public ChatMessageDataManager(ElasticClient client, ChatMessageIndexSettings settings) : base(client, GetIndexNameForCurrentMonth(settings?.ChatMessageIndexName))
        {
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
            DateTime date = DateTime.UtcNow.AddMonths(-settings.DurationOfStorageActualMessages);
            _minIndexNameForActualMessageStorage = $"{settings.ChatMessageIndexName}_{date.Year}_{date:MM}";
        }

        public async Task<string> AddOrUpdate(ChatMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (message.Sender == null)
            {
                throw new ArgumentNullException($"{nameof(message.Sender)} is null. Message cannot be with out Sender");
            }

            if (message.ChannelId == null)
            {
                throw new ArgumentNullException($"{nameof(message.ChannelId)} is null. Message cannot be with out channelID");
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexWithAddOrUpdateAliases();
            }

            if (message.Timestamp.HasValue == false)
            {
                message.Timestamp = DateTime.UtcNow;
            }

            return await IndexDocumentAsync(message);
        }

        public async Task<List<string>> AddOrUpdate(IList<ChatMessage> messages)
        {
            if (messages == null)
            {
                throw new ArgumentNullException(nameof(messages));
            }

            foreach (var item in messages)
            {
                if (item.Sender == null)
                {
                    throw new ArgumentNullException($"{nameof(item.Sender)} is null. Message cannot be with out Sender");
                }

                if (item.ChannelId == null)
                {
                    throw new ArgumentNullException($"{nameof(item.ChannelId)} is null. Message cannot be with out channelID");
                }

                if (item.Timestamp.HasValue == false)
                {
                    item.Timestamp = DateTime.UtcNow;
                }
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexWithAddOrUpdateAliases();
            }

            return await IndexManyDocumentsAsync(messages);
        }

        public async Task Delete(string messageId)

        {
            if (string.IsNullOrEmpty(messageId))
            {
                throw new ArgumentNullException(nameof(messageId));
            }

            await DeleteDocumentAsync(messageId);
        }

        #region private methods

        private static string GetIndexNameForCurrentMonth(string indexName)
        {
            return $"{indexName}_{DateTime.UtcNow.Year}_{DateTime.UtcNow:MM}";
        }

        private async Task CreateIndexWithAddOrUpdateAliases()
        {
            await CreateIndexAsync(ss => ss
                .Map(CreateMap())
            );

            if ((await _client.Indices.AliasExistsAsync(_settings.CurrentMonthMessagesAliasName)).Exists)
            {
                var indexesInCurrentMonth = _client.GetIndicesPointingToAlias(_settings.CurrentMonthMessagesAliasName);
                var bulkAliasDescriptor = new BulkAliasDescriptor();

                foreach (var indexName in indexesInCurrentMonth)
                {
                    bulkAliasDescriptor.Remove(a => a.Index(indexName).Alias(_settings.CurrentMonthMessagesAliasName));
                }

                await _client.Indices.BulkAliasAsync(bulkAliasDescriptor);
            }

            if ((await _client.Indices.AliasExistsAsync(_settings.ActualMessagesAliasName)).Exists)
            {
                var indexesLast3Month = _client.GetIndicesPointingToAlias(_settings.ActualMessagesAliasName);
                var bulkAliasDescriptor2 = new BulkAliasDescriptor();

                List<string> inexesToArchive = new List<string>();

                foreach (var index in indexesLast3Month)
                {
                    if (string.Compare(index, _minIndexNameForActualMessageStorage) < 0)
                    {
                        bulkAliasDescriptor2.Remove(a => a.Index(index).Alias(_settings.ActualMessagesAliasName));
                        inexesToArchive.Add(index);
                    }
                }

                await _client.Indices.BulkAliasAsync(bulkAliasDescriptor2);

                foreach (var index in inexesToArchive)
                {
                    await _client.Indices.PutAliasAsync(index, _settings.ArchiveMessagesAliasName);
                }
            }

            await _client.Indices.PutAliasAsync(_indexName, _settings.CurrentMonthMessagesAliasName);
            await _client.Indices.PutAliasAsync(_indexName, _settings.ActualMessagesAliasName);
        }

        private static Func<TypeMappingDescriptor<ChatMessage>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(p => p
                    .Keyword(
                        f => f.Name(n => n.ChannelId)
                    )
                );
        }

        #endregion
    }
}