﻿using DataAccessLayerAbstraction.ChatData.DataManager;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.ChatData.DataManagers
{
    public class ChatChannelDataManager : ElasticsearchEntityManagerBase<ChatChannel>, IChatChannelDataManager
    {
        public ChatChannelDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(ChatChannel channel)
        {
            if (channel == null)
            {
                throw new ArgumentNullException(nameof(channel));
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(s => s
                    .Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(channel);
        }

        public async Task DeleteChannel(string channelId)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            await DeleteDocumentAsync(channelId);
        }

        public async Task AddMember(string channelId, ChatMember member)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (member == null)
            {
                throw new ArgumentNullException(nameof(member));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            member.LastSeen = DateTime.UtcNow;

            await _client.UpdateAsync<ChatChannel>(channelId, i => i
               .Script(script => script
                   .Source(@"
                        for(int i=0; i <= ctx._source.channelMembers.size() - 1; i++){
                            if(ctx._source.channelMembers[i].memberId == params.chatMember.memberId && ctx._source.channelMembers[i].memberType == params.chatMember.memberType){
                               ctx._source.channelMembers.remove(i);
                               break;
                            }
                        }

                        ctx._source.channelMembers.add(params.chatMember);
                    ")
                   .Params(new Dictionary<string, object>()
                       {
                            {"chatMember", member}
                       }
                   )
               )
            );
        }

        public async Task RemoveMember(string channelId, ChatMember member)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (member == null)
            {
                throw new ArgumentNullException(nameof(member));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            await _client.UpdateAsync<ChatChannel>(channelId, i => i
                .Script(script => script
                    .Source(@"
                        for(int i=0; i <= ctx._source.channelMembers.size() - 1; i++){
                            if(ctx._source.channelMembers[i].memberId == params.chatMember.memberId && ctx._source.channelMembers[i].memberType == params.chatMember.memberType){
                               ctx._source.channelMembers.remove(i);
                               break;
                            }
                        }
                    ")
                    .Params(new Dictionary<string, object>()
                        {
                            {"chatMember", member}
                        }
                    )
                )
            );
        }

        public async Task UpdateLastSeen(string channelId, ChatMember member, DateTime? lastSeen = null)
        {
            if (member == null)
            {
                throw new ArgumentNullException(nameof(member));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            member.LastSeen = lastSeen ?? DateTime.UtcNow;

            await _client.UpdateByQueryAsync<ChatChannel>(s => s
                .Query(rq => rq.Term(_elasticInnerIdFieldName, channelId))
                .Script(script => script
                    .Source(@"for(int i=0; i <= ctx._source.channelMembers.size() - 1; i++){
                            if(ctx._source.channelMembers[i].memberId == params.chatMember.memberId && ctx._source.channelMembers[i].memberType == params.chatMember.memberType){
                               ctx._source.channelMembers[i].lastSeen = params.chatMember.lastSeen;
                               break;
                            }
                        }
                    ")
                    .Params(new Dictionary<string, object>()
                        {
                            {"chatMember", member}
                        }
                    )
                )
            );
        }

        public async Task UpdateLastMassage(string channelId, ChatMessage message)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (message.Timestamp.HasValue == false)
            {
                message.Timestamp = DateTime.UtcNow;
            }

            var x = await _client.UpdateByQueryAsync<ChatChannel>(s => s
                .Query(rq => rq.Term(_elasticInnerIdFieldName, channelId))
                .Script(script => script
                    .Source(@"ctx._source.lastMessage = params.message;")
                    .Params(new Dictionary<string, object>()
                        {
                            {"message", message}
                        }
                    )
                )
            );
        }

        #region private methods

        private static Func<TypeMappingDescriptor<ChatChannel>, ITypeMapping> CreateMap()
        {
            return map => map
                .AutoMap(maxRecursion: 3)
                .Properties(p => p
                    .Nested<ChatMember>(f => f
                        .Name(n => n.ChannelMembers)
                        .AutoMap()
                        .Properties(pp=>pp
                            .Keyword(kk=>kk
                                .Name(nn=>nn.MemberId)
                            )
                        )
                    )
                );
        }

        #endregion
    }
}