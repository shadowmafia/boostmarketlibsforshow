﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.ChatData;
using DataAccessLayerAbstraction.ChatData.DataManager;
using DataAccessLayerAbstraction.ChatData.DataSearchers;
using DataAccessLayerImplementation._ElasticIndexSettings;
using DataAccessLayerImplementation.ChatData.DataManagers;
using DataAccessLayerImplementation.ChatData.DataSearchers;
using Nest;
using System;

namespace DataAccessLayerImplementation.ChatData
{
    public class ChatData : IChatData
    {
        public TemplateDataContainer<IChatChannelDataManager, IChatChannelDataSearcher> Channels { get; }

        public TemplateDataContainer<IChatMessageDataManager, IChatMessagesDataSearcher> Messages { get; }

        public ChatData(ElasticClient client, ChatDataIndexSettings indexSettings)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            if (indexSettings == null)
            {
                throw new ArgumentNullException(nameof(indexSettings));
            }

            Channels = new TemplateDataContainer<IChatChannelDataManager, IChatChannelDataSearcher>(
                new ChatChannelDataManager(client, indexSettings.ChatChannelIndexName),
                new ChatChannelDataSearcher(client, indexSettings.ChatChannelIndexName)
            );

            Messages = new TemplateDataContainer<IChatMessageDataManager, IChatMessagesDataSearcher>(
                new ChatMessageDataManager(client, indexSettings.ChatMessageIndexSettings),
                new ChatMessageDataSearcher(client, indexSettings.ChatMessageIndexSettings)
            );
        }
    }
}