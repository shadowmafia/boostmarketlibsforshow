﻿using DataAccessLayerAbstraction.ChatData.DataSearchers;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.ChatData.DataSearchers
{
    public class ChatChannelDataSearcher : ElasticsearchClientContainer, IChatChannelDataSearcher
    {
        public ChatChannelDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<ChatChannel> GetChannel(string channelId)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            var searchResponse = await _client.SearchAsync<ChatChannel>(s => s
                .Size(1)
                .Query(q => q
                    .Terms(m => m
                        .Field(_elasticInnerIdFieldName)
                        .Terms(channelId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<ChatChannel> FindPrivateChannelBetweenUsers(ChatMember member1, ChatMember member2)
        {
            if (string.IsNullOrEmpty(member1.MemberId))
            {
                throw new ArgumentNullException(nameof(member1.MemberId));
            }

            if (string.IsNullOrEmpty(member2.MemberId))
            {
                throw new ArgumentNullException(nameof(member2.MemberId));
            }

            var searchResponse = await _client.SearchAsync<ChatChannel>(s => s
                .Size(1)
                .Query(q => q
                    .Term(t => t
                        .Field(f => f.ChannelType)
                        .Value(ChatChannelType.Private)
                    ) && q
                    .Nested(nested => nested
                        .Path(p => p.ChannelMembers)
                        .Query(nq => nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.FirstOrDefault().MemberId)
                                .Value(member1.MemberId)
                            ) && nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.FirstOrDefault().MemberType)
                                .Value(member1.MemberType)
                            )
                        )
                    ) && q
                    .Nested(nested => nested
                        .Path(p => p.ChannelMembers)
                        .Query(nq => nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.FirstOrDefault().MemberId)
                                .Value(member2.MemberId)
                            ) && nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.FirstOrDefault().MemberType)
                                .Value(member2.MemberType)
                            )
                        )
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<ChatChannel>> GetChannelsByMember(
            ChatMember member,
            IEnumerable<ChatChannelType> channelTypes = null,
            DateTime? elderThen = null,
            string linkedObjectId = null,
            int? skip = null,
            int? take = null)
        {
            if (member == null)
            {
                throw new ArgumentNullException(nameof(member));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<ChatChannel>(s => s
                .Skip(skip)
                .Size(take)
                .Sort(sort => sort.Descending(field => field.LastMessage.Timestamp))
                .Query(q => q
                    .Nested(nested => nested
                        .Path(p => p.ChannelMembers)
                        .Query(nq => nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.First().MemberType)
                                .Value(member.MemberType)
                            ) && nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.First().MemberId)
                                .Value(member.MemberId)
                            )
                        )
                    ) && q
                    .Terms(t => t
                        .Field(f => f.ChannelType)
                        .Terms(channelTypes)
                    ) && q
                    .Terms(t => t
                        .Field(f => f.LinkedObjectId)
                        .Terms(linkedObjectId)
                    ) && q
                    .DateRange(dr => dr
                        .Field(f => f.LastMessage.Timestamp)
                        .LessThan(elderThen)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<IList<ChatChannel>> GetChannelsSummary(ChatMember member, int? includingPrivateChannelsCount = null, int? includingOrderChannelsCount = null)
        {
            if (member == null)
            {
                throw new ArgumentNullException(nameof(member));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            const string allChannelMSearchName = "all_channel";
            const string lfgChannelMSearchName = "lfg_channel";
            const string privateChannelMSearchName = "private_channels";
            const string orderChannelMSearchName = "order_channels";

            var searchResponse = await _client.MultiSearchAsync(_indexName, ms => ms
                .Search<ChatChannel>(allChannelMSearchName, s => s
                    .Size(1)
                    .Query(q => q
                        .Nested(nested => nested
                            .Path(p => p.ChannelMembers)
                            .Query(nq => nq
                                 .Term(t => t
                                     .Field(f => f.ChannelMembers.First().MemberType)
                                     .Value(member.MemberType)
                                 ) && nq
                                 .Term(t => t
                                     .Field(f => f.ChannelMembers.First().MemberId)
                                     .Value(member.MemberId)
                                 )
                            )
                        ) && q
                        .Term(t => t
                            .Field(f => f.ChannelType)
                            .Value(ChatChannelType.All)
                        )
                    )
                )
                .Search<ChatChannel>(lfgChannelMSearchName, s => s
                    .Size(1)
                    .Query(q => q
                        .Nested(nested => nested
                            .Path(p => p.ChannelMembers)
                            .Query(nq => nq
                                .Term(t => t
                                    .Field(f => f.ChannelMembers.First().MemberType)
                                    .Value(member.MemberType)
                                ) && nq
                                .Term(t => t
                                    .Field(f => f.ChannelMembers.First().MemberId)
                                    .Value(member.MemberId)
                                )
                            )
                        ) && q
                        .Term(t => t
                            .Field(f => f.ChannelType)
                            .Value(ChatChannelType.Lfg)
                        )
                    )
                )
                .Search<ChatChannel>(privateChannelMSearchName, s => s
                    .Size(includingPrivateChannelsCount)
                    .Sort(sort => sort.Descending(field => field.LastMessage.Timestamp))
                    .Query(q => q
                        .Nested(nested => nested
                            .Path(p => p.ChannelMembers)
                            .Query(nq => nq
                                .Term(t => t
                                    .Field(f => f.ChannelMembers.First().MemberType)
                                    .Value(member.MemberType)
                                ) && nq
                                .Term(t => t
                                    .Field(f => f.ChannelMembers.First().MemberId)
                                    .Value(member.MemberId)
                                )
                            )
                        ) && q
                        .Term(t => t
                            .Field(f => f.ChannelType)
                            .Value(ChatChannelType.Private)
                        )
                    )
                )
                .Search<ChatChannel>(orderChannelMSearchName, s => s
                    .Size(includingOrderChannelsCount)
                    .Sort(sort => sort.Descending(field => field.LastMessage.Timestamp))
                    .Query(q => q
                        .Nested(nested => nested
                            .Path(p => p.ChannelMembers)
                            .Query(nq => nq
                                .Term(t => t
                                    .Field(f => f.ChannelMembers.First().MemberType)
                                    .Value(member.MemberType)
                                ) && nq
                                .Term(t => t
                                    .Field(f => f.ChannelMembers.First().MemberId)
                                    .Value(member.MemberId)
                                )
                            )
                        ) && q
                        .Term(t => t
                            .Field(f => f.ChannelType)
                            .Value(ChatChannelType.Order)
                        )
                    )
                )
            );

            List<ChatChannel> returnedChannels = includingPrivateChannelsCount.HasValue && includingOrderChannelsCount.HasValue
                ? new List<ChatChannel>(includingPrivateChannelsCount.Value + includingOrderChannelsCount.Value + 2)
                : new List<ChatChannel>();

            returnedChannels.AddRange(
                    searchResponse.GetResponse<ChatChannel>(allChannelMSearchName).Hits.Select(h =>
                    {
                        h.Source.Id = h.Id;
                        return h.Source;
                    }).ToList()
            );

            returnedChannels.AddRange(
                searchResponse.GetResponse<ChatChannel>(lfgChannelMSearchName).Hits.Select(h =>
                {
                    h.Source.Id = h.Id;
                    return h.Source;
                }).ToList()
            );

            returnedChannels.AddRange(
                searchResponse.GetResponse<ChatChannel>(privateChannelMSearchName).Hits.Select(h =>
                {
                    h.Source.Id = h.Id;
                    return h.Source;
                }).ToList()
            );

            returnedChannels.AddRange(
                searchResponse.GetResponse<ChatChannel>(orderChannelMSearchName).Hits.Select(h =>
                {
                    h.Source.Id = h.Id;
                    return h.Source;
                }).ToList()
            );

            return returnedChannels;
        }
    }
}