﻿using DataAccessLayerAbstraction.ChatData.DataSearchers;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerImplementation._ElasticIndexSettings;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.ChatData.DataSearchers
{
    public class ChatMessageDataSearcher : ElasticsearchClientContainer, IChatMessagesDataSearcher
    {
        private const string CountUnreadMessagesByChannelAgg = "count_unreaded_messages_agg";
        private readonly ChatMessageIndexSettings _settings;

        public ChatMessageDataSearcher(ElasticClient client, ChatMessageIndexSettings settings) : base(client, settings?.ActualMessagesAliasName)
        {
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public async Task<ChatMessage> GetMessageById(string messageId)
        {
            if (string.IsNullOrEmpty(messageId))
            {
                throw new ArgumentNullException(nameof(messageId));
            }

            var searchResponse = await _client.SearchAsync<ChatMessage>(s => s
                .Index(_settings.ActualMessagesAliasName)
                .Size(1)
                .Query(q => q
                    .Terms(m => m
                        .Field(_elasticInnerIdFieldName)
                        .Terms(messageId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<ChatMessage>> FindMessages(
            string channelId,
            ChatMember sender = null,
            string textSearchQuery = null,
            DateTime? minTimestamp = null,
            DateTime? maxTimestamp = null,
            DateTime? elderThen = null,
            int? skip = null,
            int? take = null)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            var searchResponse = await _client.SearchAsync<ChatMessage>(s => s
                .Index(_settings.ActualMessagesAliasName)
                .From(skip)
                .Size(take ?? _elasticMaxTakeValue - skip)
                .Sort(sort => sort.Descending(field => field.Timestamp))
                .Query(CreateBaseFindMessageQuery(channelId, sender, textSearchQuery, minTimestamp, maxTimestamp, elderThen)
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<long> FindMessagesCount(
            string channelId,
            ChatMember sender = null,
            string textSearchQuery = null,
            DateTime? elderThen = null,
            DateTime? minTimestamp = null,
            DateTime? maxTimestamp = null)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            var countResponse = await _client.CountAsync<ChatMessage>(s => s
                .Index(_settings.ActualMessagesAliasName)
                .Query(
                    CreateBaseFindMessageQuery(channelId, sender, textSearchQuery, minTimestamp, maxTimestamp, elderThen)
                )
            );

            return countResponse.Count;
        }

        public async Task<IList<(string chanelId, long unreadMessagesCount)>> GetUnreadMessagesInChannels(
            ChatMember chatMember,
            IEnumerable<string> channelIds = null,
            int? maxLastChannelIncluding = null)
        {
            if (chatMember == null)
            {
                throw new ArgumentNullException(nameof(chatMember));
            }

            if (string.IsNullOrEmpty(chatMember.MemberId))
            {
                throw new ArgumentNullException(nameof(chatMember.MemberId));
            }

            var searchMemberChannelsResponse = await _client.SearchAsync<ChatChannel>(s => s
                .Size(maxLastChannelIncluding ?? _elasticMaxTakeValue)
                .Source(sf => sf
                    .Includes(i => i
                        .Fields(
                            f => f.ChannelMembers,
                            f => _elasticInnerIdFieldName
                        )
                    )
                )
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(channelIds)
                    ) && q
                    .Nested(nested => nested
                        .Path(p => p.ChannelMembers)
                        .Query(nq => nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.First().MemberType)
                                .Value(chatMember.MemberType)
                            ) && nq
                            .Term(t => t
                                .Field(f => f.ChannelMembers.First().MemberId)
                                .Value(chatMember.MemberId)
                            )
                        )
                    )
                )
                .Sort(sort => sort
                    .Field(f => f
                        .Field(innerF => innerF.LastMessage.Timestamp)
                        .Order(SortOrder.Descending)
                    )
                )
            );

            var userChannels = searchMemberChannelsResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();

            if (userChannels.Count <= 0)
            {
                return new List<(string chanelId, long unreadMessagesCount)>();
            }

            var response = await _client.SearchAsync<ChatMessage>(s => s
                .Size(0)
                .Index(_settings.ActualMessagesAliasName)
                .Query(q => q
                    .Bool(b => b
                        .Should(GetUnreadMessagesInChannelsBigQuery(userChannels, chatMember))
                    )
                )
                .Aggregations(a => a
                    .Terms(CountUnreadMessagesByChannelAgg, ss => ss
                        .Field(f => f.ChannelId)
                    )
                )
            );

            var buckets = response.Aggregations.Terms(CountUnreadMessagesByChannelAgg).Buckets;
            var unreadMessages = new List<(string chanelId, long unreadMessagesCount)>(buckets.Count);

            foreach (var bucket in buckets)
            {
                unreadMessages.Add((chanelId: bucket.Key, unreadMessagesCount: bucket.DocCount ?? 0));
            }

            return unreadMessages;
        }

        #region private methods

        private static Func<QueryContainerDescriptor<ChatMessage>, QueryContainer> CreateBaseFindMessageQuery(
            string channelId,
            ChatMember sender,
            string textSearchQuery,
            DateTime? minTimestamp,
            DateTime? maxTimestamp,
            DateTime? elderThen)
        {
            return q => q
                   .Term(t => t
                       .Field(f => f.ChannelId)
                       .Value(channelId)
                   ) && q
                   .Term(t => t
                       .Field(f => f.Sender.MemberId)
                       .Value(sender?.MemberId)
                   ) && q
                   .Term(t => t
                       .Field(f => f.Sender.MemberType)
                       .Value(sender?.MemberType)
                   ) && q
                   .Match(m => m
                       .Field(f => f.Content)
                       .Query(textSearchQuery)
                   ) && q
                   .DateRange(d => d
                       .Field(f => f.Timestamp)
                       .GreaterThanOrEquals(minTimestamp)
                       .LessThanOrEquals(maxTimestamp)
                       .LessThan(elderThen)
                   );
        }

        private IEnumerable<Func<QueryContainerDescriptor<ChatMessage>, QueryContainer>> GetUnreadMessagesInChannelsBigQuery(List<ChatChannel> channels, ChatMember member)
        {
            foreach (var channel in channels)
            {
                foreach (var channelMember in channel.ChannelMembers)
                {
                    if (channelMember.MemberId == member.MemberId && channelMember.MemberType == member.MemberType)
                    {
                        yield return q =>
                            q.Bool(b => b
                                .Must(m => m
                                    .Term(t => t
                                        .Field(f => f.ChannelId)
                                        .Value(channel.Id)
                                    ) && m
                                    .DateRange(r => r
                                        .Field(f => f.Timestamp)
                                        .GreaterThan(channelMember.LastSeen))
                                )
                            );
                    }
                }
            }
        }

        #endregion
    }
}