﻿using DataAccessLayerAbstraction.MarketSystemData.DataManagers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataManagers
{
    public class LanguageStickerDataManager : ElasticsearchEntityManagerBase<LanguageSticker>, ILanguageStickerDataManager
    {
        public LanguageStickerDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {
        }

        public async Task<string> AddOrUpdateLanguageSticker(LanguageSticker languageSticker)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                    .Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(languageSticker);
        }

        public async Task DeleteLanguageSticker(string stickerId)
        {
            await DeleteDocumentAsync(stickerId);
        }

        private static Func<TypeMappingDescriptor<LanguageSticker>, ITypeMapping> CreateMap()
        {
            return map => map
                .AutoMap()
                .Properties(props => props
                    .Keyword(text => text
                        .Name(name => name.LanguageId)
                    )
                    .Keyword(text => text
                        .Name(name => name.StickerName)
                    )
                );
        }
    }
}
