﻿using DataAccessLayerAbstraction.MarketSystemData.DataManagers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataManagers
{
    public class ActionLogDataManager : ElasticsearchEntityManagerBase<ActionLog>, IActionLogDataManager
    {
        public ActionLogDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> CreateActionLog(ActionLog actionLog)
        {
            if (string.IsNullOrEmpty(actionLog.Id) == false)
            {
                throw new ArgumentOutOfRangeException(nameof(actionLog.Id) + "must be empy for create new log");
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(s => s
                    .Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(actionLog);
        }

        private static Func<TypeMappingDescriptor<ActionLog>, ITypeMapping> CreateMap()
        {
            return map => map
                .AutoMap()
                .Properties(props => props
                    .Object<LogInitiator>(obj => obj
                        .Name(name => name.Initiator)
                        .AutoMap(3)
                        .Properties(iprops => iprops
                            .Keyword(text => text
                                .Name(n => n.InitiatorId)
                            )
                        )
                    )
                    .Object<LogTarget>(obj => obj
                        .Name(name => name.Target)
                        .AutoMap(3)
                        .Properties(iprops => iprops
                            .Keyword(text => text
                                .Name(n => n.TargetId)
                            )
                        )
                    )
                    .Nested<Dictionary<string, HttpAction>>(nested => nested
                         .Name(name => name.Actions)
                         .AutoMap(3)
                         .Properties(aprops => aprops
                             .Binary(b => b
                                 .Name(name => name.Values.FirstOrDefault().Body)
                             )
                         )
                    )
                );
        }
    }
}