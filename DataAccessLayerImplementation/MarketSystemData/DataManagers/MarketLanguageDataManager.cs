﻿using DataAccessLayerAbstraction.MarketSystemData.DataManagers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataManagers
{
    public class MarketLanguageDataManager : ElasticsearchEntityManagerBase<MarketLanguage>, IMarketLanguageDataManager
    {
        public MarketLanguageDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {
        }

        public async Task AddOrUpdateMarketLanguage(MarketLanguage language)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(s => s
                    .Map(CreateMap())
                );
            }

            await IndexDocumentAsync(language);
        }

        public async Task DeleteMarketLanguage(string languageId)
        {
            await DeleteDocumentAsync(languageId);
        }

        private static Func<TypeMappingDescriptor<MarketLanguage>, ITypeMapping> CreateMap()
        {
            return map => map
                .AutoMap()
                .Properties(props => props
                    .Keyword(text => text
                        .Name(name => name.LanguageName)
                    )
                    .Keyword(text => text
                        .Name(name => name.Locale)
                    )
                );
        }
    }
}
