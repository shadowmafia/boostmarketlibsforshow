﻿using DataAccessLayerAbstraction.MarketSystemData.DataManagers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataManagers
{
    public class TransactionDataManager : ElasticsearchEntityManagerBase<Transaction>, ITransactionDataManager
    {
        public TransactionDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> CreateTransaction(Transaction transaction)
        {
            if (string.IsNullOrEmpty(transaction.Id) == false)
            {
                throw new ArgumentOutOfRangeException(nameof(transaction.Id) + "must be empy for create new log");
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(s => s
                    .Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(transaction);
        }

        private static Func<TypeMappingDescriptor<Transaction>, ITypeMapping> CreateMap()
        {
            return map => map
                .AutoMap()
                .Properties(props => props
                    .Keyword(text => text
                        .Name(name => name.BindedObjectId)
                    )
                );
        }
    }
}