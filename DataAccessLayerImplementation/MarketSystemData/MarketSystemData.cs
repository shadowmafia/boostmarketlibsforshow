﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.MarketSystemData;
using DataAccessLayerAbstraction.MarketSystemData.DataManagers;
using DataAccessLayerAbstraction.MarketSystemData.DataSearchers;
using DataAccessLayerImplementation._ElasticIndexSettings;
using DataAccessLayerImplementation.MarketSystemData.DataManagers;
using DataAccessLayerImplementation.MarketSystemData.DataSearchers;
using Nest;
using System;

namespace DataAccessLayerImplementation.MarketSystemData
{
    public class MarketSystemData : IMarketSystemData
    {
        public TemplateDataContainer<IActionLogDataManager, IActionLogDataSearcher> ActionLog { get; }
        public TemplateDataContainer<ITransactionDataManager, ITransactionDataSearcher> Transaction { get; }
        public TemplateDataContainer<IMarketLanguageDataManager, IMarketLanguageDataSearcher> MarketLanguage { get; }
        public TemplateDataContainer<ILanguageStickerDataManager, ILanguageStickerDataSearcher> LanguageSticker { get; }

        public MarketSystemData(ElasticClient client, MarketSystemDataIndexSettings indexSettings)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            if (indexSettings == null)
            {
                throw new ArgumentNullException(nameof(indexSettings));
            }

            ActionLog = new TemplateDataContainer<IActionLogDataManager, IActionLogDataSearcher>(
                new ActionLogDataManager(client, indexSettings.ActionLogIndexName),
                new ActionLogDataSearcher(client, indexSettings.ActionLogIndexName)
            );

            Transaction = new TemplateDataContainer<ITransactionDataManager, ITransactionDataSearcher>(
                new TransactionDataManager(client, indexSettings.TransactionIndexName),
                new TransactionDataSearcher(client, indexSettings.TransactionIndexName)
            );

            MarketLanguage = new TemplateDataContainer<IMarketLanguageDataManager, IMarketLanguageDataSearcher>(
                new MarketLanguageDataManager(client, indexSettings.MarketLanguageIndexName),
                new MarketLanguageDataSearcher(client, indexSettings.MarketLanguageIndexName)
            );

            LanguageSticker = new TemplateDataContainer<ILanguageStickerDataManager, ILanguageStickerDataSearcher>(
                new LanguageStickerDataManager(client, indexSettings.LanguageStickerIndexName),
                new LanguageStickerDataSearcher(client, indexSettings.LanguageStickerIndexName)
            );
        }
    }
}