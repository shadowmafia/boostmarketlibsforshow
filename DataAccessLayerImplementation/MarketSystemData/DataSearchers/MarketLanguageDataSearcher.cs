﻿using DataAccessLayerAbstraction.MarketSystemData.DataSearchers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataSearchers
{
    public class MarketLanguageDataSearcher : ElasticsearchClientContainer, IMarketLanguageDataSearcher
    {
        public MarketLanguageDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {
        }

        public async Task<MarketLanguage> GetMarketLanguage(
            string languageId = null,
            string locale = null,
            string languageName = null)
        {
            if (string.IsNullOrEmpty(languageId) && string.IsNullOrEmpty(locale) && string.IsNullOrEmpty(languageName))
            {
                throw new ArgumentOutOfRangeException("at least one of the parameters must be specified");
            }

            var searchResponse = await _client.SearchAsync<MarketLanguage>(search => search
                .Size(1)
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(languageId)
                    ) && q
                    .Terms(t => t
                        .Field(f => f.Locale)
                        .Terms(locale)
                    ) && q
                    .Terms(t => t
                        .Field(f => f.LanguageName)
                        .Terms(languageName)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<MarketLanguage>> GetAllLanguages()
        {
            var searchResponse = await _client.SearchAsync<MarketLanguage>();

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }
    }
}
