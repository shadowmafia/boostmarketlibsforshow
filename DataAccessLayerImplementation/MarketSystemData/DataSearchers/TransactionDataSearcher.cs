﻿using DataAccessLayerAbstraction.MarketSystemData.DataSearchers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.TransactionClasses;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataSearchers
{
    public class TransactionDataSearcher : ElasticsearchClientContainer, ITransactionDataSearcher
    {
        public TransactionDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {
        }

        public async Task<Transaction> GetTransaction(string transactionId)
        {
            if (string.IsNullOrEmpty(transactionId))
            {
                throw new ArgumentNullException(nameof(transactionId));
            }

            var searchResponse = await _client.SearchAsync<Transaction>(search => search
                .Size(1)
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(transactionId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<Transaction>> FindTransactions(
            string bindedObjectId = null,
            string transactionCommentContain = null,
            decimal? minValue = null,
            decimal? maxValue = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<TransactionType> transactionTypes = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<Transaction>(search => search
                .Skip(skip)
                .Size(take)
                .Query(
                    CreateBaseFindTransactionQuery(
                        bindedObjectId,
                        transactionCommentContain,
                        minValue,
                        maxValue,
                        dateTimeFrom,
                        dateTimeTo,
                        transactionTypes
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindTransactionsCount(
            string bindedObjectId,
            string transactionCommentContain,
            decimal? minValue,
            decimal? maxValue,
            DateTime? dateTimeFrom,
            DateTime? dateTimeTo,
            IEnumerable<TransactionType> transactionTypes)
        {
            var countResponse = await _client.CountAsync<Transaction>(q => q
                .Query(
                    CreateBaseFindTransactionQuery(
                        bindedObjectId,
                        transactionCommentContain,
                        minValue,
                        maxValue,
                        dateTimeFrom,
                        dateTimeTo,
                        transactionTypes
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        private Func<QueryContainerDescriptor<Transaction>, QueryContainer> CreateBaseFindTransactionQuery(
            string bindedObjectId = null,
            string transactionCommentContain = null,
            decimal? minValue = null,
            decimal? maxValue = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<TransactionType> transactionTypes = null)
        {
            DateTime queryMinDateTime = dateTimeFrom ?? DateTime.MinValue;
            DateTime queryMaxDateTime = dateTimeTo ?? DateTime.MaxValue;

            return query =>
                query
                    .MatchPhrase(terms => terms
                        .Field(f => f.TransactionComment)
                        .Query(transactionCommentContain)
                    )
                && query
                    .Terms(terms => terms
                        .Field(f => f.BindedObjectId)
                        .Terms(bindedObjectId)
                    )
                && query
                    .Terms(terms => terms
                        .Field(f => f.TransactionType)
                        .Terms(transactionTypes)
                    )
                && query
                    .Range(range => range
                        .GreaterThanOrEquals(Convert.ToDouble(minValue))
                        .LessThanOrEquals(Convert.ToDouble(maxValue))
                    )
                && query
                    .DateRange(terms => terms
                        .Field(f => f.DateTime)
                        .GreaterThanOrEquals(queryMinDateTime)
                        .LessThanOrEquals(queryMaxDateTime)
                    );
        }
    }
}