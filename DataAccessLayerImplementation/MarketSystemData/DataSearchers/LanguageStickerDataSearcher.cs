﻿using DataAccessLayerAbstraction.MarketSystemData.DataSearchers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataSearchers
{
    public class LanguageStickerDataSearcher : ElasticsearchClientContainer, ILanguageStickerDataSearcher
    {
        public LanguageStickerDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {
        }

        public async Task<LanguageSticker> GetLanguageSticker(string stickerId)
        {
            if (string.IsNullOrEmpty(stickerId))
            {
                throw new ArgumentNullException(nameof(stickerId));
            }

            var searchResponse = await _client.SearchAsync<LanguageSticker>(search => search
                .Size(1)
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(stickerId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<LanguageSticker>> FindLanguageSticker(
            string languageId = null,
            string stickerName = null,
            string stickerContentContain = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<LanguageSticker>(search => search
                .Skip(skip)
                .Size(take)
                .Query(
                    CreateBaseFindLanguageStickerQuery(
                        languageId,
                        stickerName,
                        stickerContentContain
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindLanguageStickerCount(
            string languageId = null,
            string stickerName = null,
            string stickerContentContain = null)
        {
            var countResponse = await _client.CountAsync<LanguageSticker>(search => search
                .Query(
                    CreateBaseFindLanguageStickerQuery(
                        languageId,
                        stickerName,
                        stickerContentContain
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        private Func<QueryContainerDescriptor<LanguageSticker>, QueryContainer> CreateBaseFindLanguageStickerQuery(
            string languageId,
            string stickerName,
            string stickerContentContain)
        {
            return query => query
                .Terms(t => t
                    .Field(f => f.LanguageId)
                    .Terms(languageId)
                ) && query
                .Terms(t => t
                    .Field(f => f.StickerName)
                    .Terms(stickerName)
                ) && query
                .MatchPhrase(t => t
                    .Field(f => f.StickerContent)
                    .Query(stickerContentContain)
                );
        }
    }
}