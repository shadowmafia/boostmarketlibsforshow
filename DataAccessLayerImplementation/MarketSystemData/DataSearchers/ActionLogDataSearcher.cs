﻿using DataAccessLayerAbstraction.MarketSystemData.DataSearchers;
using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketSystemData.DataSearchers
{
    public class ActionLogDataSearcher : ElasticsearchClientContainer, IActionLogDataSearcher
    {
        public ActionLogDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<IList<ActionLog>> FindActionLogs(
            string actionInitiatorId = null,
            string actionTargetId = null,
            string messageContain = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<LogInitiator> actionInitiators = null,
            IEnumerable<LogActionType> actionTypes = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<ActionLog>(s => s
                .Skip(skip)
                .Size(take)
                .Query(
                    CreateBaseActionLogQuery(
                        actionInitiatorId,
                        actionTargetId,
                        messageContain,
                        dateTimeFrom,
                        dateTimeTo,
                        actionInitiators,
                        actionTypes
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindActionLogsCount(
            string actionInitiatorId = null,
            string actionTargetId = null,
            string messageContain = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<LogInitiator> actionInitiators = null,
            IEnumerable<LogActionType> actionTypes = null)
        {
            var countResponse = await _client.CountAsync<ActionLog>(q => q
                .Query(
                    CreateBaseActionLogQuery(
                        actionInitiatorId,
                        actionTargetId,
                        messageContain,
                        dateTimeFrom,
                        dateTimeTo,
                        actionInitiators,
                        actionTypes
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        #region private methods

        private Func<QueryContainerDescriptor<ActionLog>, QueryContainer> CreateBaseActionLogQuery(
            string actionInitiatorId,
            string actionTargetId,
            string messageContain,
            DateTime? dateTimeFrom,
            DateTime? dateTimeTo,
            IEnumerable<LogInitiator> actionInitiators,
            IEnumerable<LogActionType> actionTypes)
        {
            DateTime requestDateTimeFrom = dateTimeFrom ?? DateTime.MinValue;
            DateTime requestDateTimeTo = dateTimeTo ?? DateTime.MaxValue;

            return query => query
                .Terms(t => t
                    .Field(f => f.Initiator.InitiatorId)
                    .Terms(actionInitiatorId)
                );
            //&& query
            //  .Terms(t => t
            //      .Field(f => f.ActionTargetId)
            //      .Terms(actionTargetId)
            //  )
            //&& query
            //  .Terms(t => t
            //      .Field(f => f.ActionInitiator)
            //      .Terms(actionInitiators)
            //  )
            //&& query
            //  .Terms(t => t
            //      .Field(f => f.LogActionType)
            //      .Terms(actionTypes)
            //  )
            //&& query
            //  .MatchPhrase(m => m
            //      .Field(f => f.Message)
            //      .Query(messageContain)
            //  )
            //&& query
            //  .DateRange(d => d
            //      .Field(f => f.DateTime)
            //      .GreaterThanOrEquals(requestDateTimeFrom)
            //      .LessThanOrEquals(requestDateTimeTo)
            //  );
        }

        #endregion
    }
}