﻿using DataAccessLayerAbstraction.GameData.DataManagers;
using DataAccessLayerAbstraction.GameData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.GameData.DataManagers
{
    public class BoosterMatchDataManager : ElasticsearchEntityManagerBase<BoosterMatch>, IBoosterMatchDataManager
    {
        public BoosterMatchDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(BoosterMatch boosterMatch)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(cd => cd
                    .Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(boosterMatch);
        }

        public async Task DeleteBoosterMatch(string id)
        {
            await DeleteDocumentAsync(id);
        }

        private static Func<TypeMappingDescriptor<BoosterMatch>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(
                    p => p
                        .Keyword(
                            f => f.Name(n => n.BoosterId)
                        )
                        .Keyword(
                            f => f.Name(n => n.OrderId)
                        )
                );
        }
    }
}