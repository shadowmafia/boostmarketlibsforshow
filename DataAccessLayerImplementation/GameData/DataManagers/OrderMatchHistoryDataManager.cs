﻿using DataAccessLayerAbstraction.GameData.DataManagers;
using DataAccessLayerAbstraction.GameData.Mapping;
using Nest;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.GameData.DataManagers
{
    public class OrderMatchHistoryDataManager : ElasticsearchEntityManagerBase<OrderMatchHistory>, IOrderMatchHistoryDataManager
    {
        public OrderMatchHistoryDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(OrderMatchHistory orderMatchHistory)
        {
            return await IndexDocumentAsync(orderMatchHistory);
        }

        public async Task DeleteOrderMatchHistory(string id)
        {
            await DeleteDocumentAsync(id);
        }
    }
}