﻿using DataAccessLayerAbstraction.GameData.DataManagers;
using DataAccessLayerAbstraction.GameData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.GameData.DataManagers
{
    public class BoosterGameAnalyticsDataManager : ElasticsearchEntityManagerBase<BoosterGameAnalytics>, IBoosterGameAnalyticsDataManager
    {
        public BoosterGameAnalyticsDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(BoosterGameAnalytics gameAnalytics)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(s =>
                   s.Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(gameAnalytics);
        }

        public async Task DeleteBoosterGameAnalytics(string id)
        {
            await DeleteDocumentAsync(id);
        }

        private static Func<TypeMappingDescriptor<BoosterGameAnalytics>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(
                    p => p
                        .Keyword(
                            f => f.Name(n => n.ChampionStats.Keys)
                        )
                );
        }
    }
}