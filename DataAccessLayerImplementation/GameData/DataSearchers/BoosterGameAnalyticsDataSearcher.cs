﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.DataSearchers;
using DataAccessLayerAbstraction.GameData.Mapping;
using DataAccessLayerAbstraction.GameData.Mapping.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.GameData.DataSearchers
{
    internal class BoosterGameAnalyticsDataSearcher : ElasticsearchClientContainer, IBoosterGameAnalyticsDataSearcher
    {
        public BoosterGameAnalyticsDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<BoosterGameAnalytics> GetBoosterGameAnalytics(string boosterId)
        {
            if (string.IsNullOrEmpty(boosterId))
            {
                throw new ArgumentNullException(nameof(boosterId));
            }

            var searchResponse = await _client.SearchAsync<BoosterGameAnalytics>(s => s
                .Size(1)
                .Query(q => q
                   .Terms(t => t
                       .Field(_elasticInnerIdFieldName)
                       .Terms(boosterId)
                   )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.BoosterId = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<GamesStats>> GetBoosterGameStats(
            string boosterId,
            IEnumerable<string> includeChampionIds = null,
            IEnumerable<Tier> includeTiers = null)
        {
            if (string.IsNullOrEmpty(boosterId))
            {
                throw new ArgumentNullException(nameof(boosterId));
            }

            var searchResponse = await _client.SearchAsync<BoosterGameAnalytics>(s => s
                .Source(sf => sf
                    .Includes(i => i
                        .Fields(
                            BuildFieldIncludesForGetBoosterGameStats(includeChampionIds, includeTiers)
                        )
                    )
                )
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(boosterId)
                    )
                )
            );

#if DEBUG
            string queryString = System.Text.Encoding.UTF8.GetString(searchResponse.ApiCall.RequestBodyInBytes);
#endif

            List<GamesStats> gamesStats = new List<GamesStats>();

            var bga = searchResponse.Documents.FirstOrDefault();

            if (bga == null)
            {
                return gamesStats;
            }

            foreach (var stat in bga.ChampionStats)
            {
                gamesStats.Add(stat.Value);
            }

            foreach (var stat in bga.TierStats)
            {
                gamesStats.Add(stat.Value);
            }

            return gamesStats;
        }

        public async Task<IList<BoosterGameAnalytics>> FindBoosterGameAnalytics(
            IEnumerable<string> boosterIds = null,
            IEnumerable<string> haveChampionIds = null,
            IEnumerable<Tier> haveTiers = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (take.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<BoosterGameAnalytics>(s => s
                .Skip(skip)
                .Size(take)
                .Query(q => q
                    .Bool(b => b
                        .Must(
                            CreateBaseFindBoosterGameAnalyticsQuery(
                                boosterIds,
                                haveChampionIds,
                                haveTiers
                            )
                        )
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.BoosterId = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindBoosterGameAnalyticsCount(
            IEnumerable<string> boosterIds = null,
            IEnumerable<string> haveChampionIds = null,
            IEnumerable<Tier> haveTiers = null
        )
        {
            var countResponse = await _client.CountAsync<BoosterGameAnalytics>(s => s
                .Query(q => q
                    .Bool(b => b
                        .Must(
                            CreateBaseFindBoosterGameAnalyticsQuery(
                                boosterIds,
                                haveChampionIds,
                                haveTiers
                            )
                        )
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        #region private methods

        private string[] BuildFieldIncludesForGetBoosterGameStats(
            IEnumerable<string> championIds,
            IEnumerable<Tier> tiers)
        {
            List<string> fields = new List<string>();

            foreach (var item in tiers)
            {
                fields.Add($"tierStats.{((int)item).ToString()}");
            }

            foreach (var item in championIds)
            {
                fields.Add($"championStats.{item}");
            }

            return fields.ToArray();
        }

        private IEnumerable<Func<QueryContainerDescriptor<BoosterGameAnalytics>, QueryContainer>> CreateBaseFindBoosterGameAnalyticsQuery(
            IEnumerable<string> boosterIds,
            IEnumerable<string> championIds,
            IEnumerable<Tier> tiers)
        {
            if (championIds != null)
            {
                foreach (var id in championIds)
                {
                    yield return q => q.Exists(t => t.Field(f => f.ChampionStats.Suffix(id)));
                }
            }

            if (tiers != null)
            {
                foreach (var tier in tiers)
                {
                    yield return q => q.Exists(t => t.Field(f => f.TierStats.Suffix(((int)tier).ToString())));
                }
            }

            yield return q => q.Terms(t => t.Field(_elasticInnerIdFieldName).Terms(boosterIds));
        }

        #endregion
    }
}