﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.DataSearchers;
using DataAccessLayerAbstraction.GameData.Mapping;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.GameData.DataSearchers
{
    public class BoosterMatchDataSearcher : ElasticsearchClientContainer, IBoosterMatchDataSearcher
    {
        public BoosterMatchDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<BoosterMatch> GetBoosterMatch(string matchId)
        {
            if (string.IsNullOrEmpty(matchId))
            {
                throw new ArgumentNullException(nameof(matchId));
            }

            var searchResponse = await _client.SearchAsync<BoosterMatch>(s => s
                .Size(1)
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(matchId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<BoosterMatch>> FindBoosterMatches(
            string boosterId = null,
            string orderId = null,
            bool? isWin = null,
            DateTime? minGameCreation = null,
            DateTime? maxGameCreation = null,
            IEnumerable<long> championIds = null,
            IEnumerable<Tier> ties = null,
            IEnumerable<Lane> lanes = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<BoosterMatch>(s => s
                .Skip(skip)
                .Size(take)
                .Query(
                    CreateBaseBoosterMatchesQuery(
                        boosterId,
                        orderId,
                        isWin,
                        minGameCreation,
                        maxGameCreation,
                        championIds,
                        ties,
                        lanes
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindBoosterMatchesCount(
            string boosterId = null,
            string orderId = null,
            bool? isWin = null,
            DateTime? minGameCreation = null,
            DateTime? maxGameCreation = null,
            IEnumerable<long> championIds = null,
            IEnumerable<Tier> ties = null,
            IEnumerable<Lane> lanes = null)
        {
            var countResponse = await _client.CountAsync<BoosterMatch>(s => s
                .Query(
                    CreateBaseBoosterMatchesQuery(
                        boosterId,
                        orderId,
                        isWin,
                        minGameCreation,
                        maxGameCreation,
                        championIds,
                        ties,
                        lanes
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        private Func<QueryContainerDescriptor<BoosterMatch>, QueryContainer> CreateBaseBoosterMatchesQuery(
            string boosterId,
            string orderId,
            bool? isWin,
            DateTime? minGameCreation,
            DateTime? maxGameCreation,
            IEnumerable<long> championIds,
            IEnumerable<Tier> ties,
            IEnumerable<Lane> lanes)
        {
            return query =>
                query
                    .Terms(t => t
                        .Field(f => f.BoosterId)
                        .Terms(boosterId)
                    ) && query
                    .Terms(t => t
                        .Field(f => f.OrderId)
                        .Terms(orderId)
                    ) && query
                    .Terms(t => t
                        .Field(f => f.IsWin)
                        .Terms(isWin)
                    ) && query
                    .DateRange(t => t
                        .Field(f => f.GameCreation)
                        .GreaterThanOrEquals(minGameCreation)
                        .LessThanOrEquals(maxGameCreation)
                    ) && query
                    .Terms(t => t
                        .Field(f => f.ChampionId)
                        .Terms(championIds)
                    ) && query
                    .Terms(t => t
                        .Field(f => f.Tier)
                        .Terms(ties)
                    ) && query
                    .Terms(t => t
                        .Field(f => f.Lane)
                        .Terms(lanes)
                    );
        }
    }
}