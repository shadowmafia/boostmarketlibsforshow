﻿using DataAccessLayerAbstraction.GameData.DataSearchers;
using DataAccessLayerAbstraction.GameData.Mapping;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.GameData.DataSearchers
{
    public class OrderMatchHistoryDataSearcher : ElasticsearchClientContainer, IOrderMatchHistoryDataSearcher
    {
        public OrderMatchHistoryDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<OrderMatchHistory> GetOrderMatchHistory(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                throw new ArgumentNullException(nameof(orderId));
            }

            var searchResponse = await _client.SearchAsync<OrderMatchHistory>(s => s
                .Size(1)
                .Query(q => q
                    .Terms(m => m
                        .Field(_elasticInnerIdFieldName)
                        .Terms(orderId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.OrderId = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }
    }
}