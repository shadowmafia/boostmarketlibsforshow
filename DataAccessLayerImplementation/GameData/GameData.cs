﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.GameData;
using DataAccessLayerAbstraction.GameData.DataManagers;
using DataAccessLayerAbstraction.GameData.DataSearchers;
using DataAccessLayerImplementation._ElasticIndexSettings;
using DataAccessLayerImplementation.GameData.DataManagers;
using DataAccessLayerImplementation.GameData.DataSearchers;
using Nest;
using System;

namespace DataAccessLayerImplementation.GameData
{
    public class GameData : IGameData
    {
        public TemplateDataContainer<IBoosterGameAnalyticsDataManager, IBoosterGameAnalyticsDataSearcher> BoosterGameAnalytics { get; }
        public TemplateDataContainer<IOrderMatchHistoryDataManager, IOrderMatchHistoryDataSearcher> OrderMatchHistory { get; }
        public TemplateDataContainer<IBoosterMatchDataManager, IBoosterMatchDataSearcher> BoosterMatch { get; }

        public GameData(ElasticClient client, GameDataIndexSettings indexSettings)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            if (indexSettings == null)
            {
                throw new ArgumentNullException(nameof(indexSettings));
            }

            BoosterGameAnalytics = new TemplateDataContainer<IBoosterGameAnalyticsDataManager, IBoosterGameAnalyticsDataSearcher>(
                new BoosterGameAnalyticsDataManager(client, indexSettings.BoosterGameAnalyticsIndexName),
                new BoosterGameAnalyticsDataSearcher(client, indexSettings.BoosterGameAnalyticsIndexName)
            );

            BoosterMatch = new TemplateDataContainer<IBoosterMatchDataManager, IBoosterMatchDataSearcher>(
                new BoosterMatchDataManager(client, indexSettings.BoosterMatchIndexName),
                new BoosterMatchDataSearcher(client, indexSettings.BoosterMatchIndexName)
            );

            OrderMatchHistory = new TemplateDataContainer<IOrderMatchHistoryDataManager, IOrderMatchHistoryDataSearcher>(
                new OrderMatchHistoryDataManager(client, indexSettings.OrderMatchHistoryIndexName),
                new OrderMatchHistoryDataSearcher(client, indexSettings.OrderMatchHistoryIndexName)
            );
        }

    }
}