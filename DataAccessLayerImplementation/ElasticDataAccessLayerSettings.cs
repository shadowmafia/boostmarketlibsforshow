﻿using DataAccessLayerImplementation._ElasticIndexSettings;

namespace DataAccessLayerImplementation
{
    public class ElasticDataAccessLayerSettings
    {
        public string ElasticsearchNode { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int TimeOut { get; set; }
        public IndexesSettings IndexesSettings { get; set; }
    }
}