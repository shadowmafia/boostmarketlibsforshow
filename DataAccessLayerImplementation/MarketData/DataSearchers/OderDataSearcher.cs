﻿using DataAccessLayerAbstraction._CommonEnums.System;
using DataAccessLayerAbstraction.MarketData.DataSearchers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataSearchers
{
    public class OderDataSearcher : ElasticsearchClientContainer, IOderDataSearcher
    {
        public OderDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<Order> GetOrder(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                throw new ArgumentNullException(nameof(orderId));
            }

            var orderSearchResponse = await _client.SearchAsync<Order>(search => search
                .Size(1)
                .Query(q => q
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(orderId)
                    )
                )
            );

            Order resultOrder = orderSearchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();

            if (resultOrder != null)
            {
                DeserializeOrderDetails(resultOrder);
            }

            return resultOrder;
        }

        public async Task<IList<Order>> FindOrders(
            string boosterId = null,
            string customerId = null,
            string accountName = null,
            Region? region = null,
            OrderType? orderType = null,
            OrderStatus? orderStatus = null,
            PaymentState? paymentState = null,
            DateTime? minCheckoutTime = null,
            DateTime? maxCheckoutTime = null,
            DateTime? minCompletionTime = null,
            DateTime? maxCompletionTime = null,
            int? skip = null,
            int? take = null)
        {

            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var orderSearchResponse = await _client.SearchAsync<Order>(search => search
                .Skip(skip)
                .Size(take)
                .Query(
                    CreateBaseFindOrderQuery(
                        boosterId,
                        customerId,
                        accountName,
                        region,
                        orderType,
                        orderStatus,
                        paymentState,
                        minCheckoutTime,
                        maxCheckoutTime,
                        minCompletionTime,
                        maxCompletionTime
                    )
                )
            );

            return orderSearchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                DeserializeOrderDetails(h.Source);
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindOrdersCount(
            string boosterId = null,
            string customerId = null,
            string accountName = null,
            Region? region = null,
            OrderType? orderType = null,
            OrderStatus? orderStatus = null,
            PaymentState? paymentState = null,
            DateTime? minCheckoutTime = null,
            DateTime? maxCheckoutTime = null,
            DateTime? minCompletionTime = null,
            DateTime? maxCompletionTime = null)
        {
            var orderCountResponse = await _client.CountAsync<Order>(search => search
                .Query(
                    CreateBaseFindOrderQuery(
                        boosterId,
                        customerId,
                        accountName,
                        region,
                        orderType,
                        orderStatus,
                        paymentState,
                        minCheckoutTime,
                        maxCheckoutTime,
                        minCompletionTime,
                        maxCompletionTime
                    )
                )
            );

            return Convert.ToUInt32(orderCountResponse.Count);
        }

        #region private methods

        private Func<QueryContainerDescriptor<Order>, QueryContainer> CreateBaseFindOrderQuery(
          string boosterId,
          string customerId,
          string accountName,
          Region? region,
          OrderType? orderType,
          OrderStatus? orderStatus,
          PaymentState? paymentState,
          DateTime? minCheckoutTime,
          DateTime? maxCheckoutTime,
          DateTime? minCompletionTime,
          DateTime? maxCompletionTime)
        {
            DateTime queryMinCheckoutTime = minCheckoutTime ?? DateTime.MinValue;
            DateTime queryMaxCheckoutTime = maxCheckoutTime ?? DateTime.MaxValue;

            DateTime queryMinCompletionTime = minCompletionTime ?? DateTime.MinValue;
            DateTime queryMaxCompletionTime = maxCompletionTime ?? DateTime.MaxValue;

            return query =>
                query.Term(c => c
                    .Field(f => f.BoosterId)
                    .Value(boosterId)
                )
                &&
                query.Term(c => c
                    .Field(f => f.CustomerId)
                    .Value(customerId)
                )
                && query
                    .MatchPhrase(t => t
                        .Field(f => f.AccountName)
                        .Query(accountName)
                    )
                && query
                    .Term(f => f.Region, region)
                && query
                    .Term(f => f.Type, orderType)
                && query
                    .Term(f => f.Status, orderStatus)
                && query
                    .Term(f => f.PaymentState, paymentState)
                && query
                    .DateRange(d => d
                        .Field(f => f.CheckoutTime)
                        .GreaterThanOrEquals(queryMinCheckoutTime)
                        .LessThanOrEquals(queryMaxCheckoutTime)
                    )
                && query
                    .DateRange(d => d
                        .Field(f => f.CompletionTime)
                        .GreaterThanOrEquals(queryMinCompletionTime)
                        .LessThanOrEquals(queryMaxCompletionTime)
                    );
        }

        private static void DeserializeOrderDetails(Order order)
        {
            switch (order.Type)
            {
                case OrderType.SoloRankedWins:
                    order.Details = JsonConvert.DeserializeObject<SoloRankedWins>(order.DetailsJsonObject);
                    break;
                case OrderType.DuoRankedWins:
                    order.Details = JsonConvert.DeserializeObject<DuoRankedWins>(order.DetailsJsonObject);
                    break;
                case OrderType.DuoRankedGames:
                    order.Details = JsonConvert.DeserializeObject<DuoRankedGames>(order.DetailsJsonObject);
                    break;
                case OrderType.SoloPromoBoosting:
                    order.Details = JsonConvert.DeserializeObject<SoloPromoBoosting>(order.DetailsJsonObject);
                    break;
                case OrderType.DuoPromoBoosting:
                    order.Details = JsonConvert.DeserializeObject<DuoPromoBoosting>(order.DetailsJsonObject);
                    break;
                case OrderType.SoloLeagueBoosting:
                    order.Details = JsonConvert.DeserializeObject<SoloLeagueBoosting>(order.DetailsJsonObject);
                    break;
                case OrderType.DuoLeagueBoosting:
                    order.Details = JsonConvert.DeserializeObject<DuoLeagueBoosting>(order.DetailsJsonObject);
                    break;
                case OrderType.SoloPlacementMatches:
                    order.Details = JsonConvert.DeserializeObject<SoloPlacementMatches>(order.DetailsJsonObject);
                    break;
                case OrderType.DuoPlacementMatches:
                    order.Details = JsonConvert.DeserializeObject<DuoPlacementMatches>(order.DetailsJsonObject);
                    break;
                case OrderType.NormalMatches:
                    order.Details = JsonConvert.DeserializeObject<NormalMatches>(order.DetailsJsonObject);
                    break;
                case OrderType.NormalWins:
                    order.Details = JsonConvert.DeserializeObject<NormalWins>(order.DetailsJsonObject);
                    break;
                case OrderType.Coaching:
                    order.Details = JsonConvert.DeserializeObject<Coaching>(order.DetailsJsonObject);
                    break;
                case OrderType.AccountLeveling:
                    order.Details = JsonConvert.DeserializeObject<AccountLeveling>(order.DetailsJsonObject);
                    break;
                case OrderType.ChampionMastery:
                    order.Details = JsonConvert.DeserializeObject<ChampionMastery>(order.DetailsJsonObject);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(Type), order.Type, null);
            }
        }

        #endregion
    }
}