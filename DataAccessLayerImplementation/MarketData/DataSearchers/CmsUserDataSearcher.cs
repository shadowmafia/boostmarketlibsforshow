﻿using DataAccessLayerAbstraction.MarketData.DataSearchers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataSearchers
{
    public class CmsUserDataSearcher : ElasticsearchClientContainer, ICmsUserDataSearcher
    {
        public CmsUserDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<CmsUser> GetCmsUser(
            string id = null,
            string email = null)
        {
            if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(email))
            {
                throw new ArgumentOutOfRangeException("at least one of the parameters must be specified");
            }

            var searchResponse = await _client.SearchAsync<CmsUser>(s => s
                .Size(1)
                .Query(q => q
                    .Terms(m => m
                        .Field(_elasticInnerIdFieldName)
                        .Terms(id)
                    ) || q
                    .Match(m => m
                        .Field(f => f.DefaultEmail.Email)
                        .Query(email?.ToUpper())
                    ) || q
                    .Match(m => m
                        .Field(f => f.GoogleData.GMail)
                        .Query(email?.ToUpper())
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<CmsUser> GetCmsUserByGoogleId(string googleId)
        {
            if (string.IsNullOrEmpty(googleId))
            {
                throw new ArgumentOutOfRangeException("at least one of the parameters must be specified");
            }

            var searchResponse = await _client.SearchAsync<CmsUser>(s => s
                .Size(1)
                .Query(q => q
                    .Terms(m => m
                        .Field(f => f.GoogleData.GoogleId)
                        .Terms(googleId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<CmsUser>> FindCmsUsers(
            string userName = null,
            string stringSearchQueryContain = null,
            IEnumerable<string> ids = null,
            bool? isUseDiscordNotification = null,
            bool? isDiscordConnected = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null,
            IEnumerable<CmsUserType> userTypes = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<CmsUser>(s => s
                .From(skip)
                .Size(take ?? _elasticMaxTakeValue - skip)
                .Query(
                    CreateBaseFindCustomerQuery(
                        userName,
                        stringSearchQueryContain,
                        ids,
                        isUseDiscordNotification,
                        isDiscordConnected,
                        minRegistrationDate,
                        maxRegistrationDate,
                        userTypes
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindCmsUsersCount(
            string userName = null,
            string stringSearchQueryContain = null,
            IEnumerable<string> ids = null,
            bool? isUseDiscordNotification = null,
            bool? isDiscordConnected = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null,
            IEnumerable<CmsUserType> userTypes = null)
        {
            var countResponse = await _client.CountAsync<CmsUser>(s => s
                .Query(
                    CreateBaseFindCustomerQuery(
                        userName,
                        stringSearchQueryContain,
                        ids,
                        isUseDiscordNotification,
                        isDiscordConnected,
                        minRegistrationDate,
                        maxRegistrationDate,
                        userTypes
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        #region private methods

        private Func<QueryContainerDescriptor<CmsUser>, QueryContainer> CreateBaseFindCustomerQuery(
            string username,
            string stringSearchQueryContain,
            IEnumerable<string> ids,
            bool? isUseDiscordNotification,
            bool? isDiscordConnected,
            DateTime? minRegistrationDate,
            DateTime? maxRegistrationDate,
            IEnumerable<CmsUserType> userTypes)
        {
            DateTime queryMinRegistrationDate = minRegistrationDate ?? DateTime.MinValue;
            DateTime queryMaxRegistrationDate = maxRegistrationDate ?? DateTime.MaxValue;

            return query =>
                query
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(ids)
                    )
                && query
                .Match(t => t
                    .Field(f => f.Name)
                    .Query(username)
                )
                && query.Bool(b => b
                    .Must(m =>
                           m
                            .MatchPhrasePrefix(t => t
                                 .Field(f => f.Name)
                                 .Query(stringSearchQueryContain)
                            )
                        || m
                            .MatchPhrasePrefix(t => t
                                .Field(f => f.RealName)
                                .Query(stringSearchQueryContain)
                            )
                        || m
                            .MatchPhrasePrefix(t => t
                                .Field(f => f.Note)
                                .Query(stringSearchQueryContain)
                            )
                   )
                )
                && query
                    .Terms(t => t
                        .Field(f => f.IsUseDiscordNotification)
                        .Terms(isUseDiscordNotification)
                    )
                && query
                    .Terms(t => t
                        .Field(f => f.IsDiscordConnected)
                        .Terms(isDiscordConnected)
                    )
                && query
                    .Terms(t => t
                        .Field(f => f.CmsUserType)
                        .Terms(userTypes)
                    )
                && query
                    .DateRange(r => r
                        .Field(f => f.RegistrationDate)
                        .GreaterThanOrEquals(queryMinRegistrationDate)
                        .LessThanOrEquals(queryMaxRegistrationDate)
                    );
        }

        #endregion
    }
}