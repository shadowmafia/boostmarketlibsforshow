﻿using DataAccessLayerAbstraction.MarketData.DataSearchers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderFeedbackClasses;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataSearchers
{
    public class OrderFeedbackDataSearcher : ElasticsearchClientContainer, IOrderFeedbackDataSearcher
    {
        public OrderFeedbackDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<OrderFeedback> GetOrderFeedback(string feedbackId)
        {
            if (string.IsNullOrEmpty(feedbackId))
            {
                throw new ArgumentNullException(nameof(feedbackId));
            }

            var searchResponse = await _client.SearchAsync<OrderFeedback>(s => s
                .Size(1)
                .Query(q => q
                    .Terms(m => m
                        .Field(_elasticInnerIdFieldName)
                        .Terms(feedbackId)
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.OrderId = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<OrderFeedback>> FindOrderFeedbacks(
            string boosterId = null,
            string customerId = null,
            string commentTextContain = null,
            short? minRating = null,
            short? maxRating = null,
            DateTime? minDateTime = null,
            DateTime? maxDateTime = null,
            IEnumerable<FeedbackStatus> feedbackStatuses = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<OrderFeedback>(search => search
                .Skip(skip)
                .Size(take)
                .Query(
                    CreateBaseFindOrderFeedbacksQuery(
                        boosterId,
                        customerId,
                        commentTextContain,
                        minRating,
                        maxRating,
                        minDateTime,
                        maxDateTime,
                        feedbackStatuses
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.OrderId = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindOrderFeedbacksCount(
            string boosterId = null,
            string customerId = null,
            string commentTextContain = null,
            short? minRating = null,
            short? maxRating = null,
            DateTime? minDateTime = null,
            DateTime? maxDateTime = null,
            IEnumerable<FeedbackStatus> feedbackStatuses = null)
        {
            var countResponse = await _client.CountAsync<OrderFeedback>(count => count
                .Query(
                    CreateBaseFindOrderFeedbacksQuery(
                        boosterId,
                        customerId,
                        commentTextContain,
                        minRating,
                        maxRating,
                        minDateTime,
                        maxDateTime,
                        feedbackStatuses
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        #region private methods

        private Func<QueryContainerDescriptor<OrderFeedback>, QueryContainer> CreateBaseFindOrderFeedbacksQuery(
            string boosterId,
            string customerId,
            string commentTextContain,
            short? minRating,
            short? maxRating,
            DateTime? minDateTime,
            DateTime? maxDateTime,
            IEnumerable<FeedbackStatus> feedbackStatuses)
        {
            DateTime queryMinDateTime = minDateTime ?? DateTime.MinValue;
            DateTime queryMaxDateTime = maxDateTime ?? DateTime.MaxValue;

            return query =>
                query
                    .MatchPhrase(terms => terms
                        .Field(f => f.CommentText)
                        .Query(commentTextContain)
                    )
                && query
                    .Terms(terms => terms
                        .Field(f => f.BoosterId)
                        .Terms(boosterId)
                    )
                && query
                    .Terms(terms => terms
                        .Field(f => f.CustomerId)
                        .Terms(customerId)
                    )
                && query
                    .Terms(terms => terms
                        .Field(f => f.Status)
                        .Terms(feedbackStatuses)
                    )
                && query
                    .Range(terms => terms
                        .Field(f => f.Rating)
                        .GreaterThanOrEquals(minRating)
                        .LessThanOrEquals(maxRating)
                    )
                && query
                    .DateRange(terms => terms
                        .Field(f => f.DateTime)
                        .GreaterThanOrEquals(queryMinDateTime)
                        .LessThanOrEquals(queryMaxDateTime)
                    );
        }

        #endregion
    }
}