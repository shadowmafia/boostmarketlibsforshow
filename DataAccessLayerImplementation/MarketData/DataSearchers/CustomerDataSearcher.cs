﻿using DataAccessLayerAbstraction.MarketData.DataSearchers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataSearchers
{
    public class CustomerDataSearcher : ElasticsearchClientContainer, ICustomerDataSearcher
    {
        public CustomerDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<Customer> GetCustomer(
            string id = null,
            string email = null,
            string username = null)
        {
            if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(email) && string.IsNullOrEmpty(username))
            {
                throw new ArgumentOutOfRangeException("at least one of the parameters must be specified");
            }

            var searchResponse = await _client.SearchAsync<Customer>(s => s
                  .Size(1)
                  .Query(q => q
                      .Terms(m => m
                          .Field(_elasticInnerIdFieldName)
                          .Terms(id)
                      ) && q
                      .Terms(m => m
                          .Field(f => f.Email)
                          .Terms(email?.ToUpper())
                      ) && q
                      .Terms(m => m
                          .Field(f => f.Username)
                          .Terms(username?.ToUpper())
                      )
                  )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<Customer>> FindCustomers(
            bool? isEmailConfirmed = null,
            IEnumerable<string> ids = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (skip.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<Customer>(s => s
                .From(skip)
                .Size(take)
                .Query(CreateBaseFindCustomersQuery(isEmailConfirmed, ids, minRegistrationDate, maxRegistrationDate))
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.Id = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindCustomersCount(
            bool? isEmailConfirmed = null,
            IEnumerable<string> ids = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null)
        {
            var countResponse = await _client.CountAsync<Customer>(s => s
                .Query(CreateBaseFindCustomersQuery(isEmailConfirmed, ids, minRegistrationDate, maxRegistrationDate))
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        #region private methods

        private Func<QueryContainerDescriptor<Customer>, QueryContainer> CreateBaseFindCustomersQuery(
            bool? isEmailConfirmed,
            IEnumerable<string> ids,
            DateTime? minRegistrationDate,
            DateTime? maxRegistrationDate)
        {
            DateTime queryMinRegistrationDate = minRegistrationDate ?? DateTime.MinValue;
            DateTime queryMaxRegistrationDate = maxRegistrationDate ?? DateTime.MaxValue;

            return query => query
                    .Term(f => f.IsEmailConfirmed, isEmailConfirmed)
                && query
                    .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(ids)
                    )
                && query
                    .DateRange(r => r
                        .Field(f => f.RegistrationDate)
                        .GreaterThanOrEquals(queryMinRegistrationDate)
                        .LessThanOrEquals(queryMaxRegistrationDate)
                    );
        }

        #endregion
    }
}