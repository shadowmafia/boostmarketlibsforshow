﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.MarketData.DataSearchers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.BoosterProfileClasses;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataSearchers
{
    public class BoosterProfileDataSearcher : ElasticsearchClientContainer, IBoosterProfileDataSearcher
    {
        public BoosterProfileDataSearcher(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<BoosterProfile> GetBoosterProfile(string cmsUserId)
        {
            if (string.IsNullOrEmpty(cmsUserId))
            {
                throw new ArgumentNullException(nameof(cmsUserId));
            }

            var searchResponse = await _client.SearchAsync<BoosterProfile>(s => s
                .Size(1)
                .Query(q => q
                     .Terms(m => m
                         .Field(_elasticInnerIdFieldName)
                         .Terms(cmsUserId)
                     )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.CmsUserId = h.Id;
                return h.Source;
            }).ToList().FirstOrDefault();
        }

        public async Task<IList<BoosterProfile>> FindBoosterProfiles(
            IEnumerable<string> cmsUserIds = null,
            bool? isCoacher = null,
            short? minTotalFeedBacks = null,
            short? minFeedbackRating = null,
            short? minTotalCompletedOrders = null,
            double? minCoachingPricePerHouse = null,
            double? maxCoachingPricePerHouse = null,
            string lolAccountIdContain = null,
            IEnumerable<Tier> tiers = null,
            IEnumerable<BoosterGroup> boosterGroups = null,
            IEnumerable<Lane> lanes = null,
            IEnumerable<string> languages = null,
            int? skip = null,
            int? take = null)
        {
            if (skip.HasValue && skip < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(skip));
            }

            if (take.HasValue && take < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(take));
            }

            if (skip > _elasticMaxTakeValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(skip)} : cant be most then {_elasticMaxTakeValue}");
            }

            var searchResponse = await _client.SearchAsync<BoosterProfile>(s => s
                .From(skip)
                .Size(take ?? _elasticMaxTakeValue - skip)
                .Query(
                    CreateBaseFindBoosterProfileQuery(
                        cmsUserIds,
                        isCoacher,
                        minTotalFeedBacks,
                        minFeedbackRating,
                        minTotalCompletedOrders,
                        minCoachingPricePerHouse,
                        maxCoachingPricePerHouse,
                        lolAccountIdContain,
                        tiers,
                        boosterGroups,
                        lanes,
                        languages
                    )
                )
            );

            return searchResponse.Hits.Select(h =>
            {
                h.Source.CmsUserId = h.Id;
                return h.Source;
            }).ToList();
        }

        public async Task<uint> FindBoosterProfilesCount(
            IEnumerable<string> cmsUserIds = null,
            bool? isCoacher = null,
            short? minTotalFeedBacks = null,
            short? minFeedbackRating = null,
            short? minTotalCompletedOrders = null,
            double? minCoachingPricePerHouse = null,
            double? maxCoachingPricePerHouse = null,
            string lolAccountIdContain = null,
            IEnumerable<Tier> tiers = null,
            IEnumerable<BoosterGroup> boosterGroups = null,
            IEnumerable<Lane> lanes = null,
            IEnumerable<string> languages = null)
        {
            var countResponse = await _client.CountAsync<BoosterProfile>(s => s
                .Query(
                    CreateBaseFindBoosterProfileQuery(
                        cmsUserIds,
                        isCoacher,
                        minTotalFeedBacks,
                        minFeedbackRating,
                        minTotalCompletedOrders,
                        minCoachingPricePerHouse,
                        maxCoachingPricePerHouse,
                        lolAccountIdContain,
                        tiers,
                        boosterGroups,
                        lanes,
                        languages
                    )
                )
            );

            return Convert.ToUInt32(countResponse.Count);
        }

        private Func<QueryContainerDescriptor<BoosterProfile>, QueryContainer> CreateBaseFindBoosterProfileQuery(
            IEnumerable<string> cmsUserIds,
            bool? isCoacher,
            short? minTotalFeedBacks,
            short? minFeedbackRating,
            short? minTotalCompletedOrders,
            double? minCoachingPricePerHouse,
            double? maxCoachingPricePerHouse,
            string lolAccountIdContain,
            IEnumerable<Tier> tiers,
            IEnumerable<BoosterGroup> boosterGroups,
            IEnumerable<Lane> lanes,
            IEnumerable<string> languages)
        {
            return query =>
                query
                   .Terms(t => t
                        .Field(_elasticInnerIdFieldName)
                        .Terms(cmsUserIds)
                    )
                && query
                   .MatchPhrasePrefix(
                   pha => pha
                        .Field(f => f.LoLAccount.LoLAccountId)
                        .Query(lolAccountIdContain)
                   )
                && query
                    .Term(f => f.IsCoacher, isCoacher)
                && query
                    .Term(f => f.IsCoacher, isCoacher)
                && query
                    .Terms(t => t
                        .Field(f => f.LoLAccount.Tier)
                        .Terms(tiers)
                    )
                && query
                    .Terms(t => t
                        .Field(f => f.BoosterGroup)
                        .Terms(boosterGroups)
                    )
                && query
                    .Terms(t => t
                        .Field(f => f.Lanes)
                        .Terms(lanes)
                    )
                && query
                    .Terms(t => t
                        .Field(f => f.Languages)
                        .Terms(languages)
                    )
                && query
                    .Range(r => r
                        .Field(f => f.TotalFeedBacks)
                        .GreaterThanOrEquals(minTotalFeedBacks)
                    )
                && query
                    .Range(r => r
                        .Field(f => f.FeedbackRating)
                        .GreaterThanOrEquals(minFeedbackRating)
                    )
                && query
                    .Range(r => r
                        .Field(f => f.TotalCompletedOrders)
                        .GreaterThanOrEquals(minTotalCompletedOrders)
                    )
                && query
                   .Range(r => r
                       .Field(f => f.CoachingPricePerHour)
                       .GreaterThanOrEquals(minCoachingPricePerHouse)
                       .GreaterThanOrEquals(maxCoachingPricePerHouse)
                   );
        }

        public Task<IList<(CmsUser cmsUser, BoosterProfile boosterProfile)>> FindBoosterProfileWithCmsModel(
            IEnumerable<string> cmsUserIds = null,
            bool? isCoacher = null,
            short? minTotalFeedBacks = null,
            short? minFeedbackRating = null,
            short? minTotalCompletedOrders = null,
            double? minCoachingPricePerHouse = null,
            double? maxCoachingPricePerHouse = null,
            string lolAccountIdContain = null,
            IEnumerable<Tier> tiers = null,
            IEnumerable<BoosterGroup> boosterGroups = null,
            IEnumerable<Lane> lanes = null,
            IEnumerable<string> languages = null,
            int? skip = null,
            int? take = null)
        {
            throw new NotImplementedException();
        }
    }
}