﻿using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataManagers
{
    public class CustomerDataManager : ElasticsearchEntityManagerBase<Customer>, ICustomerDataManager
    {
        public CustomerDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(Customer customer)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                    .Map(CreateMap())
                );
            }

            customer.Email = customer?.Email?.ToUpper();
            customer.Username = customer?.Username?.ToUpper();

            return await IndexDocumentAsync(customer);
        }

        public async Task DeleteCustomer(string id)
        {
            await DeleteDocumentAsync(id);
        }

        private static Func<TypeMappingDescriptor<Customer>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(
                    p => p
                        .Keyword(
                            f => f.Name(n => n.Email)
                        )
                        .Keyword(
                            f => f.Name(n => n.Username)
                        )
                );
        }
    }
}