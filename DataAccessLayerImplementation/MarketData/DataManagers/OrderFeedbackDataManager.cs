﻿using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Nest;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataManagers
{
    public class OrderFeedbackDataManager : ElasticsearchEntityManagerBase<OrderFeedback>, IOrderFeedbackDataManager
    {
        public OrderFeedbackDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(OrderFeedback orderFeedback)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                    .Map(
                        CreateMap()
                    )
                );
            }

            return await IndexDocumentAsync(orderFeedback);
        }

        public async Task DeleteOrderFeedback(string id)
        {
            await DeleteDocumentAsync(id);
        }

        private static Func<TypeMappingDescriptor<OrderFeedback>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(
                    p => p
                        .Keyword(
                            f => f.Name(n => n.BoosterId)
                        )
                        .Keyword(
                            f => f.Name(n => n.CustomerId)
                        )
                );
        }
    }
}