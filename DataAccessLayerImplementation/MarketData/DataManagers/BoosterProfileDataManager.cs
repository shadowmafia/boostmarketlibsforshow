﻿using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataManagers
{
    public class BoosterProfileDataManager : ElasticsearchEntityManagerBase<BoosterProfile>, IBoosterProfileDataManager
    {
        private const string BoosterProfileTokenizer = "booster_profile_tokenizer";
        private const string BoosterProfileAnalyzer = "booster_profile_analyzer";
        private const int MinNgram = 1;
        private const int MaxNgram = 2;

        private static readonly IEnumerable<string> AnalyzerFilters = new[]
        {
            "lowercase",
            "asciifolding",
            "trim"
        };

        private static readonly IEnumerable<TokenChar> NGramTokenChars = new[]
        {
            TokenChar.Digit,
            TokenChar.Letter,
            TokenChar.Symbol
        };

        public BoosterProfileDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(BoosterProfile boosterProfile)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                     .Settings(CreateIndexSettings())
                     .Map(CreateMap())
                );
            }

            return await IndexDocumentAsync(boosterProfile);
        }

        public async Task<List<string>> AddOrUpdate(IList<BoosterProfile> boosterProfiles)
        {
            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                      .Settings(CreateIndexSettings())
                      .Map(CreateMap())
                );
            }

            return await IndexManyDocumentsAsync(boosterProfiles);
        }

        public async Task DeleteBoosterProfile(string cmsUserId)
        {
            await DeleteDocumentAsync(cmsUserId);
        }

        private Func<IndexSettingsDescriptor, IPromise<IIndexSettings>> CreateIndexSettings()
        {
            return settings => settings
               .Analysis(a => a
                   .Analyzers(an => an
                       .Custom(
                           BoosterProfileAnalyzer,
                           c => c
                               .Tokenizer(BoosterProfileTokenizer)
                               .Filters(AnalyzerFilters)
                       )
                   )
                   .Tokenizers(t => t
                       .NGram(
                           BoosterProfileTokenizer,
                           ng => ng
                               .MinGram(MinNgram)
                               .MaxGram(MaxNgram)
                               .TokenChars(NGramTokenChars)
                       )
                   )
               );
        }

        private static Func<TypeMappingDescriptor<BoosterProfile>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(props => props
                    .Text(text => text
                        .Name(n => n.LoLAccount.LoLAccountId)
                        .Analyzer(BoosterProfileAnalyzer)
                    )
                );
        }
    }
}