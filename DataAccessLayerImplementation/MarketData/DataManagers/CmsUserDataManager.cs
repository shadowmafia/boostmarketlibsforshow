﻿using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataManagers
{
    public class CmsUserDataManager : ElasticsearchEntityManagerBase<CmsUser>, ICmsUserDataManager
    {
        private const string CmsUserTokenizer = "cms_user_tokenizer";
        private const string CmsUserAnalyzer = "cms_user_analyzer";
        private const int MinNgram = 1;
        private const int MaxNgram = 2;

        private static readonly IEnumerable<string> AnalyzerFilters = new[]
        {
            "lowercase",
            "asciifolding",
            "trim"
        };

        private static readonly IEnumerable<TokenChar> NGramTokenChars = new[]
        {
            TokenChar.Digit,
            TokenChar.Letter,
            TokenChar.Symbol
        };

        public CmsUserDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(CmsUser cmsUser)
        {
            if (cmsUser == null)
            {
                throw new ArgumentNullException(nameof(cmsUser));
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                    .Settings(CreateIndexSettings())
                    .Map(CreateMap())
                );
            }

            if (cmsUser.DefaultEmail != null)
            {
                cmsUser.DefaultEmail.Email = cmsUser.DefaultEmail.Email?.ToUpper();
            }

            if (cmsUser.GoogleData != null)
            {
                cmsUser.GoogleData.GMail = cmsUser.GoogleData.GMail?.ToUpper();
            }

            return await IndexDocumentAsync(cmsUser);
        }

        public async Task<List<string>> AddOrUpdate(List<CmsUser> cmsUsers)
        {
            if (cmsUsers == null)
            {
                throw new ArgumentNullException(nameof(cmsUsers));
            }

            if (cmsUsers.Count <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(cmsUsers));
            }

            foreach (var cmsUser in cmsUsers)
            {
                if (cmsUser.DefaultEmail != null)
                {
                    cmsUser.DefaultEmail.Email = cmsUser.DefaultEmail.Email?.ToUpper();
                }

                if (cmsUser.GoogleData != null)
                {
                    cmsUser.GoogleData.GMail = cmsUser.GoogleData.GMail?.ToUpper();
                }
            }

            return await IndexManyDocumentsAsync(cmsUsers);
        }

        public async Task DeleteCmsUser(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            await DeleteDocumentAsync(id);
        }

        private static Func<TypeMappingDescriptor<CmsUser>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(
                    p => p
                        .Keyword(
                            f => f.Name(n => n.DefaultEmail.Email)
                        )
                        .Keyword(
                            f => f.Name(n => n.GoogleData.GMail)
                        )
                        .Text(text => text
                            .Name(name => name.Name)
                            .Analyzer(CmsUserAnalyzer)
                        )
                        .Text(text => text
                            .Name(name => name.RealName)
                            .Analyzer(CmsUserAnalyzer)
                        )
                        .Text(text => text
                            .Name(name => name.Note)
                            .Analyzer(CmsUserAnalyzer)
                        )
                );
        }

        private Func<IndexSettingsDescriptor, IPromise<IIndexSettings>> CreateIndexSettings()
        {
            return settings => settings
                .Analysis(a => a
                    .Analyzers(an => an
                        .Custom(
                            CmsUserAnalyzer,
                            c => c
                                .Tokenizer(CmsUserTokenizer)
                                .Filters(AnalyzerFilters)
                        )
                    )
                    .Tokenizers(t => t
                        .NGram(
                            CmsUserTokenizer,
                            ng => ng
                                .MinGram(MinNgram)
                                .MaxGram(MaxNgram)
                                .TokenChars(NGramTokenChars)
                        )
                    )
                );
        }
    }
}