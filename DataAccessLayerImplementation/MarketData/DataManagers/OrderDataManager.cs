﻿using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Nest;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation.MarketData.DataManagers
{
    public class OrderDataManager : ElasticsearchEntityManagerBase<Order>, IOrderDataManager
    {
        public OrderDataManager(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        public async Task<string> AddOrUpdate(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }

            if (order.Details == null)
            {
                throw new ArgumentNullException(nameof(order.Details));
            }

            if (await IsIndexExistAsync() == false)
            {
                await CreateIndexAsync(ss => ss
                    .Map(
                        CreateMap()
                    )
                );
            }

            order.DetailsJsonObject = JsonConvert.SerializeObject(order.Details);
            order.Id = await IndexDocumentAsync(order);

            return order.Id;
        }

        public async Task DeleteOrder(string id)
        {
            await DeleteDocumentAsync(id);
        }

        private static Func<TypeMappingDescriptor<Order>, ITypeMapping> CreateMap()
        {
            return map => map
                .Properties(
                    p => p
                        .Keyword(
                            f => f.Name(n => n.BoosterId)
                        )
                        .Keyword(
                            f => f.Name(n => n.CustomerId)
                        )
                );
        }
    }
}