﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.MarketData;
using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.DataSearchers;
using DataAccessLayerImplementation._ElasticIndexSettings;
using DataAccessLayerImplementation.MarketData.DataManagers;
using DataAccessLayerImplementation.MarketData.DataSearchers;
using Nest;
using System;

namespace DataAccessLayerImplementation.MarketData
{
    public class MarketData : IMarketData
    {
        public TemplateDataContainer<ICmsUserDataManager, ICmsUserDataSearcher> CmsUser { get; }
        public TemplateDataContainer<IBoosterProfileDataManager, IBoosterProfileDataSearcher> BoosterProfile { get; }
        public TemplateDataContainer<ICustomerDataManager, ICustomerDataSearcher> Customer { get; }
        public TemplateDataContainer<IOrderDataManager, IOderDataSearcher> Order { get; }
        public TemplateDataContainer<IOrderFeedbackDataManager, IOrderFeedbackDataSearcher> OrderFeedback { get; }

        public MarketData(ElasticClient client, MarketDataIndexSettings indexSettings)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            if (indexSettings == null)
            {
                throw new ArgumentNullException(nameof(indexSettings));
            }

            CmsUser = new TemplateDataContainer<ICmsUserDataManager, ICmsUserDataSearcher>(
                new CmsUserDataManager(client, indexSettings.CmsUserIndexName),
                new CmsUserDataSearcher(client, indexSettings.CmsUserIndexName)
            );

            BoosterProfile = new TemplateDataContainer<IBoosterProfileDataManager, IBoosterProfileDataSearcher>(
                new BoosterProfileDataManager(client, indexSettings.BoosterProfileIndexName),
                new BoosterProfileDataSearcher(client, indexSettings.BoosterProfileIndexName)
            );

            Customer = new TemplateDataContainer<ICustomerDataManager, ICustomerDataSearcher>(
                new CustomerDataManager(client, indexSettings.CustomerIndexName),
                new CustomerDataSearcher(client, indexSettings.CustomerIndexName)
            );

            Order = new TemplateDataContainer<IOrderDataManager, IOderDataSearcher>(
                new OrderDataManager(client, indexSettings.OrderIndexName),
                new OderDataSearcher(client, indexSettings.OrderIndexName)
            );

            OrderFeedback = new TemplateDataContainer<IOrderFeedbackDataManager, IOrderFeedbackDataSearcher>(
                new OrderFeedbackDataManager(client, indexSettings.OrderFeedbackIndexName),
                new OrderFeedbackDataSearcher(client, indexSettings.OrderFeedbackIndexName)
            );
        }
    }
}