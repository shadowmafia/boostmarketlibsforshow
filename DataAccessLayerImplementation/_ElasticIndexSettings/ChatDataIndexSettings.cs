﻿namespace DataAccessLayerImplementation._ElasticIndexSettings
{
    public class ChatDataIndexSettings
    {
        public string ChatChannelIndexName { get; set; }
        public ChatMessageIndexSettings ChatMessageIndexSettings { get; set; }
    }

    public class ChatMessageIndexSettings
    {
        public string ChatMessageIndexName { get; set; }
        public int DurationOfStorageActualMessages { get; set; }
        public string ActualMessagesAliasName { get; set; }
        public string CurrentMonthMessagesAliasName { get; set; }
        public string ArchiveMessagesAliasName { get; set; }
    }
}
