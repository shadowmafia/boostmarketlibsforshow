﻿namespace DataAccessLayerImplementation._ElasticIndexSettings
{
    public class IndexesSettings
    {
        public string DefaultIndexName { get; set; }
        public MarketDataIndexSettings MarketDataIndexSettings { get; set; }
        public MarketSystemDataIndexSettings MarketSystemDataIndexSettings { get; set; }
        public GameDataIndexSettings GameDataIndexSettings { get; set; }
        public ChatDataIndexSettings ChatDataIndexSettings { get; set; }
    }
}
