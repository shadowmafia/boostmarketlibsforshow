﻿namespace DataAccessLayerImplementation._ElasticIndexSettings
{
    public class MarketDataIndexSettings
    {
        public string CmsUserIndexName { get; set; }
        public string BoosterProfileIndexName { get; set; }
        public string CustomerIndexName { get; set; }
        public string OrderIndexName { get; set; }
        public string OrderFeedbackIndexName { get; set; }
    }
}