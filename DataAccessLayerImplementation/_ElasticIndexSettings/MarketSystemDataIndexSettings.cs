﻿namespace DataAccessLayerImplementation._ElasticIndexSettings
{
    public class MarketSystemDataIndexSettings
    {
        public string TransactionIndexName { get; set; }
        public string ActionLogIndexName { get; set; }
        public string MarketLanguageIndexName { get; set; }
        public string LanguageStickerIndexName { get; set; }
    }
}