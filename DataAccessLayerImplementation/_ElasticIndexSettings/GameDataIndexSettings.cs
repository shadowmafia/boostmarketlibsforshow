﻿namespace DataAccessLayerImplementation._ElasticIndexSettings
{
    public class GameDataIndexSettings
    {
        public string BoosterGameAnalyticsIndexName { get; set; }
        public string BoosterMatchIndexName { get; set; }
        public string OrderMatchHistoryIndexName { get; set; }
    }
}