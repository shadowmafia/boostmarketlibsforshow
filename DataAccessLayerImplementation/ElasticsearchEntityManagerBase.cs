﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerImplementation
{
    public abstract class ElasticsearchEntityManagerBase<T> : ElasticsearchClientContainer where T : class
    {
        public bool IsUsUpdateIndexAfterEveryChangeIndexRequest { get; set; } = false;

        protected ElasticsearchEntityManagerBase(ElasticClient client, string indexName) : base(client, indexName)
        {

        }

        protected async Task<bool> IsIndexExistAsync()
        {
            return (await _client.Indices.ExistsAsync(_indexName)).Exists;
        }

        protected async Task<bool> IsDocumentExistAsync(string documentId)
        {
            if (string.IsNullOrEmpty(documentId))
            {
                throw new ArgumentNullException(nameof(documentId));
            }

            return (await _client.DocumentExistsAsync<T>(documentId)).Exists;
        }

        protected async Task CreateIndexAsync(Func<CreateIndexDescriptor, ICreateIndexRequest> selector = null)
        {
            await _client.Indices.CreateAsync(_indexName, selector);
        }

        public async Task DeleteIndexAsync()
        {
            await _client.Indices.DeleteAsync(_indexName);
        }

        protected async Task DeleteDocumentAsync(string documentId)
        {
            if (string.IsNullOrEmpty(documentId))
            {
                throw new ArgumentNullException(nameof(documentId));
            }

            await _client.DeleteAsync<T>(documentId);
            await TryRefreshIndexAsync();
        }

        protected async Task DeleteManyDocumentsAsync(IList<T> documents)
        {
            if (documents == null)
            {
                throw new ArgumentNullException(nameof(documents));
            }

            await _client.DeleteManyAsync<T>(documents);
            await TryRefreshIndexAsync();
        }

        protected async Task<string> IndexDocumentAsync(T document)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            string id = (await _client.IndexDocumentAsync(document)).Id;
            await TryRefreshIndexAsync();

            return id;
        }

        protected async Task<List<string>> IndexManyDocumentsAsync(IList<T> documents)
        {
            if (documents == null)
            {
                throw new ArgumentNullException(nameof(documents));
            }

            List<string> ids = (await _client.IndexManyAsync(documents))
                .Items
                .Select(item => item.Id)
                .ToList();

            await TryRefreshIndexAsync();

            return ids;
        }

        public async Task RefreshIndexAsync()
        {
            await _client.Indices.RefreshAsync(_indexName);
        }

        private async Task TryRefreshIndexAsync()
        {
            if (IsUsUpdateIndexAfterEveryChangeIndexRequest)
            {
                await _client.Indices.RefreshAsync(_indexName);
            }
        }
    }
}