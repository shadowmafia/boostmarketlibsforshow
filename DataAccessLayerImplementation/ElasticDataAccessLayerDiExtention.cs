﻿using DataAccessLayerAbstraction;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataAccessLayerImplementation
{
    public static class ElasticDataAccessLayerDiExtention
    {
        public static void AddElasticLoLBoostMarketDal(
            this IServiceCollection services,
            string dalSettingsContainFile = "appsettings.json",
            string dalSettingsSectionInFile = "DataAccessLayerSettings")
        {
            ElasticDataAccessLayerSettings dalSettings = new ElasticDataAccessLayerSettings();
            IConfigurationSection dalSettingsSection = new ConfigurationBuilder()
                .AddJsonFile(dalSettingsContainFile, true)
                .Build().GetSection(dalSettingsSectionInFile);

            dalSettingsSection.Bind(dalSettings);

            services.Configure<ElasticDataAccessLayerSettings>(
                dalSettingsSection
            );

            services.AddSingleton<IDataAccessLayer>(
                new ElasticDataAccessLayer(dalSettings)
            );
        }
    }
}