﻿namespace DataAccessLayerAbstraction._CommonEnums.System
{
    public enum Region
    {
        Br,
        Eune,
        Euw,
        Na,
        Kr,
        Lan,
        Las,
        Oce,
        Ru,
        Tr,
        Jp,
        Global,
        Americas,
        Europe,
        Asia
    }
}