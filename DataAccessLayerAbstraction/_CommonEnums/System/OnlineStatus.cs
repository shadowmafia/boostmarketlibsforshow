﻿namespace DataAccessLayerAbstraction._CommonEnums.System
{
    public enum OnlineStatus
    {
        Online,
        Offline
    }
}
