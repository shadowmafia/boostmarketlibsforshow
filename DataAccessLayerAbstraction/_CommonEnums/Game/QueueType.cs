﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum QueueType
    {
        SoloDuoQueue,
        Flex
    }
}