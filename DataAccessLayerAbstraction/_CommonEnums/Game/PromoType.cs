﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum PromoType
    {
        BestOf3,
        BestOf5
    }
}