﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum LpGain
    {
        min1max4,
        min5max9,
        min10max14,
        min15max17,
        min18max24plus
    }
}