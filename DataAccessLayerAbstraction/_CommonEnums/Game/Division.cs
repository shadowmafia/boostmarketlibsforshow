﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum Division
    {
        I,
        II,
        III,
        IV
    }
}