﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum Lane
    {
        Top,
        Mid,
        Jungle,
        Support,
        Marksman
    }
}