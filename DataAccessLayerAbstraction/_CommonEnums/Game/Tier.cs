﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum Tier
    {
        Unranked,
        Iron,
        Bronze,
        Silver,
        Gold,
        Platinum,
        Diamond,
        Master,
        Grandmaster,
        Challenger
    }
}