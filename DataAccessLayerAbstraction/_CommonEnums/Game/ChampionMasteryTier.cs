﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum ChampionMasteryTier
    {
        Tier0,
        Tier1,
        Tier2,
        Tier3,
        Tier4,
        Tier5,
        Tier6,
        Tier7
    }
}