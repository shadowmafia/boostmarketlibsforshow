﻿namespace DataAccessLayerAbstraction._CommonEnums.Game
{
    public enum CurrentLp
    {
        min0max20,
        min21max40,
        min41max60,
        min61max80,
        min81max99
    }
}