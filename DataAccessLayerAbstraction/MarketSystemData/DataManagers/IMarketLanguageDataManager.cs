﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataManagers
{
    public interface IMarketLanguageDataManager
    {
        Task AddOrUpdateMarketLanguage(MarketLanguage language);
        Task DeleteMarketLanguage(string languageId);
    }
}
