﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataManagers
{
    public interface IActionLogDataManager
    {
        Task<string> CreateActionLog(ActionLog actionLog);
    }
}