﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataManagers
{
    public interface ILanguageStickerDataManager
    {
        Task<string> AddOrUpdateLanguageSticker(LanguageSticker languageSticker);
        Task DeleteLanguageSticker(string stickerId);
    }
}
