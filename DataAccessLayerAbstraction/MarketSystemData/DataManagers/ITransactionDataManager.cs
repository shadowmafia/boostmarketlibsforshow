﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataManagers
{
    public interface ITransactionDataManager
    {
        Task<string> CreateTransaction(Transaction transaction);
    }
}