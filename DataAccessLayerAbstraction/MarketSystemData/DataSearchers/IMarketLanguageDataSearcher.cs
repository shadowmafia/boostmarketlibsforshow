﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataSearchers
{
    public interface IMarketLanguageDataSearcher
    {
        Task<MarketLanguage> GetMarketLanguage(
            string languageId = null,
            string locale = null,
            string languageName = null
        );

        Task<IList<MarketLanguage>> GetAllLanguages();
    }
}
