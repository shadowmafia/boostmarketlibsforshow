﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataSearchers
{
    public interface ILanguageStickerDataSearcher
    {
        Task<LanguageSticker> GetLanguageSticker(string stickerId);

        Task<IList<LanguageSticker>> FindLanguageSticker(
            string languageId = null,
            string stickerName = null,
            string stickerContentContain = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindLanguageStickerCount(
            string languageId = null,
            string stickerName = null,
            string stickerContentContain = null
        );
    }
}
