﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataSearchers
{
    public interface IActionLogDataSearcher
    {
        Task<IList<ActionLog>> FindActionLogs(
            string actionInitiatorId = null,
            string actionTargetId = null,
            string messageContain = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<LogInitiator> actionInitiators = null,
            IEnumerable<LogActionType> actionTypes = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindActionLogsCount(
            string actionInitiatorId = null,
            string actionTargetId = null,
            string messageContain = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<LogInitiator> actionInitiators = null,
            IEnumerable<LogActionType> actionTypes = null
        );
    }
}