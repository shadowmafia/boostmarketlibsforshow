﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.TransactionClasses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketSystemData.DataSearchers
{
    public interface ITransactionDataSearcher
    {
        Task<Transaction> GetTransaction(string transactionId);

        Task<IList<Transaction>> FindTransactions(
            string bindedObjectId = null,
            string transactionCommentContain = null,
            decimal? minValue = null,
            decimal? maxValue = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<TransactionType> transactionTypes = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindTransactionsCount(
            string bindedObjectIndex = null,
            string transactionCommentContain = null,
            decimal? minValue = null,
            decimal? maxValue = null,
            DateTime? dateTimeFrom = null,
            DateTime? dateTimeTo = null,
            IEnumerable<TransactionType> transactionTypes = null
        );
    }
}