﻿using DataAccessLayerAbstraction.MarketSystemData.DataManagers;
using DataAccessLayerAbstraction.MarketSystemData.DataSearchers;

namespace DataAccessLayerAbstraction.MarketSystemData
{
    public interface IMarketSystemData
    {
        TemplateDataContainer<IActionLogDataManager, IActionLogDataSearcher> ActionLog { get; }
        TemplateDataContainer<ITransactionDataManager, ITransactionDataSearcher> Transaction { get; }
        TemplateDataContainer<IMarketLanguageDataManager, IMarketLanguageDataSearcher> MarketLanguage { get; }
        TemplateDataContainer<ILanguageStickerDataManager, ILanguageStickerDataSearcher> LanguageSticker { get; }
    }
}