﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;

namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses
{
    public class LogTarget
    {
        public LogTargetType Type { get; set; }
        public string TargetId { get; set; }
    }
}