﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;

namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses
{
    public class LogInitiator
    {
        public LogInitiatorType Type { get; set; }
        public string InitiatorId { get; set; }
    }
}