﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses
{
    public class HttpAction
    {
        public ActionType Type { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public Dictionary<string, string> Cookies { get; set; }
        public byte[] Body { get; set; }
    }
}
