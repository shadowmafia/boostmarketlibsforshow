﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums
{
    public enum LogTargetType
    {
        None,
        CmsUser,
        Customer,
        Post,
        LoLAccount,
        Order
    }
}
