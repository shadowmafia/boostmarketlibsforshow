﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums
{
    public enum Level
    {
        Info,
        Debug,
        Warn,
        Error,
        Panic,
        Fatal,
    }
}
