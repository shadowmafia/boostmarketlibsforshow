﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums
{
    public enum ActionType
    {
        Request,
        Response
    }
}
