﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums
{
    public enum LogInitiatorType
    {
        System,
        CmsUser,
        Customer
    }
}
