﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums
{
    public enum LogActionType
    {
        Unknown,
        Create,
        Update,
        Delete
    }
}