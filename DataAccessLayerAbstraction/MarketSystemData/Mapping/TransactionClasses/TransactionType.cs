﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping.TransactionClasses
{
    public enum TransactionType
    {
        MoneyTransfer,
        MoneyDebit,
        MoneyFreezing,
        WithdrawalOfMoney,
        OrderPayment
    }
}