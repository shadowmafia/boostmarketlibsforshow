﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketSystemData.Mapping
{
    public class ActionLog
    {
        public string Id { get; set; }
        public Level Level { get; set; }
        public long Timestamp { get; set; }
        public string Caller { get; set; }
        public string Message { get; set; }
        public string Stacktrace { get; set; }

        public LogActionType ActionType { get; set; }
        public LogInitiator Initiator { get; set; }
        public LogTarget Target { get; set; }

        public Dictionary<string, HttpAction> Actions { get; set; }

        public string NodeName { get; set; }
        public string PodName { get; set; }
        public string PodNamespace { get; set; }
        public string PodServiceAccount { get; set; }
    }
}