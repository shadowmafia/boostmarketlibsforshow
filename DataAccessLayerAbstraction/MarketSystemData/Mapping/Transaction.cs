﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping.TransactionClasses;
using System;

namespace DataAccessLayerAbstraction.MarketSystemData.Mapping
{
    public class Transaction
    {
        public string Id { get; set; }
        public string BindedObjectId { get; set; }
        public string TransactionComment { get; set; }
        public decimal Value { get; set; }
        public TransactionType TransactionType { get; set; }
        public DateTime DateTime { get; set; }
    }
}