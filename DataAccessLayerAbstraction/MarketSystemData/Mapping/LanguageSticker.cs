﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping
{
    public class LanguageSticker
    {
        public string Id { get; set; }
        public string LanguageId { get; set; }
        public string StickerName { get; set; }
        public string StickerContent { get; set; }
    }
}
