﻿namespace DataAccessLayerAbstraction.MarketSystemData.Mapping
{
    public class MarketLanguage
    {
        public string Id { get; set; }
        public string Locale { get; set; }
        public string LanguageName { get; set; }
    }
}