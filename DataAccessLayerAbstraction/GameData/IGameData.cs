﻿using DataAccessLayerAbstraction.GameData.DataManagers;
using DataAccessLayerAbstraction.GameData.DataSearchers;

namespace DataAccessLayerAbstraction.GameData
{
    public interface IGameData
    {
        TemplateDataContainer<IBoosterGameAnalyticsDataManager, IBoosterGameAnalyticsDataSearcher> BoosterGameAnalytics { get; }
        TemplateDataContainer<IOrderMatchHistoryDataManager, IOrderMatchHistoryDataSearcher> OrderMatchHistory { get; }
        TemplateDataContainer<IBoosterMatchDataManager, IBoosterMatchDataSearcher> BoosterMatch { get; }
    }
}