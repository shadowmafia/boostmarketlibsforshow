﻿using DataAccessLayerAbstraction.GameData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.GameData.DataSearchers
{
    public interface IOrderMatchHistoryDataSearcher
    {
        Task<OrderMatchHistory> GetOrderMatchHistory(string orderId);
    }
}