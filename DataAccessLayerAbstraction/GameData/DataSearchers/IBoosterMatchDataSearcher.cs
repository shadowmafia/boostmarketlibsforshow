﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.Mapping;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.GameData.DataSearchers
{
    public interface IBoosterMatchDataSearcher
    {
        Task<BoosterMatch> GetBoosterMatch(string matchId);

        Task<IList<BoosterMatch>> FindBoosterMatches(
            string boosterId = null,
            string orderId = null,
            bool? isWin = null,
            DateTime? minGameCreation = null,
            DateTime? maxGameCreation = null,
            IEnumerable<long> championIds = null,
            IEnumerable<Tier> ties = null,
            IEnumerable<Lane> lanes = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindBoosterMatchesCount(
            string boosterId = null,
            string orderId = null,
            bool? isWin = null,
            DateTime? minGameCreation = null,
            DateTime? maxGameCreation = null,
            IEnumerable<long> championIds = null,
            IEnumerable<Tier> ties = null,
            IEnumerable<Lane> lanes = null
        );
    }
}