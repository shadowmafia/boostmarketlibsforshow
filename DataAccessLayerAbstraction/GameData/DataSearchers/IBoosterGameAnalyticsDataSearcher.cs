﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.Mapping;
using DataAccessLayerAbstraction.GameData.Mapping.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.GameData.DataSearchers
{
    public interface IBoosterGameAnalyticsDataSearcher
    {
        Task<BoosterGameAnalytics> GetBoosterGameAnalytics(string boosterIndex);

        Task<IList<GamesStats>> GetBoosterGameStats(
            string boosterId,
            IEnumerable<string> includeChampionIds = null,
            IEnumerable<Tier> includeTiers = null
        );

        Task<IList<BoosterGameAnalytics>> FindBoosterGameAnalytics(
            IEnumerable<string> boosterIds = null,
            IEnumerable<string> haveChampionIds = null,
            IEnumerable<Tier> haveTiers = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindBoosterGameAnalyticsCount(
            IEnumerable<string> boosterIds = null,
            IEnumerable<string> championIds = null,
            IEnumerable<Tier> tiers = null
        );
    }
}