﻿using DataAccessLayerAbstraction.GameData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.GameData.DataManagers
{
    public interface IOrderMatchHistoryDataManager
    {
        Task<string> AddOrUpdate(OrderMatchHistory orderMatchHistory);
        Task DeleteOrderMatchHistory(string id);
    }
}