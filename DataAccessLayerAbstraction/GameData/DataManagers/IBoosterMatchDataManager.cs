﻿using DataAccessLayerAbstraction.GameData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.GameData.DataManagers
{
    public interface IBoosterMatchDataManager
    {
        Task<string> AddOrUpdate(BoosterMatch boosterMatch);
        Task DeleteBoosterMatch(string id);
    }
}