﻿using DataAccessLayerAbstraction.GameData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.GameData.DataManagers
{
    public interface IBoosterGameAnalyticsDataManager
    {
        Task<string> AddOrUpdate(BoosterGameAnalytics gameAnalytics);
        Task DeleteBoosterGameAnalytics(string id);
    }
}