﻿using DataAccessLayerAbstraction.GameData.Mapping.Models;
using System;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.GameData.Mapping
{
    public class OrderMatchHistory
    {
        public string OrderId { get; set; }
        public List<Match> Matches { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}