﻿using DataAccessLayerAbstraction.GameData.Mapping.Models;

namespace DataAccessLayerAbstraction.GameData.Mapping
{
    public class BoosterMatch : Match
    {
        public string Id { get; set; }
        public string BoosterId { get; set; }
        public string OrderId { get; set; }
    }
}