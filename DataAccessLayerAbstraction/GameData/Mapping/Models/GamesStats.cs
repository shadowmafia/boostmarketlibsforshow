﻿namespace DataAccessLayerAbstraction.GameData.Mapping.Models
{
    public class GamesStats
    {
        public int TotalGames { get; set; }
        public int Wins { get; set; }
        public int Loses { get; set; }
        public double Kills { get; set; }
        public double Deaths { get; set; }
        public double Assists { get; set; }
        public long AverageMinionsKilled { get; set; }

        public long DoubleKills { get; set; }
        public long TripleKills { get; set; }
        public long QuadraKills { get; set; }
        public long PentaKills { get; set; }
    }
}