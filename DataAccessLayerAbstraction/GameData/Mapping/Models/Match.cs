﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System;

namespace DataAccessLayerAbstraction.GameData.Mapping.Models
{
    public class Match
    {
        public Tier Tier { get; set; }
        public QueueType QueueType { get; set; }
        public Lane Lane { get; set; }

        public bool IsWin { get; set; }
        public bool FirstBloodKill { get; set; }
        public long GameId { get; set; }
        public long ChampionId { get; set; }
        public int Spell1Id { get; set; }
        public int Spell2Id { get; set; }

        public long ChampLevel { get; set; }
        public long GoldEarned { get; set; }
        public long Kills { get; set; }
        public long Deaths { get; set; }
        public long Assists { get; set; }
        public long TotalMinionsKilled { get; set; }

        public long DoubleKills { get; set; }
        public long TripleKills { get; set; }
        public long QuadraKills { get; set; }
        public long PentaKills { get; set; }

        public long Item1 { get; set; }
        public long Item2 { get; set; }
        public long Item3 { get; set; }
        public long Item4 { get; set; }
        public long Item5 { get; set; }
        public long Item6 { get; set; }

        public DateTime GameCreation { get; set; }
        public TimeSpan MatchDuration { get; set; }
    }
}