﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.Mapping.Models;
using System;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.GameData.Mapping
{
    public class BoosterGameAnalytics
    {
        public string BoosterId { get; set; }
        public Dictionary<string, GamesStats> ChampionStats { get; set; }
        public Dictionary<Tier, GamesStats> TierStats { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}