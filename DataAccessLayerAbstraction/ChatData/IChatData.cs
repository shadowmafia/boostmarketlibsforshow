﻿using DataAccessLayerAbstraction.ChatData.DataManager;
using DataAccessLayerAbstraction.ChatData.DataSearchers;


namespace DataAccessLayerAbstraction.ChatData
{
    public interface IChatData
    {
        TemplateDataContainer<IChatChannelDataManager, IChatChannelDataSearcher> Channels { get; }
        TemplateDataContainer<IChatMessageDataManager, IChatMessagesDataSearcher> Messages { get; }
    }
}
