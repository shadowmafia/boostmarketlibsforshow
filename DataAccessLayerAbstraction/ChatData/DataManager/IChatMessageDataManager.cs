﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.ChatData.DataManager
{
    public interface IChatMessageDataManager
    {
        Task<string> AddOrUpdate(ChatMessage message);
        Task<List<string>> AddOrUpdate(IList<ChatMessage> messages);
        Task Delete(string messageId);
    }
}