﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using System;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.ChatData.DataManager
{
    public interface IChatChannelDataManager
    {
        Task<string> AddOrUpdate(ChatChannel channel);
        Task DeleteChannel(string channelId);
        Task AddMember(string channelId, ChatMember member);
        Task RemoveMember(string channelId, ChatMember member);
        Task UpdateLastMassage(string channelId, ChatMessage message);
        Task UpdateLastSeen(string channelId, ChatMember member, DateTime? lastSeen = null);
    }
}