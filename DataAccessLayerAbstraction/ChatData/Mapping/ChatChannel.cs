﻿using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.ChatData.Mapping
{
    public class ChatChannel
    {
        public string Id { get; set; }
        public string LinkedObjectId { get; set; }
        public ChatChannelType ChannelType { get; set; }
        public List<ChatMember> ChannelMembers { get; set; }
        public ChatMessage LastMessage { get; set; }
    }
}
