﻿using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using System;

namespace DataAccessLayerAbstraction.ChatData.Mapping
{
    public class ChatMessage
    {
        public string Id { get; set; }
        public string ChannelId { get; set; }
        public ChatMember Sender { get; set; }
        public string Content { get; set; }
        public DateTime? Timestamp { get; set; }
        public string AttachedImageUrl { get; set; }
        public string MessageMarker { get; set; }
    }
}
