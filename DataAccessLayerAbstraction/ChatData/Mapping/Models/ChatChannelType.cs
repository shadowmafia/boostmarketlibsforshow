﻿namespace DataAccessLayerAbstraction.ChatData.Mapping.Models
{
    public enum ChatChannelType
    {
        Private,
        Order,
        All,
        Lfg
    }
}
