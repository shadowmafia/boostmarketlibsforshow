﻿using System;

namespace DataAccessLayerAbstraction.ChatData.Mapping.Models
{
    public class ChatMember
    {
        public string MemberId { get; set; }
        public ChatMemberType MemberType { get; set; }
        /// <summary>
        /// This property only for ChatChannel
        /// </summary>
        public DateTime? LastSeen { get; set; }
    }
}
