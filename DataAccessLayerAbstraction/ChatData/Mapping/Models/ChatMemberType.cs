﻿namespace DataAccessLayerAbstraction.ChatData.Mapping.Models
{
    public enum ChatMemberType
    {
        CmsUser,
        Customer
    }
}
