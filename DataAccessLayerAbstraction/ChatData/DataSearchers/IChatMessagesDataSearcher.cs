﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.ChatData.DataSearchers
{
    public interface IChatMessagesDataSearcher
    {
        Task<ChatMessage> GetMessageById(string messageId);
        Task<IList<(string chanelId, long unreadMessagesCount)>> GetUnreadMessagesInChannels(
            ChatMember chatMember,
            IEnumerable<string> channelIds = null,
            int? maxLastChannelIncluding = null
        );
        Task<IList<ChatMessage>> FindMessages(
            string channelId,
            ChatMember sender = null,
            string textSearchQuery = null,
            DateTime? minTimestamp = null,
            DateTime? maxTimestamp = null,
            DateTime? elderThen = null,
            int? skip = null,
            int? take = null);

        Task<long> FindMessagesCount(
            string channelId,
            ChatMember sender = null,
            string textSearchQuery = null,
            DateTime? minTimestamp = null,
            DateTime? maxTimestamp = null,
            DateTime? elderThen = null);
    }
}