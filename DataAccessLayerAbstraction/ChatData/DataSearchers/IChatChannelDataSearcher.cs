﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.ChatData.DataSearchers
{
    public interface IChatChannelDataSearcher
    {
        Task<ChatChannel> GetChannel(string channelId);

        Task<ChatChannel> FindPrivateChannelBetweenUsers(ChatMember member1, ChatMember member2);

        Task<IList<ChatChannel>> GetChannelsByMember(
            ChatMember member,
            IEnumerable<ChatChannelType> channelTypes = null,
            DateTime? elderThen = null,
            string linkedObjectId = null,
            int? skip = null,
            int? take = null
        );

        Task<IList<ChatChannel>> GetChannelsSummary(
            ChatMember member,
            int? includingPrivateChannelsCount = null,
            int? includingOrderChannelsCount = null
        );
    }
}