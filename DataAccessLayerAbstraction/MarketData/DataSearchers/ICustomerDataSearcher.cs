﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataSearchers
{
    public interface ICustomerDataSearcher
    {
        Task<Customer> GetCustomer(
            string id = null,
            string email = null,
            string username = null
        );

        Task<IList<Customer>> FindCustomers(
            bool? isEmailConfirmed = null,
            IEnumerable<string> ids = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindCustomersCount(
            bool? isEmailConfirmed = null,
            IEnumerable<string> ids = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null
        );
    }
}