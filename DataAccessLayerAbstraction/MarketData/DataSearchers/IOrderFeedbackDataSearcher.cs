﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderFeedbackClasses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataSearchers
{
    public interface IOrderFeedbackDataSearcher
    {
        Task<OrderFeedback> GetOrderFeedback(string orderId);

        Task<IList<OrderFeedback>> FindOrderFeedbacks(
            string boosterId = null,
            string customerId = null,
            string commentTextContain = null,
            short? minRating = null,
            short? maxRating = null,
            DateTime? minDateTime = null,
            DateTime? maxDateTime = null,
            IEnumerable<FeedbackStatus> feedbackStatuses = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindOrderFeedbacksCount(
            string boosterId = null,
            string customerId = null,
            string commentTextContain = null,
            short? minRating = null,
            short? maxRating = null,
            DateTime? minDateTime = null,
            DateTime? maxDateTime = null,
            IEnumerable<FeedbackStatus> feedbackStatuses = null
        );
    }
}