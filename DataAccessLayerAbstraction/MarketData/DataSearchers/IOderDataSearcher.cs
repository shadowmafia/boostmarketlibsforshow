﻿using DataAccessLayerAbstraction._CommonEnums.System;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataSearchers
{
    public interface IOderDataSearcher
    {
        Task<Order> GetOrder(string orderId);

        Task<IList<Order>> FindOrders(
            string boosterId = null,
            string customerId = null,
            string accountName = null,
            Region? region = null,
            OrderType? orderType = null,
            OrderStatus? orderStatus = null,
            PaymentState? paymentState = null,
            DateTime? minCheckoutTime = null,
            DateTime? maxCheckoutTime = null,
            DateTime? minCompletionTime = null,
            DateTime? maxCompletionTime = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindOrdersCount(
            string boosterId = null,
            string customerId = null,
            string accountName = null,
            Region? region = null,
            OrderType? orderType = null,
            OrderStatus? orderStatus = null,
            PaymentState? paymentState = null,
            DateTime? minCheckoutTime = null,
            DateTime? maxCheckoutTime = null,
            DateTime? minCompletionTime = null,
            DateTime? maxCompletionTime = null
        );
    }
}