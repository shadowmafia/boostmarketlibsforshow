﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.BoosterProfileClasses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataSearchers
{
    public interface IBoosterProfileDataSearcher
    {
        Task<BoosterProfile> GetBoosterProfile(string cmsUserId);

        Task<IList<BoosterProfile>> FindBoosterProfiles
        (
            IEnumerable<string> cmsUserIds = null,
            bool? isCoacher = null,
            short? minTotalFeedBacks = null,
            short? minFeedbackRating = null,
            short? minTotalCompletedOrders = null,
            double? minCoachingPricePerHouse = null,
            double? maxCoachingPricePerHouse = null,
            string lolAccountIdContain = null,
            IEnumerable<Tier> tiers = null,
            IEnumerable<BoosterGroup> boosterGroups = null,
            IEnumerable<Lane> lanes = null,
            IEnumerable<string> languages = null,
            int? skip = null,
            int? take = null
        );

        Task<IList<(CmsUser cmsUser, BoosterProfile boosterProfile)>> FindBoosterProfileWithCmsModel
        (
            IEnumerable<string> cmsUserIds = null,
            bool? isCoacher = null,
            short? minTotalFeedBacks = null,
            short? minFeedbackRating = null,
            short? minTotalCompletedOrders = null,
            double? minCoachingPricePerHouse = null,
            double? maxCoachingPricePerHouse = null,
            string lolAccountIdContain = null,
            IEnumerable<Tier> tiers = null,
            IEnumerable<BoosterGroup> boosterGroups = null,
            IEnumerable<Lane> lanes = null,
            IEnumerable<string> languages = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindBoosterProfilesCount
        (
            IEnumerable<string> cmsUserIds = null,
            bool? isCoacher = null,
            short? minTotalFeedBacks = null,
            short? minFeedbackRating = null,
            short? minTotalCompletedOrders = null,
            double? minCoachingPricePerHouse = null,
            double? maxCoachingPricePerHouse = null,
            string lolAccountIdContain = null,
            IEnumerable<Tier> tiers = null,
            IEnumerable<BoosterGroup> boosterGroups = null,
            IEnumerable<Lane> lanes = null,
            IEnumerable<string> languages = null
        );
    }
}