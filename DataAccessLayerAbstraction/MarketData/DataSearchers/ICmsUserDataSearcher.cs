﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataSearchers
{
    public interface ICmsUserDataSearcher
    {
        Task<CmsUser> GetCmsUser(
            string id = null,
            string email = null
        );

        Task<CmsUser> GetCmsUserByGoogleId(
            string googleId
        );

        Task<IList<CmsUser>> FindCmsUsers(
            string userName = null,
            string stringSearchQueryContain = null,
            IEnumerable<string> ids = null,
            bool? isUseDiscordNotification = null,
            bool? isDiscordConnected = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null,
            IEnumerable<CmsUserType> userTypes = null,
            int? skip = null,
            int? take = null
        );

        Task<uint> FindCmsUsersCount(
            string userName = null,
            string stringSearchQueryContain = null,
            IEnumerable<string> ids = null,
            bool? isUseDiscordNotification = null,
            bool? isDiscordConnected = null,
            DateTime? minRegistrationDate = null,
            DateTime? maxRegistrationDate = null,
            IEnumerable<CmsUserType> userTypes = null
        );
    }
}