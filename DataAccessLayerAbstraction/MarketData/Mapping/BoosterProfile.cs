﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.MarketData.Mapping.BoosterProfileClasses;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping
{
    public class BoosterProfile
    {
        public string CmsUserId { get; set; }
        public string AboutMe { get; set; }
        public LoLAccount LoLAccount { get; set; }
        public LiveStream LiveStream { get; set; }
        public HashSet<Lane> Lanes { get; set; }
        public HashSet<string> Languages { get; set; }
        public BoosterGroup BoosterGroup { get; set; }
        public bool IsCoacher { get; set; }

        public double FeedbackRating { get; set; }
        public short TotalFeedBacks { get; set; }
        public short TotalCompletedOrders { get; set; }
        public short OrdersInProcessing { get; set; }
        public decimal CoachingPricePerHour { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenMoney { get; set; }
        public decimal AmountEarnedMoney { get; set; }
    }
}