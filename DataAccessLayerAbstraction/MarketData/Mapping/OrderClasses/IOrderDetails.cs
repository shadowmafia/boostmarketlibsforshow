﻿using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses
{
    public interface IOrderDetails
    {
        List<AdditionalService> AdditionalServices { get; set; }
    }
}