﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class DuoRankedWins : IOrderDetails
    {
        public Division CurrentDivision { get; set; }
        public Tier CurrentTier { get; set; }
        public QueueType QueueType { get; set; }
        public short NeedWins { get; set; }
        public string CoachingLanguageId { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}