﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class Coaching : IOrderDetails
    {
        public TimeSpan CoachingDuration { get; set; }
        public Tier MinCoacherTier { get; set; }
        public string CoacherLanguageId { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}
