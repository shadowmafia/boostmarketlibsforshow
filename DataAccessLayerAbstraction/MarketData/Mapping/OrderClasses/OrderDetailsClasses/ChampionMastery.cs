﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class ChampionMastery : IOrderDetails
    {
        public string ChampionId { get; set; }
        public ChampionMasteryTier CurrentTier { get; set; }
        public ChampionMasteryTier FinalTier { get; set; }
        public short TokensNeeded { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}