﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class SoloLeagueBoosting : IOrderDetails
    {
        public QueueType QueueType { get; set; }
        public LpGain LpGain { get; set; }

        public Division CurrentDivision { get; set; }
        public Tier CurrentTier { get; set; }
        public CurrentLp CurrentLp { get; set; }

        public Division FinalDivision { get; set; }
        public Tier FinalTier { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}