﻿using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class AccountLeveling : IOrderDetails
    {
        public short CurrentLevel { get; set; }
        public short FinalLevel { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}