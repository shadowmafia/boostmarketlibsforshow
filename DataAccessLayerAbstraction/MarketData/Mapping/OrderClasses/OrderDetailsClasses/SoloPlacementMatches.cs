﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class SoloPlacementMatches : IOrderDetails
    {
        public Division LastSeasonDivision { get; set; }
        public Tier LastSeasonTier { get; set; }
        public QueueType QueueType { get; set; }
        public short NeedGames { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}