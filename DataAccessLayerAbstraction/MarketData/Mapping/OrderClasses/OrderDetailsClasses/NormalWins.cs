﻿using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class NormalWins : IOrderDetails
    {
        public short NeedGames { get; set; }
        public string CoachingLanguageId { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}