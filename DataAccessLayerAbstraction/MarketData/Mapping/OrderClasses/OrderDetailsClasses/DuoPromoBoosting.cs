﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class DuoPromoBoosting : IOrderDetails
    {
        public Division CurrentDivision { get; set; }
        public Tier CurrentTier { get; set; }
        public QueueType QueueType { get; set; }
        public PromoType PromoType { get; set; }
        public short Wins { get; set; }
        public short Loses { get; set; }
        public string CoachingLanguageId { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}