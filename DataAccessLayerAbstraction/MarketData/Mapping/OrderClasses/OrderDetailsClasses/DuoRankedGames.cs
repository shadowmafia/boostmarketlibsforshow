﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses
{
    public class DuoRankedGames : IOrderDetails
    {
        public Division CurrentDivision { get; set; }
        public Tier CurrentTier { get; set; }
        public QueueType QueueType { get; set; }
        public short NeedGames { get; set; }
        public string CoachingLanguageId { get; set; }

        public List<AdditionalService> AdditionalServices { get; set; }
    }
}