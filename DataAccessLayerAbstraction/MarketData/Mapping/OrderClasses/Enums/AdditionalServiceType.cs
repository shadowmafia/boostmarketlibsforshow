﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums
{
    public enum AdditionalServiceType
    {
        SpecificChampions,
        WithStreaming,
        PriorityOrder,
        PlusOneWin,
        WithCoaching,
        PlayWithBooster,
        IWillProvideXpBoostEvry10Games
    }
}