﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums
{
    public enum PaymentState
    {
        AwaitingPayment,
        Paid
    }
}