﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums
{
    public enum OrderStatus
    {
        InProcessing,
        Pause,
        Pending,
        InProgress,
        InApproving,
        Completed,
        Canceled
    }
}