﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums
{
    public enum OrderType
    {
        SoloRankedWins,
        DuoRankedWins,
        DuoRankedGames,
        SoloPromoBoosting,
        DuoPromoBoosting,
        SoloLeagueBoosting,
        DuoLeagueBoosting,
        SoloPlacementMatches,
        DuoPlacementMatches,
        NormalMatches,
        NormalWins,
        Coaching,
        AccountLeveling,
        ChampionMastery
    }
}