﻿using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses
{
    public class AdditionalService
    {
        public bool IsPercent { get; set; }
        public int Markup { get; set; }
        public AdditionalServiceType AdditionalServiceType { get; set; }
    }
}