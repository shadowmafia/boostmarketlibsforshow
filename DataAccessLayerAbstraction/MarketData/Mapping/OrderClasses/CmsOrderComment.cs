﻿using System;

namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses
{
    public class CmsOrderComment
    {
        public string CmsUserIndex { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}