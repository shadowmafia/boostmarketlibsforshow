﻿using DataAccessLayerAbstraction._CommonEnums.Game;

namespace DataAccessLayerAbstraction.MarketData.Mapping.BoosterProfileClasses
{
    public class LoLAccount
    {
        public string LoLAccountId { get; set; }
        public Tier Tier { get; set; }
        public Division Division { get; set; }
        public short LeaguePoints { get; set; }
        public int Wins { get; set; }
        public int Loses { get; set; }
    }
}
