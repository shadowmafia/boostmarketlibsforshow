﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.BoosterProfileClasses
{
    public enum BoosterGroup
    {
        Junior,
        Middle,
        Senior,
        Lead,
        Expert,
        Vip
    }
}