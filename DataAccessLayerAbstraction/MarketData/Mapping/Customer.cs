﻿using DataAccessLayerAbstraction._CommonEnums.System;
using System;

namespace DataAccessLayerAbstraction.MarketData.Mapping
{
    public class Customer
    {
        public string Id { get; set; }
        public OnlineStatus OnlineStatus { get; set; }
        public string Email { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string Skype { get; set; }
        public string Discord { get; set; }
        public string AvatarUrl { get; set; }

        public DateTime LastSeen { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}