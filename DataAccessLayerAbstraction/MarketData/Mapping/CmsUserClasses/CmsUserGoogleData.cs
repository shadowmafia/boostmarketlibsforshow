﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses
{
    public class CmsUserGoogleData
    {
        public bool IsGoogleConnected { get; set; }
        public bool IsGMailVerified { get; set; }
        public string GoogleId { get; set; }
        public string GMail { get; set; }
    }
}