﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses
{
    public enum CmsUserType
    {
        Booster,
        Manager,
        Admin,
        SuperAdmin
    }
}