﻿using System;

namespace DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses
{
    public class DefaultEmail
    {
        public string Email { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public string LastVerifiedEmail { get; set; }
        public DateTime? LastVerifiedEmailDateTime { get; set; }
        public DateTime? LastVerificationEmailDateTime { get; set; }
    }
}
