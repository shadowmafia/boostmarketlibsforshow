﻿using DataAccessLayerAbstraction._CommonEnums.System;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using System;

namespace DataAccessLayerAbstraction.MarketData.Mapping
{
    public class CmsUser
    {
        public string Id { get; set; }
        public bool IsBanned { get; set; }
        public bool IsManagerBooster { get; set; }
        public OnlineStatus OnlineStatus { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string DiscordTag { get; set; }
        public string ProfileImageUrl { get; set; }
        public string RealName { get; set; }
        public string Note { get; set; }
        public string AdminNote { get; set; }
        public bool IsDiscordConnected { get; set; }
        public bool IsUseDiscordNotification { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime BanDate { get; set; }
        public DateTime LastOrderCompleteDate { get; set; }

        public CmsUserType CmsUserType { get; set; }

        public DefaultEmail DefaultEmail { get; set; }
        public CmsUserGoogleData GoogleData { get; set; }
    }
}