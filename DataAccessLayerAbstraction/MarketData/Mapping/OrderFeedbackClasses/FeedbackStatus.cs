﻿namespace DataAccessLayerAbstraction.MarketData.Mapping.OrderFeedbackClasses
{
    public enum FeedbackStatus
    {
        New,
        Confirmed,
        Rejected
    }
}