﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction._CommonEnums.System;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums;
using System;
using System.Collections.Generic;

namespace DataAccessLayerAbstraction.MarketData.Mapping
{
    public class Order
    {
        public string Id { get; set; }
        public string BoosterId { get; set; }
        public string CustomerId { get; set; }

        public string AccountName { get; set; }
        public string AccountLogin { get; set; }
        public string AccountPassword { get; set; }
        public string CustomerComment { get; set; }
        public decimal Price { get; set; }
        public TimeSpan MinEstimateValue { get; set; }
        public TimeSpan MaxEstimateValue { get; set; }
        public string DiscountCode { get; set; }

        public Region Region { get; set; }
        public OrderType Type { get; set; }
        public FlashButton FlashButton { get; set; }
        public OrderStatus Status { get; set; }
        public PaymentState PaymentState { get; set; }
        public bool IsAppearOfflineOnChat { get; set; }

        public DateTime CheckoutTime { get; set; }
        public DateTime CompletionTime { get; set; }
        public List<CmsOrderComment> CmsOrderComments { get; set; }

        public IOrderDetails Details { get; set; }
        public string DetailsJsonObject { get; set; }
    }
}