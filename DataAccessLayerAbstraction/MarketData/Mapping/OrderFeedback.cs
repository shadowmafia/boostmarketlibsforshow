﻿using DataAccessLayerAbstraction.MarketData.Mapping.OrderFeedbackClasses;
using System;

namespace DataAccessLayerAbstraction.MarketData.Mapping
{
    public class OrderFeedback
    {
        public string OrderId { get; set; }
        public string CustomerId { get; set; }
        public string BoosterId { get; set; }
        public DateTime DateTime { get; set; }
        public short Rating { get; set; }
        public string CommentText { get; set; }
        public FeedbackStatus Status { get; set; }
    }
}