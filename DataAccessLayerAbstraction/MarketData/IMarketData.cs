﻿using DataAccessLayerAbstraction.MarketData.DataManagers;
using DataAccessLayerAbstraction.MarketData.DataSearchers;

namespace DataAccessLayerAbstraction.MarketData
{
    public interface IMarketData
    {
        TemplateDataContainer<ICmsUserDataManager, ICmsUserDataSearcher> CmsUser { get; }
        TemplateDataContainer<IBoosterProfileDataManager, IBoosterProfileDataSearcher> BoosterProfile { get; }
        TemplateDataContainer<ICustomerDataManager, ICustomerDataSearcher> Customer { get; }
        TemplateDataContainer<IOrderDataManager, IOderDataSearcher> Order { get; }
        TemplateDataContainer<IOrderFeedbackDataManager, IOrderFeedbackDataSearcher> OrderFeedback { get; }
    }
}