﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataManagers
{
    public interface IOrderFeedbackDataManager
    {
        Task<string> AddOrUpdate(OrderFeedback orderFeedback);
        Task DeleteOrderFeedback(string id);
    }
}