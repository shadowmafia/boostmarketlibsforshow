﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataManagers
{
    public interface IOrderDataManager
    {
        Task<string> AddOrUpdate(Order order);
        Task DeleteOrder(string id);
    }
}