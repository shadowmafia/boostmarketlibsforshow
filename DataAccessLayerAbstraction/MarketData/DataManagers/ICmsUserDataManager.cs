﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataManagers
{
    public interface ICmsUserDataManager
    {
        Task<string> AddOrUpdate(CmsUser cmsUser);
        Task<List<string>> AddOrUpdate(List<CmsUser> cmsUser);
        Task DeleteCmsUser(string id);
    }
}