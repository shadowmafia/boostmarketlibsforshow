﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataManagers
{
    public interface ICustomerDataManager
    {
        Task<string> AddOrUpdate(Customer customer);
        Task DeleteCustomer(string id);
    }
}