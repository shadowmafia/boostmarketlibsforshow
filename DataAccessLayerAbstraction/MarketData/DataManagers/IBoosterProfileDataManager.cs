﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.MarketData.DataManagers
{
    public interface IBoosterProfileDataManager
    {
        Task<string> AddOrUpdate(BoosterProfile boosterProfile);
        Task<List<string>> AddOrUpdate(IList<BoosterProfile> boosterProfile);
        Task DeleteBoosterProfile(string cmsUserId);
    }
}