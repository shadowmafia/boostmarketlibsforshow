﻿using System;

namespace DataAccessLayerAbstraction
{
    /// <summary>
    /// Generic container for entity data management and searching methods.
    /// </summary>
    /// <typeparam name="T1">Manager</typeparam>
    /// <typeparam name="T2">Searcher</typeparam>
    public class TemplateDataContainer<T1, T2>
    {
        public TemplateDataContainer(T1 manager, T2 searcher)
        {
            if (manager == null)
            {
                throw new ArgumentNullException(nameof(manager));
            }

            if (searcher == null)
            {
                throw new ArgumentNullException(nameof(searcher));
            }

            Manager = manager;
            Searcher = searcher;
        }

        public T1 Manager { get; }
        public T2 Searcher { get; }
    }
}