﻿using DataAccessLayerAbstraction.ChatData;
using DataAccessLayerAbstraction.GameData;
using DataAccessLayerAbstraction.MarketData;
using DataAccessLayerAbstraction.MarketSystemData;

namespace DataAccessLayerAbstraction
{
    public interface IDataAccessLayer
    {
        IMarketData MarketData { get; }
        IMarketSystemData MarketSystemData { get; }
        IGameData GameData { get; }
        IChatData ChatData { get; }
    }
}