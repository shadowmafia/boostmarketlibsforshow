# README #


### What is this? ###
 
* This is a prototype of libraries for a marketplace providing in-game services.

### How to use ###

* 1) Install Elasticsearch.
* 2) Generate credentials in elasticsearch.
* 3) Configure elasticsearc in [appsettings.json](https://bitbucket.org/shadowmafia/boostmarketlibsforshow/src/master/DataAccessLayerTests/appsettings.json) for tests.
* 4) Run tests, enjoy and learn.

### Additional ###

**You can also check this using a prototype chat service. But don't forget to look into appsettings.json in the projects of this repository.**
**[ChatServicePrototype](https://bitbucket.org/shadowmafia/chatserviceprototype/src/master/)**