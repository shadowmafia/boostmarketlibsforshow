﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace DataAccessLayerTests
{
    public static class TestsHelper
    {
        public static TestServer InitTestServer()
        {
            var builder = new WebHostBuilder()
                .UseEnvironment("UnitTest")
                .UseStartup<Startup>();

            return new TestServer(builder);
        }
    }
}