﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.Mapping;
using DataAccessLayerAbstraction.GameData.Mapping.Models;
using DataAccessLayerImplementation.GameData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.GameData
{
    [TestClass]
    [TestCategory("OrderMatchHistoryTests")]
    public class OrderMatchHistoryDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.GameDataIndexSettings.OrderMatchHistoryIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.GameDataIndexSettings.OrderMatchHistoryIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetOrderMatchHistory_Method()
        {
            OrderMatchHistory testOrderMatchHistory = new OrderMatchHistory
            {
                LastUpdate = DateTime.UtcNow,
                OrderId = "TestOrderId1",
                Matches = new List<Match>
                {
                    new Match
                    {
                        Tier = Tier.Diamond,
                        ChampionId = 2294,
                        Assists = 10,
                        ChampLevel = 18,
                        Deaths = 10
                    }
                }
            };

            OrderMatchHistory testOrderMatchHistory2 = new OrderMatchHistory
            {
                LastUpdate = DateTime.UtcNow,
                OrderId = "TestOrderId2",
                Matches = new List<Match>
                {
                    new Match
                    {
                        Tier = Tier.Challenger,
                        ChampionId = 2294,
                        Assists = 10,
                        ChampLevel = 18,
                        Deaths = 10
                    }
                }
            };

            await _dal.GameData.OrderMatchHistory.Manager.AddOrUpdate(testOrderMatchHistory);
            await _dal.GameData.OrderMatchHistory.Manager.AddOrUpdate(testOrderMatchHistory2);

            await ((OrderMatchHistoryDataManager)_dal.GameData.OrderMatchHistory.Manager).RefreshIndexAsync();

            OrderMatchHistory getResult1 = await _dal.GameData.OrderMatchHistory.Searcher.GetOrderMatchHistory(testOrderMatchHistory.OrderId);
            OrderMatchHistory getResult2 = await _dal.GameData.OrderMatchHistory.Searcher.GetOrderMatchHistory(testOrderMatchHistory2.OrderId);

            Assert.AreEqual(testOrderMatchHistory.OrderId, getResult1.OrderId);
            Assert.AreEqual(testOrderMatchHistory2.OrderId, getResult2.OrderId);
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetOrderMatchHistory_OrderIdIsNull()
        {
            await _dal.GameData.OrderMatchHistory.Searcher.GetOrderMatchHistory(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetOrderMatchHistory_OrderIdIsEmpty()
        {
            await _dal.GameData.OrderMatchHistory.Searcher.GetOrderMatchHistory(string.Empty);
        }

        #endregion
    }
}
