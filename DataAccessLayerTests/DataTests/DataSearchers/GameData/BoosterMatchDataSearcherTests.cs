﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.Mapping;
using DataAccessLayerImplementation.GameData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.GameData
{
    [TestClass]
    [TestCategory("BoosterMatchTests")]
    public class BoosterMatchDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.GameDataIndexSettings.BoosterMatchIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.GameDataIndexSettings.BoosterMatchIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetBoosterMatch_Method()
        {
            List<BoosterMatch> testBoosterMatches = await CreateTestsBoosterMatches();

            BoosterMatch getResult1 = await _dal.GameData.BoosterMatch.Searcher.GetBoosterMatch(testBoosterMatches[0].Id);
            BoosterMatch getResult2 = await _dal.GameData.BoosterMatch.Searcher.GetBoosterMatch(testBoosterMatches[0].Id);

            Assert.IsNotNull(getResult1);
            Assert.IsNotNull(getResult2);
            Assert.AreEqual(testBoosterMatches[0].BoosterId, getResult1.BoosterId);
            Assert.AreEqual(testBoosterMatches[2].BoosterId, getResult2.BoosterId);
        }

        [TestMethod]
        public async Task Test_FindBoosterMatches_FindBoosterMatchesCount_Methods()
        {
            List<BoosterMatch> testBoosterMatches = await CreateTestsBoosterMatches();

            List<BoosterMatch> searchResult1 = (await _dal.GameData.BoosterMatch.Searcher.FindBoosterMatches(
                boosterId: testBoosterMatches[0].BoosterId,
                orderId: testBoosterMatches[0].OrderId,
                isWin: true,
                minGameCreation: testBoosterMatches[0].GameCreation,
                maxGameCreation: testBoosterMatches[0].GameCreation,
                championIds: new List<long>
                {
                    1,
                    2
                },
                ties: new List<Tier>
                {
                    Tier.Challenger,
                    Tier.Grandmaster
                },
                lanes: new List<Lane>
                {
                    Lane.Mid,
                    Lane.Jungle
                },
                skip: 1
                )).ToList();

            uint countResult1 = await _dal.GameData.BoosterMatch.Searcher.FindBoosterMatchesCount(
                boosterId: testBoosterMatches[0].BoosterId,
                orderId: testBoosterMatches[0].OrderId,
                isWin: true,
                minGameCreation: testBoosterMatches[0].GameCreation,
                maxGameCreation: testBoosterMatches[0].GameCreation,
                championIds: new List<long>
                {
                    1,
                    2
                },
                ties: new List<Tier>
                {
                    Tier.Challenger,
                    Tier.Grandmaster
                },
                lanes: new List<Lane>
                {
                    Lane.Mid,
                    Lane.Jungle
                }
            );

            Assert.IsNotNull(searchResult1);
            Assert.AreEqual(2u, countResult1);
            Assert.AreEqual(1, searchResult1.Count);
            Assert.IsNotNull(searchResult1.FirstOrDefault(item => item.Id == testBoosterMatches[1].Id));
        }


        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterMatch_BoosterIdIsNull()
        {
            await _dal.GameData.BoosterMatch.Searcher.GetBoosterMatch(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterMatch_BoosterIdIsEmpty()
        {
            await _dal.GameData.BoosterMatch.Searcher.GetBoosterMatch(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindBoosterMatches_SkipLessThenNull()
        {
            await _dal.GameData.BoosterMatch.Searcher.FindBoosterMatches(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindBoosterMatches_TakeLessThenNull()
        {
            await _dal.GameData.BoosterMatch.Searcher.FindBoosterMatches(skip: -1);
        }

        #endregion

        #region private methods

        private async Task<List<BoosterMatch>> CreateTestsBoosterMatches()
        {
            var testBoosterMatch1 = new BoosterMatch
            {
                BoosterId = "testBoosterId1",
                OrderId = "testOrderId1",
                IsWin = true,
                ChampionId = 1,
                Tier = Tier.Challenger,
                Lane = Lane.Mid,
                GameCreation = new DateTime(2020, 4, 13)
            };

            var testBoosterMatch2 = new BoosterMatch
            {
                BoosterId = "testBoosterId1",
                OrderId = "testOrderId1",
                IsWin = true,
                ChampionId = 2,
                Tier = Tier.Grandmaster,
                Lane = Lane.Jungle,
                GameCreation = new DateTime(2020, 4, 13)
            };

            var testBoosterMatch3 = new BoosterMatch
            {
                BoosterId = "testBoosterId1",
                OrderId = "testOrderId1",
                IsWin = true,
                ChampionId = 5,
                Tier = Tier.Diamond,
                Lane = Lane.Mid,
                GameCreation = new DateTime(2020, 4, 13)
            };

            var testBoosterMatch4 = new BoosterMatch
            {
                BoosterId = "testBoosterId2",
                OrderId = "testOrderId2",
                IsWin = false,
                ChampionId = 1,
                Tier = Tier.Challenger,
                Lane = Lane.Mid,
                GameCreation = new DateTime(2020, 4, 13)
            };

            testBoosterMatch1.Id = await _dal.GameData.BoosterMatch.Manager.AddOrUpdate(testBoosterMatch1);
            testBoosterMatch2.Id = await _dal.GameData.BoosterMatch.Manager.AddOrUpdate(testBoosterMatch2);
            testBoosterMatch3.Id = await _dal.GameData.BoosterMatch.Manager.AddOrUpdate(testBoosterMatch3);
            testBoosterMatch4.Id = await _dal.GameData.BoosterMatch.Manager.AddOrUpdate(testBoosterMatch4);

            await ((BoosterMatchDataManager)_dal.GameData.BoosterMatch.Manager).RefreshIndexAsync();

            return new List<BoosterMatch>
            {
                testBoosterMatch1,
                testBoosterMatch2,
                testBoosterMatch3,
                testBoosterMatch4
            };
        }

        #endregion
    }
}
