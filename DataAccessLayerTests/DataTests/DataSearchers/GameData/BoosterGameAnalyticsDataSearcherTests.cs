﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.GameData.Mapping;
using DataAccessLayerAbstraction.GameData.Mapping.Models;
using DataAccessLayerImplementation.GameData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.GameData
{
    [TestClass]
    [TestCategory("BoosterGameAnalyticsTests")]
    public class BoosterGameAnalyticsDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.GameDataIndexSettings.BoosterGameAnalyticsIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.GameDataIndexSettings.BoosterGameAnalyticsIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetBoosterGameAnalytics_Method()
        {
            List<BoosterGameAnalytics> testBoosterGameAnalyticses = await CreateTestBoosterGameAnalytics();

            BoosterGameAnalytics getResult1 = await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameAnalytics(testBoosterGameAnalyticses[0].BoosterId);
            BoosterGameAnalytics getResult2 = await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameAnalytics(testBoosterGameAnalyticses[2].BoosterId);

            Assert.IsNotNull(getResult1);
            Assert.IsNotNull(getResult2);
            Assert.AreEqual(testBoosterGameAnalyticses[0].BoosterId, getResult1.BoosterId);
            Assert.AreEqual(testBoosterGameAnalyticses[2].BoosterId, getResult2.BoosterId);
        }

        [TestMethod]
        public async Task Test_GetBoosterGameStats_Method()
        {
            List<BoosterGameAnalytics> testBoosterGameAnalyticses = await CreateTestBoosterGameAnalytics();

            List<GamesStats> getResult1 = (await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameStats(
                testBoosterGameAnalyticses[0].BoosterId,
                includeTiers: new List<Tier>
                {
                    Tier.Silver,
                    Tier.Gold,
                },
                includeChampionIds: new List<string>
                {
                    "championId3",
                    "championId4"
                }
            )).ToList();

            List<GamesStats> getResult2 = (await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameStats(
                testBoosterGameAnalyticses[2].BoosterId,
                includeTiers: new List<Tier>
                {
                    Tier.Iron,
                    Tier.Bronze,
                },
                includeChampionIds: new List<string>
                {
                    "championId1",
                    "championId2"
                }
            )).ToList();

            Assert.AreEqual(4, getResult1.Count);

            Assert.IsTrue(getResult1.Exists(item => item.TotalGames == 3));
            Assert.IsTrue(getResult1.Exists(item => item.TotalGames == 4));
            Assert.IsTrue(getResult1.Exists(item => item.TotalGames == 7));
            Assert.IsTrue(getResult1.Exists(item => item.TotalGames == 8));

            Assert.AreEqual(4, getResult2.Count);
            Assert.IsTrue(getResult2.Exists(item => item.TotalGames == 31));
            Assert.IsTrue(getResult2.Exists(item => item.TotalGames == 32));
            Assert.IsTrue(getResult2.Exists(item => item.TotalGames == 35));
            Assert.IsTrue(getResult2.Exists(item => item.TotalGames == 36));
        }

        [TestMethod]

        public async Task Test_FindBoosterGameAnalytics_FindBoosterGameAnalyticsCount_Methods()
        {
            List<BoosterGameAnalytics> testBoosterGameAnalyticses = await CreateTestBoosterGameAnalytics();

            List<BoosterGameAnalytics> getResult1 = (await _dal.GameData.BoosterGameAnalytics.Searcher.FindBoosterGameAnalytics(
                new List<string>
                {
                    testBoosterGameAnalyticses[0].BoosterId,
                    testBoosterGameAnalyticses[2].BoosterId
                },
                new List<string>()
                {
                    "championId1",
                    "championId5"
                }
            )).ToList();

            uint countResult1 = await _dal.GameData.BoosterGameAnalytics.Searcher.FindBoosterGameAnalyticsCount(
                new List<string>
                {
                    testBoosterGameAnalyticses[0].BoosterId,
                    testBoosterGameAnalyticses[2].BoosterId
                },
                new List<string>()
                {
                    "championId1",
                    "championId5"
                }
            );

            Assert.AreEqual(1u, countResult1);
            Assert.IsNotNull(getResult1.FirstOrDefault(item => item.BoosterId == testBoosterGameAnalyticses[0].BoosterId));
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterGameAnalytics_BoosterIdIsNull()
        {
            await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameAnalytics(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterGameAnalytics_BoosterIdIsEmpty()
        {
            await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameAnalytics(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterGameStats_BoosterIdIsNull()
        {
            await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameStats(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterGameStats_BoosterIdIsEmpty()
        {
            await _dal.GameData.BoosterGameAnalytics.Searcher.GetBoosterGameStats(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindBoosterGameAnalytics_SkipLessThenNull()
        {
            await _dal.GameData.BoosterGameAnalytics.Searcher.FindBoosterGameAnalytics(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindBoosterGameAnalytics_TakeLessThenNull()
        {
            await _dal.GameData.BoosterGameAnalytics.Searcher.FindBoosterGameAnalytics(take: -1);
        }

        #endregion

        #region private methods

        private async Task<List<BoosterGameAnalytics>> CreateTestBoosterGameAnalytics()
        {
            var testAnalytics1 = new BoosterGameAnalytics
            {
                BoosterId = "testBoosterId1",
                LastUpdate = DateTime.UtcNow,
                ChampionStats = new Dictionary<string, GamesStats>
                {
                    {"championId1", new GamesStats {TotalGames = 1}},
                    {"championId2", new GamesStats {TotalGames = 2}},
                    {"championId3", new GamesStats {TotalGames = 3}},
                    {"championId4", new GamesStats {TotalGames = 4}},
                    {"championId5", new GamesStats {TotalGames = 4777}}
                },
                TierStats = new Dictionary<Tier, GamesStats>
                {
                    {Tier.Iron, new GamesStats {TotalGames = 5}},
                    {Tier.Bronze, new GamesStats {TotalGames = 6}},
                    {Tier.Silver, new GamesStats {TotalGames = 7}},
                    {Tier.Gold, new GamesStats {TotalGames = 8}},
                }
            };

            var testAnalytics2 = new BoosterGameAnalytics
            {
                BoosterId = "testBoosterId2",
                LastUpdate = DateTime.UtcNow,
                ChampionStats = new Dictionary<string, GamesStats>
                {
                    {"championId1", new GamesStats {TotalGames = 21}},
                    {"championId2", new GamesStats {TotalGames = 22}},
                    {"championId3", new GamesStats {TotalGames = 23}},
                    {"championId4", new GamesStats {TotalGames = 24}}
                },
                TierStats = new Dictionary<Tier, GamesStats>
                {
                    {Tier.Iron, new GamesStats {TotalGames = 25}},
                    {Tier.Bronze, new GamesStats {TotalGames = 26}},
                    {Tier.Silver, new GamesStats {TotalGames = 27}},
                    {Tier.Gold, new GamesStats {TotalGames = 28}},
                }
            };

            var testAnalytics3 = new BoosterGameAnalytics
            {
                BoosterId = "testBoosterId3",
                LastUpdate = DateTime.UtcNow,
                ChampionStats = new Dictionary<string, GamesStats>
                {
                    {"championId1", new GamesStats {TotalGames = 31}},
                    {"championId2", new GamesStats {TotalGames = 32}},
                    {"championId3", new GamesStats {TotalGames = 33}},
                    {"championId4", new GamesStats {TotalGames = 34}}
                },
                TierStats = new Dictionary<Tier, GamesStats>
                {
                    {Tier.Iron, new GamesStats {TotalGames = 35}},
                    {Tier.Bronze, new GamesStats {TotalGames = 36}},
                    {Tier.Silver, new GamesStats {TotalGames = 37}},
                    {Tier.Gold, new GamesStats {TotalGames = 38}},
                }
            };

            await _dal.GameData.BoosterGameAnalytics.Manager.AddOrUpdate(testAnalytics1);
            await _dal.GameData.BoosterGameAnalytics.Manager.AddOrUpdate(testAnalytics2);
            await _dal.GameData.BoosterGameAnalytics.Manager.AddOrUpdate(testAnalytics3);

            await ((BoosterGameAnalyticsDataManager)_dal.GameData.BoosterGameAnalytics.Manager).RefreshIndexAsync();

            return new List<BoosterGameAnalytics>
            {
                testAnalytics1,
                testAnalytics2,
                testAnalytics3
            };
        }

        #endregion
    }
}