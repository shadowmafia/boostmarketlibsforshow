﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.ActionLogClasses.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketSystemData
{
    [TestClass]
    [TestCategory("ActionLogTests")]
    public class ActionLogDataSearchTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.ActionLogIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.ActionLogIndexName
            );
        }

        [TestMethod]
        public async Task Test_FindActionLog_FindActionLogCount_MethodsTests()
        {
            await _dal.MarketSystemData.ActionLog.Manager.CreateActionLog(new ActionLog
            {
                Timestamp = DateTime.Now.Ticks,
                ActionType = LogActionType.Create,
                Actions = new Dictionary<string, HttpAction>
                {
                    {
                        "qwe", new HttpAction {
                            Type = ActionType.Request,
                            Body = Encoding.ASCII.GetBytes("asdasdasaaaaa"),
                            Cookies = new Dictionary<string, string>
                            {
                                {"asd","asd"}
                            }
                    }
                    }
                },
                Caller = "1",
                Initiator = new LogInitiator
                {
                    InitiatorId = "a1",
                    Type = LogInitiatorType.System
                },
                Level = Level.Debug,
                Message = "asdasd",
                NodeName = "asd",
                PodName = "asdm",
                PodNamespace = "asd",
                PodServiceAccount = "asd",
                Stacktrace = "asd",
                Target = new LogTarget
                {
                    Type = LogTargetType.CmsUser,
                    TargetId = "123"
                }
            });

            Assert.IsTrue(true);
            //List<ActionLog> testActionLogs = await CreateTestActionLogs();
            //await ((ActionLogDataManager)_dal.MarketSystemData.ActionLog.Manager).RefreshIndexAsync();

            //List<ActionLog> searchResult1 = (await _dal.MarketSystemData.ActionLog.Searcher.FindActionLogs(
            //    actionInitiatorId: testActionLogs[0].ActionInitiatorId,
            //    actionTargetId: testActionLogs[0].ActionTargetId,
            //    messageContain: "was",
            //    dateTimeFrom: testActionLogs[0].DateTime,
            //    dateTimeTo: testActionLogs[1].DateTime,
            //    actionInitiators: new List<LogInitiator>
            //    {
            //        LogInitiator.Admin,
            //        LogInitiator.Booster
            //    },
            //    actionTypes: new List<LogActionType>
            //    {
            //        LogActionType.Create,
            //        LogActionType.Update
            //    },
            //    skip: 1
            //)).ToList();

            //uint countResult1 = await _dal.MarketSystemData.ActionLog.Searcher.FindActionLogsCount(
            //    actionInitiatorId: testActionLogs[0].ActionInitiatorId,
            //    actionTargetId: testActionLogs[0].ActionTargetId,
            //    messageContain: "was",
            //    dateTimeFrom: testActionLogs[0].DateTime,
            //    dateTimeTo: testActionLogs[1].DateTime,
            //    actionInitiators: new List<LogInitiator>
            //    {
            //        LogInitiator.Admin,
            //        LogInitiator.Booster
            //    },
            //    actionTypes: new List<LogActionType>
            //    {
            //        LogActionType.Create,
            //        LogActionType.Update
            //    }
            //);

            //Assert.AreEqual(2u, countResult1);
            //Assert.IsNotNull(searchResult1);
            //Assert.AreEqual(1, searchResult1.Count);
            //Assert.IsNotNull(searchResult1.FirstOrDefault(item => item.Id == testActionLogs[1].Id));
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindActionLogs_SkipLessThenNull()
        {
            await _dal.MarketSystemData.ActionLog.Searcher.FindActionLogs(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindActionLogs_TakeLessThenNull()
        {
            await _dal.MarketSystemData.ActionLog.Searcher.FindActionLogs(skip: -1);
        }

        #endregion

        #region private methods

        private async Task<List<ActionLog>> CreateTestActionLogs()
        {
            //var actionLog1 = new ActionLog
            //{
            //    ActionInitiator = LogInitiator.Admin,
            //    ActionInitiatorId = "adminId1",
            //    ActionTarget = LogTarget.Booster,
            //    ActionTargetId = "boosterId1",
            //    DateTime = new DateTime(2020, 10, 11),
            //    LogActionType = LogActionType.Create,
            //    Message = "Booster was created"
            //};

            //var actionLog2 = new ActionLog
            //{
            //    ActionInitiator = LogInitiator.Admin,
            //    ActionInitiatorId = "adminId1",
            //    ActionTarget = LogTarget.Booster,
            //    ActionTargetId = "boosterId1",
            //    DateTime = new DateTime(2020, 10, 12),
            //    LogActionType = LogActionType.Update,
            //    Message = "Booster was changed"
            //};

            //var actionLog3 = new ActionLog
            //{
            //    ActionInitiator = LogInitiator.System,
            //    ActionTarget = LogTarget.Booster,
            //    DateTime = new DateTime(2020, 10, 13),
            //    LogActionType = LogActionType.Unknown,
            //    Message = "Transaction error"
            //};

            //actionLog1.Id = await _dal.MarketSystemData.ActionLog.Manager.CreateActionLog(actionLog1);
            //actionLog2.Id = await _dal.MarketSystemData.ActionLog.Manager.CreateActionLog(actionLog2);
            //actionLog3.Id = await _dal.MarketSystemData.ActionLog.Manager.CreateActionLog(actionLog3);

            return new List<ActionLog>()
            {
                //actionLog1,
                //actionLog2,
                //actionLog3
            };
        }

        #endregion
    }
}
