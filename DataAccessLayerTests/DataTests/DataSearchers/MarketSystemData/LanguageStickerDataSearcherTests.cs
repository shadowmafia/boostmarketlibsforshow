﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerImplementation.MarketSystemData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketSystemData
{
    [TestClass]
    [TestCategory("LanguageStickerTests")]
    public class LanguageStickerDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.LanguageStickerIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.LanguageStickerIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetLanguageSticker_FindLanguageSticker_FindLanguageStickerCount_Methods()
        {
            List<LanguageSticker> stickers = await CreateTestLanguageStickers();
            await ((LanguageStickerDataManager)_dal.MarketSystemData.LanguageSticker.Manager).RefreshIndexAsync();

            LanguageSticker getStickerResult1 = await _dal.MarketSystemData.LanguageSticker.Searcher.GetLanguageSticker(stickers[0].Id);
            LanguageSticker getStickerResult2 = await _dal.MarketSystemData.LanguageSticker.Searcher.GetLanguageSticker(stickers[1].Id);

            List<LanguageSticker> findStickersResult1 = (await _dal.MarketSystemData.LanguageSticker.Searcher.FindLanguageSticker(
                stickers[0].LanguageId,
                stickers[0].StickerName,
                "русском"
            )).ToList();

            List<LanguageSticker> findStickersResult2 = (await _dal.MarketSystemData.LanguageSticker.Searcher.FindLanguageSticker(
                stickerName: stickers[0].StickerName,
                skip: 1
            )).ToList();

            uint findStickersResult2Count = await _dal.MarketSystemData.LanguageSticker.Searcher.FindLanguageStickerCount(
                stickerName: stickers[0].StickerName
            );

            List<LanguageSticker> findStickersResult3 = (await _dal.MarketSystemData.LanguageSticker.Searcher.FindLanguageSticker(
                stickerContentContain: "текст"
            )).ToList();

            Assert.AreEqual(stickers[0].Id, getStickerResult1.Id);
            Assert.AreEqual(stickers[1].Id, getStickerResult2.Id);
            Assert.AreEqual(1, findStickersResult1.Count);
            Assert.IsNotNull(findStickersResult1.FirstOrDefault(item => item.Id == stickers[0].Id));
            Assert.AreEqual(1, findStickersResult2.Count);

            Assert.IsNotNull(findStickersResult2.FirstOrDefault(item => item.Id == stickers[1].Id));
            Assert.AreEqual(2u, findStickersResult2Count);

            Assert.AreEqual(2, findStickersResult3.Count);
            Assert.IsNotNull(findStickersResult3.FirstOrDefault(item => item.Id == stickers[0].Id));
            Assert.IsNotNull(findStickersResult3.FirstOrDefault(item => item.Id == stickers[2].Id));

        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetLanguageSticker_StickerIdIsNull()
        {
            await _dal.MarketSystemData.LanguageSticker.Searcher.GetLanguageSticker(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetLanguageSticker_StickerIdIsEmpty()
        {
            await _dal.MarketSystemData.LanguageSticker.Searcher.GetLanguageSticker(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindLanguageSticker_SkipLessThenNull()
        {
            await _dal.MarketSystemData.LanguageSticker.Searcher.FindLanguageSticker(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindLanguageSticker_TakeLessThenNull()
        {
            await _dal.MarketSystemData.LanguageSticker.Searcher.FindLanguageSticker(skip: -1);
        }

        #endregion

        #region private methods

        private async Task<List<LanguageSticker>> CreateTestLanguageStickers()
        {
            LanguageSticker sticker1 = new LanguageSticker
            {
                LanguageId = "1",
                StickerName = "textRes_justText",
                StickerContent = "Просто текст на русском"
            };

            LanguageSticker sticker2 = new LanguageSticker
            {
                LanguageId = "2",
                StickerName = "textRes_justText",
                StickerContent = "Just text on english"
            };

            LanguageSticker sticker3 = new LanguageSticker
            {
                LanguageId = "1",
                StickerName = "textRes_testText",
                StickerContent = "Тестовый текст"
            };

            sticker1.Id = await _dal.MarketSystemData.LanguageSticker.Manager.AddOrUpdateLanguageSticker(sticker1);
            sticker2.Id = await _dal.MarketSystemData.LanguageSticker.Manager.AddOrUpdateLanguageSticker(sticker2);
            sticker3.Id = await _dal.MarketSystemData.LanguageSticker.Manager.AddOrUpdateLanguageSticker(sticker3);

            return new List<LanguageSticker>
            {
                sticker1,
                sticker2,
                sticker3
            };
        }

        #endregion
    }
}
