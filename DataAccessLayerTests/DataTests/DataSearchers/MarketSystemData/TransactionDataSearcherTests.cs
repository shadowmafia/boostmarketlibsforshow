﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerAbstraction.MarketSystemData.Mapping.TransactionClasses;
using DataAccessLayerImplementation.MarketSystemData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketSystemData
{
    [TestClass]
    [TestCategory("TransactionTests")]
    public class TransactionDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.TransactionIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.TransactionIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetTransaction_Method()
        {
            List<Transaction> transactions = await CreateTestTransactions();
            await ((TransactionDataManager)_dal.MarketSystemData.Transaction.Manager).RefreshIndexAsync();

            Transaction getResult1 = await _dal.MarketSystemData.Transaction.Searcher.GetTransaction(transactions[0].Id);
            Transaction getResult2 = await _dal.MarketSystemData.Transaction.Searcher.GetTransaction(transactions[2].Id);

            Assert.IsNotNull(getResult1);
            Assert.AreEqual(transactions[0].Id, getResult1.Id);
            Assert.IsNotNull(getResult2);
            Assert.AreEqual(transactions[2].Id, getResult2.Id);
        }

        [TestMethod]
        public async Task Test_FindTransactions_FindTransactionCount_Methods()
        {
            List<Transaction> transactions = await CreateTestTransactions();
            await ((TransactionDataManager)_dal.MarketSystemData.Transaction.Manager).RefreshIndexAsync();

            List<Transaction> searchResult1 = (await _dal.MarketSystemData.Transaction.Searcher.FindTransactions(
                transactions[1].BindedObjectId,
                "transaction",
                transactions[1].Value,
                transactions[2].Value,
                transactions[1].DateTime,
                transactions[2].DateTime,
                new List<TransactionType>
                {
                    TransactionType.MoneyTransfer,
                    TransactionType.MoneyDebit
                },
                skip: 1
            )).ToList();

            uint countResult1 = await _dal.MarketSystemData.Transaction.Searcher.FindTransactionsCount(
                transactions[1].BindedObjectId,
                "transaction",
                transactions[1].Value,
                transactions[2].Value,
                transactions[1].DateTime,
                transactions[2].DateTime,
                new List<TransactionType>
                {
                    TransactionType.MoneyTransfer,
                    TransactionType.MoneyDebit
                }
            );

            Assert.AreEqual(2u, countResult1);
            Assert.IsNotNull(searchResult1);
            Assert.AreEqual(1, searchResult1.Count);
            Assert.IsNotNull(searchResult1.FirstOrDefault(item => item.Id == transactions[2].Id));
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetTransaction_TransactionIdIsNull()
        {
            await _dal.MarketSystemData.Transaction.Searcher.GetTransaction(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetTransaction_TransactionIdIsEmpty()
        {
            await _dal.MarketSystemData.Transaction.Searcher.GetTransaction(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindTransactions_SkipLessThenNull()
        {
            await _dal.MarketSystemData.Transaction.Searcher.FindTransactions(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindTransactions_TakeLessThenNull()
        {
            await _dal.MarketSystemData.Transaction.Searcher.FindTransactions(skip: -1);
        }

        #endregion

        #region private methods

        private async Task<List<Transaction>> CreateTestTransactions()
        {
            var testTransaction1 = new Transaction
            {
                BindedObjectId = "testOrderId1",
                DateTime = new DateTime(2020, 2, 10),
                TransactionComment = "just transaction comment",
                TransactionType = TransactionType.OrderPayment,
                Value = 300
            };

            var testTransaction2 = new Transaction
            {
                BindedObjectId = "testBoosterId1",
                DateTime = new DateTime(2020, 2, 10),
                TransactionComment = "just transaction comment",
                TransactionType = TransactionType.MoneyTransfer,
                Value = 100
            };

            var testTransaction3 = new Transaction
            {
                BindedObjectId = "testBoosterId1",
                DateTime = new DateTime(2020, 2, 12),
                TransactionComment = "just asdasd transaction asdasd comment",
                TransactionType = TransactionType.MoneyDebit,
                Value = 100
            };

            testTransaction1.Id = await _dal.MarketSystemData.Transaction.Manager.CreateTransaction(testTransaction1);
            testTransaction2.Id = await _dal.MarketSystemData.Transaction.Manager.CreateTransaction(testTransaction2);
            testTransaction3.Id = await _dal.MarketSystemData.Transaction.Manager.CreateTransaction(testTransaction3);

            return new List<Transaction>()
            {
               testTransaction1,
               testTransaction2,
               testTransaction3
            };
        }

        #endregion
    }
}