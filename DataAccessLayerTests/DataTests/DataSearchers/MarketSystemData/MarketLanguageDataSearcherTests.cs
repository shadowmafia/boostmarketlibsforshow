﻿using DataAccessLayerAbstraction.MarketSystemData.Mapping;
using DataAccessLayerImplementation.MarketSystemData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketSystemData
{
    [TestClass]
    [TestCategory("MarketLanguageTests")]
    public class MarketLanguageDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.MarketLanguageIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketSystemDataIndexSettings.MarketLanguageIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetMarketLanguage_GetAllLanguages_Methods()
        {
            List<MarketLanguage> testLanguages = await CreateTestMarketLanguages();
            await ((MarketLanguageDataManager)_dal.MarketSystemData.MarketLanguage.Manager).RefreshIndexAsync();

            MarketLanguage searchById = await _dal.MarketSystemData.MarketLanguage.Searcher.GetMarketLanguage(testLanguages[0].Id);
            MarketLanguage searchByName = await _dal.MarketSystemData.MarketLanguage.Searcher.GetMarketLanguage(languageName: testLanguages[1].LanguageName);
            MarketLanguage searchByLocale = await _dal.MarketSystemData.MarketLanguage.Searcher.GetMarketLanguage(locale: testLanguages[2].Locale);

            List<MarketLanguage> getAllLanguages = (await _dal.MarketSystemData.MarketLanguage.Searcher.GetAllLanguages()).ToList();

            Assert.AreEqual(testLanguages[0].Id, searchById.Id);
            Assert.AreEqual(testLanguages[1].LanguageName, searchByName.LanguageName);
            Assert.AreEqual(testLanguages[2].Locale, searchByLocale.Locale);
            Assert.IsTrue(getAllLanguages.Exists(item => item.Id == testLanguages[0].Id));
            Assert.IsTrue(getAllLanguages.Exists(item => item.Id == testLanguages[1].Id));
            Assert.IsTrue(getAllLanguages.Exists(item => item.Id == testLanguages[2].Id));
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_GetMarketLanguage_Methods_WithOutParams()
        {
            await _dal.MarketSystemData.MarketLanguage.Searcher.GetMarketLanguage();
        }

        #endregion

        #region private methods

        private async Task<List<MarketLanguage>> CreateTestMarketLanguages()
        {
            MarketLanguage testMarketLanguageRu = new MarketLanguage
            {
                Id = "1",
                Locale = "rus",
                LanguageName = "Русский"
            };

            MarketLanguage testMarketLanguageEng = new MarketLanguage
            {
                Id = "2",
                Locale = "eng",
                LanguageName = "English"
            };

            MarketLanguage testMarketLanguageDeu = new MarketLanguage
            {
                Id = "3",
                Locale = "deu",
                LanguageName = "Deutsch"
            };

            await _dal.MarketSystemData.MarketLanguage.Manager.AddOrUpdateMarketLanguage(testMarketLanguageRu);
            await _dal.MarketSystemData.MarketLanguage.Manager.AddOrUpdateMarketLanguage(testMarketLanguageEng);
            await _dal.MarketSystemData.MarketLanguage.Manager.AddOrUpdateMarketLanguage(testMarketLanguageDeu);

            return new List<MarketLanguage>
            {
                testMarketLanguageRu,
                testMarketLanguageEng,
                testMarketLanguageDeu
            };
        }

        #endregion
    }
}