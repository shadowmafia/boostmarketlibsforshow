﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using DataAccessLayerImplementation.MarketData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketData
{
    [TestClass]
    [TestCategory("CmsUserTests")]
    public class CmsUserDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CmsUserIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CmsUserIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetCmsUser_Method()
        {
            List<CmsUser> testCmsUsers = await CreateTestCmsUsers();

            CmsUser returnedWithFakeId = await _dal.MarketData.CmsUser.Searcher.GetCmsUser("fakeId");
            CmsUser returnedCmsUser0 = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(testCmsUsers[0].Id);
            CmsUser returnedCmsUser1 = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(email: testCmsUsers[1].DefaultEmail.Email.ToLower());

            Assert.AreEqual(testCmsUsers[0].Id, returnedCmsUser0.Id);
            Assert.AreEqual(testCmsUsers[1].Id, returnedCmsUser1.Id);
            Assert.IsNull(returnedWithFakeId);
        }

        [TestMethod]
        public async Task Test_FindCmsUsers_FindCmsUsersCount_Method()
        {
            List<CmsUser> testCmsUsers = await CreateTestCmsUsers();

            List<CmsUser> searchResult1 = (await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(
                ids: testCmsUsers.Select(booster => booster.Id).ToList(),
                isDiscordConnected: true,
                isUseDiscordNotification: true,
                userTypes: new List<CmsUserType>
                {
                    CmsUserType.Admin,
                    CmsUserType.Booster,
                    CmsUserType.Manager
                },
                skip: 1
            )).ToList();

            uint countResult1 = await _dal.MarketData.CmsUser.Searcher.FindCmsUsersCount(
                ids: testCmsUsers.Select(booster => booster.Id).ToList(),
                isDiscordConnected: true,
                isUseDiscordNotification: true,
                userTypes: new List<CmsUserType>
                {
                    CmsUserType.Admin,
                    CmsUserType.Booster,
                    CmsUserType.Manager
                }
            );

            List<CmsUser> searchResult2 = (await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(
                isDiscordConnected: true,
                isUseDiscordNotification: true,
                userTypes: new List<CmsUserType>
                {
                    CmsUserType.Admin,
                }
            )).ToList();

            Assert.AreEqual(1, searchResult1.Count);
            Assert.IsNotNull(searchResult1.FirstOrDefault(cmsUser => cmsUser.Id == testCmsUsers[2].Id));
            Assert.AreEqual(2u, countResult1);

            Assert.IsNotNull(searchResult2.FirstOrDefault(cmsUser => cmsUser.Id == testCmsUsers[0].Id));
        }


        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_GetCmsUserMethod_AllParamsIsNull()
        {
            await _dal.MarketData.CmsUser.Searcher.GetCmsUser();
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindCmsUsers_SkipLessThenNull()
        {
            await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindCmsUsers_TakeLessThenNull()
        {
            await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(skip: -1);
        }

        #endregion

        #region private methods

        private async Task<List<CmsUser>> CreateTestCmsUsers()
        {

            var testCmsUser1 = new CmsUser()
            {
                CmsUserType = CmsUserType.Admin,
                DiscordTag = "Admin#2294",
                IsDiscordConnected = true,
                DefaultEmail = new DefaultEmail
                {
                    Email = "Admin@mail.com",
                    IsEmailConfirmed = true,
                },
                IsUseDiscordNotification = true,
                Name = "ValeraAdmin",
                Password = "hasedAdminPassword",
                RegistrationDate = DateTime.UtcNow,
                ProfileImageUrl = "imgUrl"
            };

            var testCmsUser2 = new CmsUser()
            {
                CmsUserType = CmsUserType.Manager,
                DiscordTag = "Manager#2294",
                IsDiscordConnected = true,
                DefaultEmail = new DefaultEmail
                {
                    Email = "Manager@mail.com",
                    IsEmailConfirmed = true,
                },
                IsUseDiscordNotification = false,
                Name = "VasiaManager",
                Password = "hasedManagerPassword",
                RegistrationDate = DateTime.UtcNow,
                ProfileImageUrl = "imgUrl2"
            };

            var testCmsUser3 = new CmsUser()
            {
                CmsUserType = CmsUserType.Booster,
                DiscordTag = "Booster#2294",
                IsDiscordConnected = true,
                DefaultEmail = new DefaultEmail
                {
                    Email = "Booster@mail.com",
                    IsEmailConfirmed = true,
                },
                IsUseDiscordNotification = true,
                Name = "RomaBooster",
                Password = "hasedBoosterPassword",
                RegistrationDate = DateTime.UtcNow,
                ProfileImageUrl = "imgUrl"
            };

            testCmsUser1.Id = await _dal.MarketData.CmsUser.Manager.AddOrUpdate(testCmsUser1);
            testCmsUser2.Id = await _dal.MarketData.CmsUser.Manager.AddOrUpdate(testCmsUser2);
            testCmsUser3.Id = await _dal.MarketData.CmsUser.Manager.AddOrUpdate(testCmsUser3);

            await ((CmsUserDataManager)_dal.MarketData.CmsUser.Manager).RefreshIndexAsync();

            return new List<CmsUser>
            {
                testCmsUser1,
                testCmsUser2,
                testCmsUser3
            };
        }

        #endregion
    }
}
