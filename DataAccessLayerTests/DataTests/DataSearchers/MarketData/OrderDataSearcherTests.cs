﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses;
using DataAccessLayerImplementation.MarketData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketData
{
    [TestCategory("OrderTests")]
    [TestClass]
    public class OrderDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.OrderIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CmsUserIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetOrder_Method()
        {
            Order testOrder1 = CreateTestOrder();
            Order testOrder2 = CreateTestOrder();

            testOrder2.Type = OrderType.ChampionMastery;
            testOrder2.Details = new ChampionMastery()
            {
                TokensNeeded = 5,
                ChampionId = "2294",
                CurrentTier = ChampionMasteryTier.Tier4,
                FinalTier = ChampionMasteryTier.Tier7,
                AdditionalServices = new List<AdditionalService>()
                {
                    new AdditionalService()
                    {
                        AdditionalServiceType = AdditionalServiceType.PlusOneWin
                    }
                },
            };

            string order1Id = await _dal.MarketData.Order.Manager.AddOrUpdate(testOrder1);
            string order2Id = await _dal.MarketData.Order.Manager.AddOrUpdate(testOrder2);

            await ((OrderDataManager)_dal.MarketData.Order.Manager).RefreshIndexAsync();

            Order searchResult1 = await _dal.MarketData.Order.Searcher.GetOrder(order1Id);
            Order searchResult2 = await _dal.MarketData.Order.Searcher.GetOrder(order2Id);

            Assert.AreEqual(order1Id, searchResult1.Id);
            Assert.AreEqual(order2Id, searchResult2.Id);
            Assert.IsNotNull(searchResult1.Details);
            Assert.IsNotNull(searchResult2.Details);
        }

        [TestMethod]
        public async Task Test_FindOrder_FindOrderCount_Methods()
        {
            #region Create orders objects

            var testOrder1 = CreateTestOrder();
            var testOrder2 = new Order()
            {
                AccountLogin = "testLogin2",
                AccountName = "testName2",
                Price = 1000,
                CustomerId = "testCustomerId1",
                AccountPassword = "testPassword2",
                BoosterId = "testBoosterId2",
                Status = OrderStatus.Completed,
                PaymentState = PaymentState.Paid,
                CheckoutTime = new DateTime(2020, 3, 12, 23, 23, 10),
                CmsOrderComments = new List<CmsOrderComment>()
                {
                    new CmsOrderComment()
                    {
                        CmsUserIndex = "testUserIndex1",
                        DateTime = DateTime.UtcNow,
                        Message = "testMessage 1 bla"
                    }
                },
                CompletionTime = new DateTime(2020, 5, 14, 23, 23, 10),
                CustomerComment = "Want booster was play after 12.00",

                Type = OrderType.ChampionMastery,
                Details = new ChampionMastery()
                {
                    ChampionId = "2294",
                    CurrentTier = ChampionMasteryTier.Tier3,
                    FinalTier = ChampionMasteryTier.Tier7,
                    TokensNeeded = 2,
                    AdditionalServices = new List<AdditionalService>()
                    {
                        new AdditionalService()
                        {
                            AdditionalServiceType = AdditionalServiceType.SpecificChampions,
                            IsPercent = false,
                            Markup = 20
                        }
                    }
                }
            };

            #endregion

            string order1Id = await _dal.MarketData.Order.Manager.AddOrUpdate(testOrder1);
            string order2Id = await _dal.MarketData.Order.Manager.AddOrUpdate(testOrder2);

            await ((OrderDataManager)_dal.MarketData.Order.Manager).RefreshIndexAsync();

            IList<Order> orderSearchResult1 = await _dal.MarketData.Order.Searcher.FindOrders(skip: 1);
            IList<Order> orderSearchResult2 = await _dal.MarketData.Order.Searcher.FindOrders(
                orderType: OrderType.ChampionMastery,
                orderStatus: OrderStatus.Completed,
                paymentState: PaymentState.Paid,
                customerId: testOrder2.CustomerId,
                boosterId: testOrder2.BoosterId,
                accountName: testOrder2.AccountName
            );

            uint searchResult1Count = await _dal.MarketData.Order.Searcher.FindOrdersCount();
            uint searchResult2Count = await _dal.MarketData.Order.Searcher.FindOrdersCount(
                orderType: OrderType.ChampionMastery,
                orderStatus: OrderStatus.Completed,
                paymentState: PaymentState.Paid,
                customerId: testOrder2.CustomerId,
                boosterId: testOrder2.BoosterId,
                accountName: testOrder2.AccountName
            );

            Assert.AreEqual(1, orderSearchResult1.Count);
            Assert.AreEqual(1, orderSearchResult2.Count);
            Assert.AreEqual(2u, searchResult1Count);
            Assert.AreEqual(1u, searchResult2Count);
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetOrder_OrderIdIsNull()
        {
            await _dal.MarketData.Order.Searcher.GetOrder(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetOrder_OrderIdIsEmpty()
        {
            await _dal.MarketData.Order.Searcher.GetOrder(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindOrders_SkipLessThenNull()
        {
            await _dal.MarketData.Order.Searcher.FindOrders(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindOrders_TakeLessThenNull()
        {
            await _dal.MarketData.Order.Searcher.FindOrders(skip: -1);
        }

        #endregion

        #region private methods

        private Order CreateTestOrder()
        {
            return new Order()
            {
                AccountLogin = "testLogin",
                AccountName = "testName",
                AccountPassword = "testPassword",
                BoosterId = "testBoosterId",
                Status = OrderStatus.InProcessing,
                PaymentState = PaymentState.Paid,
                CheckoutTime = new DateTime(2020, 3, 12, 23, 23, 10),
                CmsOrderComments = new List<CmsOrderComment>()
                {
                    new CmsOrderComment()
                    {
                        CmsUserIndex = "testUserIndex1",
                        DateTime = DateTime.UtcNow,
                        Message = "testMessage 1 bla"
                    },
                    new CmsOrderComment()
                    {
                        CmsUserIndex = "testUserIndex2",
                        DateTime = DateTime.UtcNow,
                        Message = "testMessage 2 bla bla"
                    }
                },
                CompletionTime = new DateTime(2020, 5, 14, 23, 23, 10),
                CustomerComment = "Want booster was play after 12.00",
                CustomerId = "testCustomerId1",
                Type = OrderType.DuoLeagueBoosting,
                Details = new DuoLeagueBoosting()
                {
                    AdditionalServices = new List<AdditionalService>()
                    {
                        new AdditionalService()
                        {
                            AdditionalServiceType = AdditionalServiceType.PlusOneWin,
                            IsPercent = false,
                            Markup = 10
                        },
                        new AdditionalService()
                        {
                            AdditionalServiceType = AdditionalServiceType.PriorityOrder,
                            IsPercent = true,
                            Markup = 25
                        }
                    },
                    CurrentDivision = Division.III,
                    CurrentTier = Tier.Diamond,
                    CurrentLp = CurrentLp.min81max99,
                    FinalDivision = Division.I,
                    FinalTier = Tier.Diamond,
                    LpGain = LpGain.min18max24plus,
                    QueueType = QueueType.SoloDuoQueue,
                    CoachingMarketLanguage = "1"
                }
            };
        }

        #endregion
    }
}
