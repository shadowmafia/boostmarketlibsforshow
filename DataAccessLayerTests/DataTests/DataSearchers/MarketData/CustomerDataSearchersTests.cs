﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerImplementation.MarketData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketData
{
    [TestCategory("CustomerTests")]
    [TestClass]
    public class CustomerDataSearchersTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }


        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CustomerIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CustomerIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetCustomer_Method()
        {
            List<Customer> testCustomers = await CreateTestCustomers();

            Customer returnedWithFakeId = await _dal.MarketData.Customer.Searcher.GetCustomer("fakeId");
            Customer returnedCustomer1 = await _dal.MarketData.Customer.Searcher.GetCustomer(id: testCustomers[0].Id);
            Customer returnedCustomer2 = await _dal.MarketData.Customer.Searcher.GetCustomer(email: "Vasia@gmail.com");
            Customer returnedCustomer3 = await _dal.MarketData.Customer.Searcher.GetCustomer(username: "Serega");

            Assert.AreEqual(testCustomers[0].Id, returnedCustomer1.Id);
            Assert.AreEqual(testCustomers[1].Id, returnedCustomer2.Id);
            Assert.AreEqual(testCustomers[2].Id, returnedCustomer3.Id);
            Assert.IsNull(returnedWithFakeId);
        }

        [TestMethod]
        public async Task Test_FindCustomers_FindCustomerCounts_Methods()
        {
            List<Customer> testCustomers = await CreateTestCustomers();

            uint testBoolAgg = await _dal.MarketData.Customer.Searcher.FindCustomersCount(
                isEmailConfirmed: false
            );

            uint testDateAgg = await _dal.MarketData.Customer.Searcher.FindCustomersCount(
                minRegistrationDate: new DateTime(2019, 10, 9),
                maxRegistrationDate: new DateTime(2019, 10, 21)
            );

            uint testCombineAgg = await _dal.MarketData.Customer.Searcher.FindCustomersCount(
                isEmailConfirmed: false,
                minRegistrationDate: new DateTime(2019, 10, 10),
                maxRegistrationDate: new DateTime(2019, 10, 20)
            );

            var testFindWithUotParams = await _dal.MarketData.Customer.Searcher.FindCustomers();
            uint testWithOutParamsAgg = await _dal.MarketData.Customer.Searcher.FindCustomersCount();

            Assert.AreEqual(2u, testBoolAgg);
            Assert.AreEqual(2u, testDateAgg);
            Assert.AreEqual(1u, testCombineAgg);
            Assert.AreEqual(3u, testWithOutParamsAgg);
            Assert.AreEqual(3, testFindWithUotParams.Count);
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_GetCustomerMethod_AllParamsIsNull()
        {
            await _dal.MarketData.Customer.Searcher.GetCustomer();
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindCustomers_SkipLessThenNull()
        {
            await _dal.MarketData.Customer.Searcher.FindCustomers(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindCustomers_TakeLessThenNull()
        {
            await _dal.MarketData.Customer.Searcher.FindCustomers(skip: -1);
        }

        #endregion


        #region private methods

        private async Task<List<Customer>> CreateTestCustomers()
        {

            Customer testCustomer1 = new Customer()
            {
                Username = "Valera",
                Email = "ValeraVetrov@gmail.com",
                FirstName = "Valera",
                LastName = "Vetrov",
                Country = "Ukraine",
                IsEmailConfirmed = false,
                Discord = "ValeraVetrov#discord",
                Skype = "ValeraVetrovSkype",
                Password = "HasPasswordValera",
                RegistrationDate = new DateTime(2019, 10, 10)
            };

            Customer testCustomer2 = new Customer()
            {
                Username = "Vasia",
                Email = "Vasia@gmail.com",
                FirstName = "Vasia",
                LastName = "Vasia",
                Country = "Russia",
                IsEmailConfirmed = true,
                Discord = "Vasia#discord",
                Skype = "Vasia",
                Password = "HasPasswordVasia",
                RegistrationDate = new DateTime(2019, 10, 20)
            };

            Customer testCustomer3 = new Customer()
            {
                Username = "Serega",
                Email = "Serega@gmail.com",
                FirstName = "Serega",
                LastName = "Serega",
                Country = "Poland",
                IsEmailConfirmed = false,
                Discord = "Serega#discord",
                Skype = "Serega",
                Password = "HasPasswordSerega",
                RegistrationDate = new DateTime(2019, 10, 30)
            };

            testCustomer1.Id = await _dal.MarketData.Customer.Manager.AddOrUpdate(testCustomer1);
            testCustomer2.Id = await _dal.MarketData.Customer.Manager.AddOrUpdate(testCustomer2);
            testCustomer3.Id = await _dal.MarketData.Customer.Manager.AddOrUpdate(testCustomer3);

            await ((CustomerDataManager)_dal.MarketData.Customer.Manager).RefreshIndexAsync();

            return new List<Customer>
            {
                testCustomer1,
                testCustomer2,
                testCustomer3
            };
        }

        #endregion
    }
}