﻿using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.BoosterProfileClasses;
using DataAccessLayerImplementation.MarketData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketData
{
    [TestClass]
    [TestCategory("BoosterProfileTests")]
    public class BoosterProfileDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.BoosterProfileIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.BoosterProfileIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetBoosterProfile_Method()
        {
            List<BoosterProfile> testBoosterProfile = await CreateTestBoosterProfiles();

            BoosterProfile returnedWithFakeId = await _dal.MarketData.BoosterProfile.Searcher.GetBoosterProfile("fakeId");
            BoosterProfile returnedBoosterProfile0 = await _dal.MarketData.BoosterProfile.Searcher.GetBoosterProfile(testBoosterProfile[0].CmsUserId);

            Assert.AreEqual(testBoosterProfile[0].CmsUserId, returnedBoosterProfile0.CmsUserId);
            Assert.IsNull(returnedWithFakeId);
        }

        [TestMethod]
        public async Task Test_FindBoosterProfiles_FindBoosterProfilesCount_Method()
        {
            List<BoosterProfile> testBoosterProfile = await CreateTestBoosterProfiles();

            List<BoosterProfile> searchResult1 = (
                await _dal.MarketData.BoosterProfile.Searcher.FindBoosterProfiles(
                    lanes: new List<Lane>
                    {
                        Lane.Mid
                    },
                    minTotalFeedBacks: 70,
                    minFeedbackRating: 5,
                    minTotalCompletedOrders: 100,
                    minCoachingPricePerHouse: 50,
                    isCoacher: true,
                    boosterGroups: new List<BoosterGroup>
                    {
                        BoosterGroup.Expert,
                        BoosterGroup.Junior,
                        BoosterGroup.Vip
                    },
                    languages: new List<string>()
                    {
                        "1"
                    },
                    tiers: new List<Tier>
                    {
                        Tier.Diamond,
                        Tier.Challenger
                    },
                    maxCoachingPricePerHouse: 50
                )
            ).ToList();

            List<BoosterProfile> searchResult2 = (await _dal.MarketData.BoosterProfile.Searcher.FindBoosterProfiles(lolAccountIdContain: "Shadowmafia")).ToList();
            List<BoosterProfile> searchResult3 = (await _dal.MarketData.BoosterProfile.Searcher.FindBoosterProfiles(lolAccountIdContain: "warh")).ToList();

            Assert.AreEqual(2, searchResult1.Count);
            Assert.AreEqual(2, searchResult2.Count);
            Assert.AreEqual(1, searchResult3.Count);
            Assert.IsNotNull(searchResult1.FirstOrDefault(booster => booster.CmsUserId == testBoosterProfile[0].CmsUserId));
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterProfile_BoosterIdIsNull()
        {
            await _dal.MarketData.BoosterProfile.Searcher.GetBoosterProfile(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetBoosterProfile_BoosterIdIsEmpty()
        {
            await _dal.MarketData.BoosterProfile.Searcher.GetBoosterProfile(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindBoosterProfiles_SkipLessThenNull()
        {
            await _dal.MarketData.BoosterProfile.Searcher.FindBoosterProfiles(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindBoosterProfiles_TakeLessThenNull()
        {
            await _dal.MarketData.BoosterProfile.Searcher.FindBoosterProfiles(take: -1);
        }

        #endregion

        #region private methods

        private async Task<List<BoosterProfile>> CreateTestBoosterProfiles()
        {
            var testBoosterProfile1 = new BoosterProfile()
            {
                CmsUserId = "Test Cms User 1",
                LoLAccount = new LoLAccount()
                {
                    Tier = Tier.Diamond,
                    Division = Division.I,
                    LeaguePoints = 100,
                    LoLAccountId = "Shadowmafia"
                },
                TotalCompletedOrders = 100,
                CoachingPricePerHour = 50,
                Lanes = new HashSet<Lane>
                {
                    Lane.Mid,
                    Lane.Jungle
                },
                Languages = new HashSet<string> {
                    "1",
                    "2"
                },
                BoosterGroup = BoosterGroup.Expert,
                FeedbackRating = 5,
                TotalFeedBacks = 70,
                IsCoacher = true,
                AmountEarnedMoney = 5000,
                Balance = 3700,
                FrozenMoney = 200,
            };

            var testBoosterProfile2 = new BoosterProfile()
            {
                CmsUserId = "TestCmsUser2",
                LoLAccount = new LoLAccount()
                {
                    Tier = Tier.Challenger,
                    LeaguePoints = 500,
                    LoLAccountId = "Warhunter"
                },
                TotalCompletedOrders = 1000,
                CoachingPricePerHour = 50,
                Lanes = new HashSet<Lane>
                {
                    Lane.Top,
                    Lane.Mid,
                    Lane.Jungle
                },
                Languages = new HashSet<string> {
                    "1"
                },
                BoosterGroup = BoosterGroup.Vip,
                FeedbackRating = 10,
                TotalFeedBacks = 100,
                IsCoacher = false,
                AmountEarnedMoney = 10000,
                Balance = 3700,
                FrozenMoney = 1000,
            };

            var testBoosterProfile3 = new BoosterProfile()
            {
                CmsUserId = "TestCmsUser3",
                LoLAccount = new LoLAccount()
                {
                    Tier = Tier.Diamond,
                    Division = Division.I,
                    LeaguePoints = 100,
                    LoLAccountId = "Shadowmafia2"
                },
                TotalCompletedOrders = 100,
                CoachingPricePerHour = 50,
                Lanes = new HashSet<Lane>
                {
                    Lane.Mid,
                    Lane.Jungle
                },
                Languages = new HashSet<string> {
                    "1",
                    "2"
                },
                BoosterGroup = BoosterGroup.Expert,
                FeedbackRating = 5,
                TotalFeedBacks = 70,
                IsCoacher = true,
                AmountEarnedMoney = 5000,
                Balance = 3700,
                FrozenMoney = 200
            };

            testBoosterProfile1.CmsUserId = await _dal.MarketData.BoosterProfile.Manager.AddOrUpdate(testBoosterProfile1);
            testBoosterProfile2.CmsUserId = await _dal.MarketData.BoosterProfile.Manager.AddOrUpdate(testBoosterProfile2);
            testBoosterProfile3.CmsUserId = await _dal.MarketData.BoosterProfile.Manager.AddOrUpdate(testBoosterProfile3);

            await ((BoosterProfileDataManager)_dal.MarketData.BoosterProfile.Manager).RefreshIndexAsync();

            return new List<BoosterProfile>
            {
                testBoosterProfile1,
                testBoosterProfile2,
                testBoosterProfile3
            };
        }

        #endregion
    }
}
