﻿using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderFeedbackClasses;
using DataAccessLayerImplementation.MarketData.DataManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.MarketData
{
    [TestCategory("OrderFeedbackTests")]
    [TestClass]
    public class OrderFeedbackDataSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.OrderFeedbackIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.OrderFeedbackIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetOrderFeedback()
        {
            List<OrderFeedback> testOrderFeedbacks = await CreateTestOrderFeedbacks();

            var testResultWithFakeOrderId = await _dal.MarketData.OrderFeedback.Searcher.GetOrderFeedback("fakeOrder");
            var testResult1 = await _dal.MarketData.OrderFeedback.Searcher.GetOrderFeedback(testOrderFeedbacks[0].OrderId);
            var testResult2 = await _dal.MarketData.OrderFeedback.Searcher.GetOrderFeedback(testOrderFeedbacks[1].OrderId);

            Assert.IsNull(testResultWithFakeOrderId);
            Assert.AreEqual(testOrderFeedbacks[0].OrderId, testResult1.OrderId);
            Assert.AreEqual(testOrderFeedbacks[1].OrderId, testResult2.OrderId);
        }

        [TestMethod]
        public async Task Test_FindOrderFeedbacks_FindOrderFeedbacksCounts_Methods()
        {
            List<OrderFeedback> testOrderFeedbacks = await CreateTestOrderFeedbacks();

            List<OrderFeedback> searchResult1 = (await
                _dal.MarketData.OrderFeedback.Searcher.FindOrderFeedbacks(
                  testOrderFeedbacks[0].BoosterId,
                  testOrderFeedbacks[0].CustomerId,
                  "cool booster",
                  5,
                  5,
                  testOrderFeedbacks[0].DateTime,
                  testOrderFeedbacks[0].DateTime,
                  new List<FeedbackStatus>
                  {
                      FeedbackStatus.Confirmed
                  }
            )).ToList();

            List<OrderFeedback> searchResult2 = (await
                _dal.MarketData.OrderFeedback.Searcher.FindOrderFeedbacks(
                    "booster1",
                    commentTextContain: "booster wp",
                    skip: 1
                )).ToList();

            uint searchResult2Count = await _dal.MarketData.OrderFeedback.Searcher.FindOrderFeedbacksCount(
                    "booster1",
                    commentTextContain: "booster wp"
            );


            Assert.IsNotNull(searchResult1);
            Assert.AreEqual(1, searchResult1.Count);
            Assert.IsNotNull(searchResult1.FirstOrDefault(x => x.OrderId == testOrderFeedbacks[0].OrderId));

            Assert.IsNotNull(searchResult2);
            Assert.AreEqual(1, searchResult2.Count);
            Assert.IsNotNull(searchResult2.FirstOrDefault(x => x.OrderId == testOrderFeedbacks[0].OrderId));

            Assert.AreEqual(2u, searchResult2Count);
        }

        #region check params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetOrderFeedback_FeedbackIdIsNull()
        {
            await _dal.MarketData.OrderFeedback.Searcher.GetOrderFeedback(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_GetOrderFeedback_FeedbackIdIsEmpty()
        {
            await _dal.MarketData.OrderFeedback.Searcher.GetOrderFeedback(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindOrderFeedbacks_SkipLessThenNull()
        {
            await _dal.MarketData.OrderFeedback.Searcher.FindOrderFeedbacks(skip: -1);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task Test_FindOrderFeedbacks_TakeLessThenNull()
        {
            await _dal.MarketData.OrderFeedback.Searcher.FindOrderFeedbacks(skip: -1);
        }

        #endregion

        #region private methods

        private async Task<List<OrderFeedback>> CreateTestOrderFeedbacks()
        {
            var testOrderFeedback1 = new OrderFeedback
            {
                OrderId = "order1",
                BoosterId = "booster1",
                CustomerId = "customer1",
                Rating = 5,
                Status = FeedbackStatus.Confirmed,
                DateTime = new DateTime(2020, 3, 31),
                CommentText = "So cool booster wp!"
            };

            var testOrderFeedback2 = new OrderFeedback
            {
                OrderId = "order2",
                BoosterId = "booster1",
                CustomerId = "customer1",
                Rating = 3,
                Status = FeedbackStatus.Rejected,
                DateTime = new DateTime(2020, 3, 29),
                CommentText = "ok!"
            };

            var testOrderFeedback3 = new OrderFeedback
            {
                OrderId = "order3",
                BoosterId = "booster1",
                CustomerId = "customer2",
                Rating = 1,
                Status = FeedbackStatus.New,
                DateTime = new DateTime(2020, 3, 15),
                CommentText = "nice booster wp!"
            };

            testOrderFeedback3.OrderId = await _dal.MarketData.OrderFeedback.Manager.AddOrUpdate(testOrderFeedback3);
            testOrderFeedback1.OrderId = await _dal.MarketData.OrderFeedback.Manager.AddOrUpdate(testOrderFeedback1);
            testOrderFeedback2.OrderId = await _dal.MarketData.OrderFeedback.Manager.AddOrUpdate(testOrderFeedback2);

            await ((OrderFeedbackDataManager)_dal.MarketData.OrderFeedback.Manager).RefreshIndexAsync();

            return new List<OrderFeedback>
            {
                testOrderFeedback1,
                testOrderFeedback2,
                testOrderFeedback3
            };
        }

        #endregion
    }
}