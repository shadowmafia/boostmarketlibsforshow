﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerImplementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.ChatData
{
    [TestCategory("ChatTest")]
    [TestClass]
    public class ChatChannelSearcherTests : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName
            );
        }

        [TestMethod]
        public async Task Test_GetChannelSummary_Method()
        {
            #region Test data

            ChatMember member1 = new ChatMember
            {
                MemberId = "member1",
                MemberType = ChatMemberType.CmsUser
            };
            ChatMember member1Customer = new ChatMember
            {
                MemberId = "member1",
                MemberType = ChatMemberType.Customer
            };
            ChatMember member2 = new ChatMember
            {
                MemberId = "member2",
                MemberType = ChatMemberType.CmsUser
            };
            ChatMember member3 = new ChatMember
            {
                MemberId = "member2",
                MemberType = ChatMemberType.CmsUser
            };

            ChatChannel testChannel = new ChatChannel
            {
                ChannelType = ChatChannelType.Order,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-1),
                    Content = "Channel 1 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member1Customer
                }
            };
            ChatChannel testChannel2 = new ChatChannel
            {
                ChannelType = ChatChannelType.All,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-2),
                    Content = "Channel 2 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member1Customer,
                    member2,
                    member3
                }
            };
            ChatChannel testChannel3 = new ChatChannel
            {
                ChannelType = ChatChannelType.Lfg,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-3),
                    Content = "Channel 2 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member2,
                    member3
                }
            };
            ChatChannel testChannel4 = new ChatChannel
            {
                ChannelType = ChatChannelType.Private,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-3),
                    Content = "Channel 2 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member2
                }
            };
            ChatChannel testChannel5 = new ChatChannel
            {
                ChannelType = ChatChannelType.Private,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-3),
                    Content = "Channel 2 last message",
                    Sender = member3,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member2,
                    member3
                }
            };


            #endregion

            await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel);
            await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel2);
            string channel3Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel3);
            await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel4);
            string channel5Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel5);

            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager).RefreshIndexAsync();

            IList<ChatChannel> searchResult = await _dal.ChatData.Channels.Searcher.GetChannelsSummary(member1);

            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName);

            Assert.AreEqual(4, searchResult.Count);
            Assert.IsTrue(searchResult.Any(_ => _.Id == channel3Id));
            Assert.IsFalse(searchResult.Any(_ => _.Id == channel5Id));
        }

        [TestMethod]
        public async Task Test_GetChatChannel_GetChannelsByMember_TryFindPrivateChannel_Methods()
        {
            #region Test data

            ChatMember member1 = new ChatMember
            {
                MemberId = "member1",
                MemberType = ChatMemberType.CmsUser
            };
            ChatMember member1Customer = new ChatMember
            {
                MemberId = "member1",
                MemberType = ChatMemberType.Customer
            };
            ChatMember member2 = new ChatMember
            {
                MemberId = "member2",
                MemberType = ChatMemberType.CmsUser
            };
            ChatMember member3 = new ChatMember
            {
                MemberId = "member3",
                MemberType = ChatMemberType.CmsUser
            };

            ChatChannel testChannel = new ChatChannel
            {
                ChannelType = ChatChannelType.Order,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-1),
                    Content = "Channel 1 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member1Customer
                }
            };
            ChatChannel testChannel2 = new ChatChannel
            {
                ChannelType = ChatChannelType.All,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-2),
                    Content = "Channel 2 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member1Customer,
                    member2,
                    member3
                }
            };
            ChatChannel testChannel3 = new ChatChannel
            {
                ChannelType = ChatChannelType.Lfg,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-3),
                    Content = "Channel 2 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member2,
                    member3
                }
            };
            ChatChannel testChannel4 = new ChatChannel
            {
                ChannelType = ChatChannelType.Private,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-4),
                    Content = "Channel 4 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member2
                }
            };

            ChatChannel testChannel5 = new ChatChannel
            {
                ChannelType = ChatChannelType.Private,
                LastMessage = new ChatMessage
                {
                    Timestamp = DateTime.UtcNow.AddDays(-5),
                    Content = "Channel 5 last message",
                    Sender = member1,
                },
                ChannelMembers = new List<ChatMember>
                {
                    member1,
                    member1Customer
                }
            };

            #endregion

            await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel);
            string channelId2 = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel2);
            await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel3);
            string channelId4 = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel4);
            string channelId5 = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChannel5);
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager).RefreshIndexAsync();

            ChatChannel resultChannel2 = await _dal.ChatData.Channels.Searcher.GetChannel(channelId2);

            IList<ChatChannel> getAllChannelsByMemberResult = await _dal.ChatData.Channels.Searcher.GetChannelsByMember(member1);
            IList<ChatChannel> getLafAndALLChannelsResult2 = await _dal.ChatData.Channels.Searcher.GetChannelsByMember(member1, new List<ChatChannelType>
            {
                ChatChannelType.All,
                ChatChannelType.Lfg
            });

            IList<ChatChannel> getAllChannelsResult3 = await _dal.ChatData.Channels.Searcher.GetChannelsByMember(
                member1,
                elderThen: testChannel.LastMessage.Timestamp,
                take: 1
            );

            var findPrivateResult = await _dal.ChatData.Channels.Searcher.FindPrivateChannelBetweenUsers(member1, member2);
            var findPrivateResult2 = await _dal.ChatData.Channels.Searcher.FindPrivateChannelBetweenUsers(member1, member1Customer);
            var findPrivateResult3 = await _dal.ChatData.Channels.Searcher.FindPrivateChannelBetweenUsers(member1, member3);
            
            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName);

            Assert.AreEqual(channelId4, findPrivateResult.Id);
            Assert.AreEqual(channelId5, findPrivateResult2.Id);
            Assert.IsNull(findPrivateResult3);

            Assert.AreEqual(testChannel2.ChannelType, resultChannel2.ChannelType);
            Assert.AreEqual(testChannel2.LastMessage.Content, resultChannel2.LastMessage.Content);
            Assert.AreEqual(channelId2, getAllChannelsResult3.FirstOrDefault().Id);

            Assert.AreEqual(testChannel2.ChannelType, resultChannel2.ChannelType);

            Assert.AreEqual(5, getAllChannelsByMemberResult.Count);
            Assert.AreEqual(2, getLafAndALLChannelsResult2.Count);
        }
    }
}
