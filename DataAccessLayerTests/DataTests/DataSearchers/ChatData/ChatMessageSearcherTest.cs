﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerImplementation;
using DataAccessLayerImplementation._ElasticIndexSettings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataSearchers.ChatData
{
    [TestCategory("ChatTest")]
    [TestClass]
    public class ChatMessageSearcherTest : BaseDataSearcherTests
    {
        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            BaseClassInit();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            BaseClassCleanup();
        }

        [TestInitialize]
        public async Task TestInit()
        {
            await _elasticClient.Indices.DeleteAsync(
                $"{_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatMessageIndexSettings.ChatMessageIndexName}_{DateTime.UtcNow.Year}_{DateTime.UtcNow:MM}"
            );
        }

        [TestCleanup]
        public async Task TestCleanUp()
        {
            await _elasticClient.Indices.DeleteAsync(
                $"{_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatMessageIndexSettings.ChatMessageIndexName}_{DateTime.UtcNow.Year}_{DateTime.UtcNow:MM}"
            );
        }

        [TestMethod]
        public async Task Test_TestIndexAliasReplication_Logic()
        {
            string messageIndexName = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatMessageIndexSettings.ChatMessageIndexName;
            DateTime yearLast = DateTime.UtcNow.AddMonths(-6);
            DateTime prevMonthDate = DateTime.UtcNow.AddMonths(-1);
            ChatDataIndexSettings settings = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings;

            string yearLastIndexName =
                $"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{yearLast.Year}_{yearLast:MM}";
            string prevMonthDateIndexName =
                $"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{prevMonthDate.Year}_{prevMonthDate:MM}";

            await _elasticClient.Indices.CreateAsync(yearLastIndexName);
            await _elasticClient.Indices.PutAliasAsync(yearLastIndexName, settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.PutAliasAsync(yearLastIndexName, settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);

            await _elasticClient.Indices.CreateAsync(prevMonthDateIndexName);
            await _elasticClient.Indices.PutAliasAsync(prevMonthDateIndexName, settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.PutAliasAsync(prevMonthDateIndexName, settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);

            var chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000001",
            };
            var chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = "000001"
            };
            var chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000002"
            };

            ChatChannel chatChanel1 = new ChatChannel()
            {
                ChannelType = ChatChannelType.All,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };
            ChatChannel chatChanel2 = new ChatChannel()
            {
                ChannelType = ChatChannelType.Private,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                }
            };
            ChatChannel chatChanel3 = new ChatChannel()
            {
                ChannelType = ChatChannelType.Lfg,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };

            string chatChanelId1 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel1);
            string chatChanelId2 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel2);
            string chatChanelId3 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel3);

            await _elasticClient.IndexManyAsync(GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, yearLast), yearLastIndexName);
            await _elasticClient.IndexManyAsync(GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, prevMonthDate), prevMonthDateIndexName);

            await ((ElasticsearchEntityManagerBase<ChatMessage>)_dal.ChatData.Messages.Manager).RefreshIndexAsync();

            var messagesForCurrentMonth = GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, prevMonthDate);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[0]);

            await ((ElasticsearchEntityManagerBase<ChatMessage>)_dal.ChatData.Messages.Manager).RefreshIndexAsync();

            var currentMonthIndexes = _elasticClient.GetIndicesPointingToAlias(settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);
            var actualMessagesIndexes = _elasticClient.GetIndicesPointingToAlias(settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            var archiveMessagesIndexes = _elasticClient.GetIndicesPointingToAlias(settings.ChatMessageIndexSettings.ArchiveMessagesAliasName);

            await _elasticClient.Indices.DeleteAsync(yearLastIndexName);
            await _elasticClient.Indices.DeleteAsync(prevMonthDateIndexName);
            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName);

            Assert.AreEqual($"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{DateTime.UtcNow.Year}_{DateTime.UtcNow:MM}", currentMonthIndexes.FirstOrDefault());
            Assert.IsTrue(actualMessagesIndexes.Contains($"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{DateTime.UtcNow.Year}_{DateTime.UtcNow:MM}"));
            Assert.IsTrue(actualMessagesIndexes.Contains(prevMonthDateIndexName));
            Assert.AreEqual(yearLastIndexName, archiveMessagesIndexes.FirstOrDefault());
        }

        [TestMethod]
        public async Task Test_GetUnreadMessageInChannels_Logic()
        {
            string messageIndexName = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatMessageIndexSettings.ChatMessageIndexName;
            DateTime yearLast = DateTime.UtcNow.AddMonths(-6);
            DateTime prevMonthDate = DateTime.UtcNow.AddMonths(-1);
            ChatDataIndexSettings settings = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings;

            string yearLastIndexName =
                $"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{yearLast.Year}_{yearLast:MM}";
            string prevMonthDateIndexName =
                $"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{prevMonthDate.Year}_{prevMonthDate:MM}";

            await _elasticClient.Indices.CreateAsync(yearLastIndexName);
            await _elasticClient.Indices.PutAliasAsync(yearLastIndexName, settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.PutAliasAsync(yearLastIndexName, settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);

            await _elasticClient.Indices.CreateAsync(prevMonthDateIndexName);
            await _elasticClient.Indices.PutAliasAsync(prevMonthDateIndexName, settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.PutAliasAsync(prevMonthDateIndexName, settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);

            var chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000001",
            };
            var chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = "000001"
            };
            var chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000002"
            };

            ChatChannel chatChanel1 = new ChatChannel()
            {
                ChannelType = ChatChannelType.All,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };
            ChatChannel chatChanel2 = new ChatChannel()
            {
                ChannelType = ChatChannelType.Private,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                }
            };
            ChatChannel chatChanel3 = new ChatChannel()
            {
                ChannelType = ChatChannelType.Lfg,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };

            string chatChanelId1 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel1);
            string chatChanelId2 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel2);
            string chatChanelId3 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel3);

            await _elasticClient.IndexManyAsync(GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, yearLast), yearLastIndexName);
            await _elasticClient.IndexManyAsync(GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, prevMonthDate), prevMonthDateIndexName);

            await _elasticClient.Indices.RefreshAsync(yearLastIndexName);
            await _elasticClient.Indices.RefreshAsync(yearLastIndexName);

            var messagesForCurrentMonth = GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, prevMonthDate);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[0]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[2]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[1]);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[3]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[4]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[5]);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[6]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[7]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[8]);

            await _dal.ChatData.Channels.Manager.UpdateLastSeen(chatChanelId1, chatMember1, prevMonthDate.AddMinutes(-2));
            await _dal.ChatData.Channels.Manager.UpdateLastSeen(chatChanelId2, chatMember1, prevMonthDate.AddMinutes(-6));
            await _dal.ChatData.Channels.Manager.UpdateLastSeen(chatChanelId3, chatMember1);

            await _elasticClient.Indices.RefreshAsync(settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.RefreshAsync(settings.ChatChannelIndexName);

            var result = await _dal.ChatData.Messages.Searcher.GetUnreadMessagesInChannels(chatMember1);
            var result3 = await _dal.ChatData.Messages.Searcher.GetUnreadMessagesInChannels(chatMember1, new List<string>
            {
                chatChanelId1,
                chatChanelId3
            });

            await _dal.ChatData.Channels.Manager.UpdateLastSeen(chatChanelId2, chatMember1);

            await _elasticClient.Indices.RefreshAsync(settings.ChatChannelIndexName);

            var result2 = await _dal.ChatData.Messages.Searcher.GetUnreadMessagesInChannels(chatMember1);

            await _elasticClient.Indices.DeleteAsync(settings.ChatChannelIndexName);
            await _elasticClient.Indices.DeleteAsync(yearLastIndexName);
            await _elasticClient.Indices.DeleteAsync(prevMonthDateIndexName);

            var dontNeedChannel = result.Any(x => x.chanelId == chatChanelId3);
            var dontNeedChannel2 = result2.Any(x => x.chanelId == chatChanelId2);

            Assert.AreEqual(1, result.FirstOrDefault(x => x.chanelId == chatChanelId1).unreadMessagesCount);
            Assert.AreEqual(2, result.FirstOrDefault(x => x.chanelId == chatChanelId2).unreadMessagesCount);
            Assert.IsFalse(dontNeedChannel);

            Assert.AreEqual(1, result3.FirstOrDefault(x => x.chanelId == chatChanelId1).unreadMessagesCount);
            Assert.IsFalse(result3.Any(_ => _.chanelId == chatChanelId3));

            Assert.AreEqual(1, result2.FirstOrDefault(x => x.chanelId == chatChanelId1).unreadMessagesCount);
            Assert.IsFalse(dontNeedChannel2);
        }

        [TestMethod]
        public async Task Test_FindMessages_FindMessagesCount_Methods()
        {
            string messageIndexName = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatMessageIndexSettings.ChatMessageIndexName;
            DateTime yearLast = DateTime.UtcNow.AddMonths(-6);
            DateTime prevMonthDate = DateTime.UtcNow.AddMonths(-1);
            ChatDataIndexSettings settings = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings;

            string yearLastIndexName =
                $"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{yearLast.Year}_{yearLast:MM}";
            string prevMonthDateIndexName =
                $"{settings.ChatMessageIndexSettings.ChatMessageIndexName}_{prevMonthDate.Year}_{prevMonthDate:MM}";

            await _elasticClient.Indices.CreateAsync(yearLastIndexName);
            await _elasticClient.Indices.PutAliasAsync(yearLastIndexName, settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.PutAliasAsync(yearLastIndexName, settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);

            await _elasticClient.Indices.CreateAsync(prevMonthDateIndexName);
            await _elasticClient.Indices.PutAliasAsync(prevMonthDateIndexName, settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.PutAliasAsync(prevMonthDateIndexName, settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);

            var chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000001",
            };
            var chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = "000001"
            };
            var chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000002"
            };

            ChatChannel chatChanel1 = new ChatChannel()
            {
                ChannelType = ChatChannelType.All,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };
            ChatChannel chatChanel2 = new ChatChannel()
            {
                ChannelType = ChatChannelType.Private,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                }
            };
            ChatChannel chatChanel3 = new ChatChannel()
            {
                ChannelType = ChatChannelType.Lfg,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };

            string chatChanelId1 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel1);
            string chatChanelId2 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel2);
            string chatChanelId3 = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanel3);

            await _elasticClient.IndexManyAsync(GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, yearLast), yearLastIndexName);
            await _elasticClient.IndexManyAsync(GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, prevMonthDate), prevMonthDateIndexName);

            await _elasticClient.Indices.RefreshAsync(settings.ChatMessageIndexSettings.ActualMessagesAliasName);

            var messagesForCurrentMonth = GetMessagesPackByDate(chatChanelId1, chatChanelId2, chatChanelId3, prevMonthDate);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[0]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[2]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[1]);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[3]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[4]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[5]);

            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[6]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[7]);
            await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForCurrentMonth[8]);

            await _elasticClient.Indices.RefreshAsync(settings.ChatMessageIndexSettings.ActualMessagesAliasName);

            var searchResult = await _dal.ChatData.Messages.Searcher.FindMessages(chatChanelId1, chatMember1, "friend2");
            var searchResult2Count = await _dal.ChatData.Messages.Searcher.FindMessagesCount(chatChanelId2);
            var searchResult2 = await _dal.ChatData.Messages.Searcher.FindMessages(chatChanelId1, skip: 1);

            await _elasticClient.Indices.DeleteAsync(settings.ChatChannelIndexName);
            await _elasticClient.Indices.DeleteAsync(yearLastIndexName);
            await _elasticClient.Indices.DeleteAsync(prevMonthDateIndexName);

            Assert.AreEqual(1, searchResult.Count);
            Assert.AreEqual(3, searchResult2Count);
            Assert.AreEqual(2, searchResult2.Count);
        }

        private List<ChatMessage> GetMessagesPackByDate(string channelId1, string channelId2, string channelId3, DateTime time)
        {
            var chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000001",
            };
            var chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = "000001"
            };
            var chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = "000002"
            };

            //Channel 1 messages
            ChatMessage channel1_Message1 = new ChatMessage()
            {
                ChannelId = channelId1,
                Content = "Hello my  dear friend",
                Sender = chatMember1,
                Timestamp = time.AddMinutes(-1)
            };
            ChatMessage channel1_Message2 = new ChatMessage()
            {
                ChannelId = channelId1,
                Content = "Hello my  dear friend2",
                Sender = chatMember1,
                Timestamp = time.AddMinutes(-2)
            };
            ChatMessage channel1_Message3 = new ChatMessage()
            {
                ChannelId = channelId1,
                Content = "Hello my  dear friend3",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-3)
            };

            //Channel 2 messages
            ChatMessage channel2_Message1 = new ChatMessage()
            {
                ChannelId = channelId2,
                Content = "Hello my  dear friend",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-4)
            };
            ChatMessage channel2_Message2 = new ChatMessage()
            {
                ChannelId = channelId2,
                Content = "Hello my  dear friend2",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-5)
            };
            ChatMessage channel2_Message3 = new ChatMessage()
            {
                ChannelId = channelId2,
                Content = "Hello my  dear friend3",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-6)
            };

            //Channel 3 messages
            ChatMessage channel3_Message1 = new ChatMessage()
            {
                ChannelId = channelId3,
                Content = "Hello my  dear friend",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-7)
            };
            ChatMessage channel3_Message2 = new ChatMessage()
            {
                ChannelId = channelId3,
                Content = "Hello my  dear friend2",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-8)
            };
            ChatMessage channel3_Message3 = new ChatMessage()
            {
                ChannelId = channelId3,
                Content = "Hello my  dear friend3",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-9)
            };

            return new List<ChatMessage>
            {
                channel1_Message1,
                channel1_Message2,
                channel1_Message3,
                channel2_Message1,
                channel2_Message2,
                channel2_Message3,
                channel3_Message1,
                channel3_Message2,
                channel3_Message3
            };
        }
    }
}