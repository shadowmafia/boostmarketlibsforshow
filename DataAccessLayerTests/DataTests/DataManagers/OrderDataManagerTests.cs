﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction._CommonEnums.Game;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.Enums;
using DataAccessLayerAbstraction.MarketData.Mapping.OrderClasses.OrderDetailsClasses;
using DataAccessLayerImplementation;
using DataAccessLayerImplementation.MarketData.DataManagers;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataManagers
{
    [TestCategory("OrderTests")]
    [TestClass]
    public class OrderDataManagerTests
    {
        private static IDataAccessLayer _dal;
        private static TestServer _testServer;
        protected static IElasticClient _elasticClient;
        protected static ElasticDataAccessLayerSettings _elasticDalSettings;

        [ClassInitialize]
        public static void Test_ClassInit(TestContext testContext)
        {
            _testServer = TestsHelper.InitTestServer();
            _dal = (IDataAccessLayer)_testServer.Services.GetService(typeof(IDataAccessLayer));
            _elasticClient = (IElasticClient)_testServer.Services.GetService(typeof(IElasticClient));
            _elasticDalSettings = ((IOptions<ElasticDataAccessLayerSettings>)_testServer.Services.GetService(typeof(IOptions<ElasticDataAccessLayerSettings>))).Value;
        }

        [ClassCleanup]
        public static async Task TestClassCleanup()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.OrderIndexName
            );

            _testServer = null;
            _dal = null;
            _elasticClient = null;
            _elasticDalSettings = null;
        }

        [TestMethod]
        public async Task Test_AddOrUpdateOrder_DeleteOrder_Method()
        {
            Order testOrder1 = CreateTestOrder();
            Order testOrder2 = CreateTestOrder();

            testOrder2.Details = new NormalMatches()
            {
                NeedGames = 10,
                AdditionalServices = new List<AdditionalService>()
                {
                    new AdditionalService()
                    {
                        AdditionalServiceType = AdditionalServiceType.WithStreaming,
                        IsPercent = true,
                        Markup = 30,
                    }
                }
            };

            testOrder2.Type = OrderType.NormalMatches;

            testOrder1.Id = await _dal.MarketData.Order.Manager.AddOrUpdate(testOrder1);
            testOrder2.Id = await _dal.MarketData.Order.Manager.AddOrUpdate(testOrder2);

            await ((OrderDataManager)_dal.MarketData.Order.Manager).RefreshIndexAsync();

            Order returnedTestOrder1 = await _dal.MarketData.Order.Searcher.GetOrder(testOrder1.Id);
            Order returnedTestOrder2 = await _dal.MarketData.Order.Searcher.GetOrder(testOrder2.Id);

            Assert.IsNotNull(returnedTestOrder1);
            Assert.IsNotNull(returnedTestOrder2);
            Assert.IsNotNull(returnedTestOrder1.Details);
            Assert.IsNotNull(returnedTestOrder2.Details);

            Assert.IsInstanceOfType(returnedTestOrder1.Details, typeof(DuoLeagueBoosting));
            Assert.AreEqual(testOrder1.Id, returnedTestOrder1.Id);
            Assert.AreEqual(testOrder1.Type, returnedTestOrder1.Type);
            Assert.AreEqual(testOrder1.Region, returnedTestOrder1.Region);

            Assert.IsInstanceOfType(returnedTestOrder2.Details, typeof(NormalMatches));
            Assert.AreEqual(testOrder2.Id, returnedTestOrder2.Id);
            Assert.AreEqual(testOrder2.Type, returnedTestOrder2.Type);
            Assert.AreEqual(testOrder2.Region, returnedTestOrder2.Region);

            await _dal.MarketData.Order.Manager.DeleteOrder(testOrder1.Id);
            await _dal.MarketData.Order.Manager.DeleteOrder(testOrder2.Id);
        }

        #region testParams

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_AddOrUpdateOrderMethod_orderIsNull()
        {
            await _dal.MarketData.Order.Manager.AddOrUpdate(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_AddOrUpdateOrderMethod_order_DetailsIsNull()
        {
            await _dal.MarketData.Order.Manager.AddOrUpdate(new Order());
        }

        #endregion

        #region private methods

        private Order CreateTestOrder()
        {
            return new Order()
            {
                AccountLogin = "testLogin",
                AccountName = "testName",
                AccountPassword = "testPassword",
                BoosterId = "testBoosterId",
                CheckoutTime = new DateTime(2020, 3, 12, 23, 23, 10),
                CmsOrderComments = new List<CmsOrderComment>()
                {
                    new CmsOrderComment()
                    {
                        CmsUserIndex = "testUserIndex1",
                        DateTime = DateTime.UtcNow,
                        Message = "testMessage 1 bla"
                    },
                    new CmsOrderComment()
                    {
                        CmsUserIndex = "testUserIndex2",
                        DateTime = DateTime.UtcNow,
                        Message = "testMessage 2 bla bla"
                    }
                },
                CompletionTime = new DateTime(2020, 5, 14, 23, 23, 10),
                CustomerComment = "Want booster was play after 12.00",
                CustomerId = "testCustomerId1",
                Type = OrderType.DuoLeagueBoosting,
                Details = new DuoLeagueBoosting()
                {
                    AdditionalServices = new List<AdditionalService>()
                    {
                        new AdditionalService()
                        {
                            AdditionalServiceType = AdditionalServiceType.PlusOneWin,
                            IsPercent = false,
                            Markup = 10
                        },
                        new AdditionalService()
                        {
                            AdditionalServiceType = AdditionalServiceType.PriorityOrder,
                            IsPercent = true,
                            Markup = 25
                        }
                    },
                    CurrentDivision = Division.III,
                    CurrentTier = Tier.Diamond,
                    CurrentLp = CurrentLp.min81max99,
                    FinalDivision = Division.I,
                    FinalTier = Tier.Diamond,
                    LpGain = LpGain.min18max24plus,
                    QueueType = QueueType.SoloDuoQueue,
                    CoachingMarketLanguage = "1"
                }
            };
        }

        #endregion
    }
}
