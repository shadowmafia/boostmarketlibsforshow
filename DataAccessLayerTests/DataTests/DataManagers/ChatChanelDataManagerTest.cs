﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerImplementation;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataTests.DataManagers
{
    [TestCategory("ChatTest")]
    [TestClass]
    public class ChatChanelDataManagerTest
    {
        private static IDataAccessLayer _dal;
        private static TestServer _testServer;
        protected static IElasticClient _elasticClient;
        protected static ElasticDataAccessLayerSettings _elasticDalSettings;

        [ClassInitialize]
        public static void Test_ClassInit(TestContext testContext)
        {
            _testServer = TestsHelper.InitTestServer();
            _dal = (IDataAccessLayer)_testServer.Services.GetService(typeof(IDataAccessLayer));
            _elasticClient = (IElasticClient)_testServer.Services.GetService(typeof(IElasticClient));
            _elasticDalSettings =
                ((IOptions<ElasticDataAccessLayerSettings>)_testServer.Services.GetService(
                    typeof(IOptions<ElasticDataAccessLayerSettings>))).Value;
        }

        [ClassCleanup]
        public static async Task TestClassCleanup()
        {
            await _elasticClient.Indices.DeleteAsync(
                _elasticDalSettings.IndexesSettings.MarketDataIndexSettings.OrderIndexName
            );

            _testServer = null;
            _dal = null;
            _elasticClient = null;
            _elasticDalSettings = null;
        }

        [TestMethod]
        public async Task Test_AddMember_RemoveMember_Method()
        {
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager).DeleteIndexAsync();

            ChatChannel testChatChanel = new ChatChannel
            {
                ChannelType = ChatChannelType.All,
                LastMessage = new ChatMessage
                {
                    Content = "azazazazz",
                    Timestamp = DateTime.UtcNow,
                },
                ChannelMembers = new List<ChatMember>
                {
                    new ChatMember
                    {
                        MemberId = "zaqqwe123",
                        MemberType = ChatMemberType.CmsUser
                    }
                }
            };

            string newChannelId = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChatChanel);
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager)
                .RefreshIndexAsync();

            await _dal.ChatData.Channels.Manager.AddMember(newChannelId, new ChatMember
            {
                MemberId = "zaqqwe123",
                MemberType = ChatMemberType.Customer
            });
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager)
                .RefreshIndexAsync();

            var result1 = await _dal.ChatData.Channels.Searcher.GetChannel(newChannelId);

            await _dal.ChatData.Channels.Manager.AddMember(newChannelId, new ChatMember
            {
                MemberId = "zaqqwe123",
                MemberType = ChatMemberType.Customer
            });
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager)
                .RefreshIndexAsync();

            var result2 = await _dal.ChatData.Channels.Searcher.GetChannel(newChannelId);

            await _dal.ChatData.Channels.Manager.RemoveMember(newChannelId, new ChatMember
            {
                MemberId = "zaqqwe123",
                MemberType = ChatMemberType.Customer
            });
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager)
                .RefreshIndexAsync();

            var result3 = await _dal.ChatData.Channels.Searcher.GetChannel(newChannelId);

            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName);

            Assert.AreEqual(2, result1.ChannelMembers.Count);
            Assert.AreEqual(2, result2.ChannelMembers.Count);
            Assert.AreEqual(1, result3.ChannelMembers.Count);
        }


        [TestMethod]
        public async Task Test_UpdateLastMassage_Method()
        {
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager).DeleteIndexAsync();

            ChatChannel testChatChanel = new ChatChannel
            {
                ChannelType = ChatChannelType.All,
                LastMessage = new ChatMessage
                {
                    Content = "azazazazz",
                    Timestamp = DateTime.UtcNow,
                },
                ChannelMembers = new List<ChatMember>
                {
                    new ChatMember
                    {
                        MemberId = "zaqqwe123",
                        MemberType = ChatMemberType.CmsUser
                    }
                }
            };

            string newChannelId = await _dal.ChatData.Channels.Manager.AddOrUpdate(testChatChanel);
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager)
                .RefreshIndexAsync();

            var testMessage = new ChatMessage()
            {
                AttachedImageUrl = "kartinka",
                Content = "Updated test message",
            };

            await _dal.ChatData.Channels.Manager.UpdateLastMassage(newChannelId, testMessage);
            await ((ElasticsearchEntityManagerBase<ChatChannel>)_dal.ChatData.Channels.Manager)
                .RefreshIndexAsync();

            var channelAfterUpdate = await _dal.ChatData.Channels.Searcher.GetChannel(newChannelId);

            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatChannelIndexName);

            Assert.AreEqual(channelAfterUpdate.LastMessage.AttachedImageUrl, testMessage.AttachedImageUrl);
            Assert.AreEqual(channelAfterUpdate.LastMessage.Content, testMessage.Content);
        }
    }
}
