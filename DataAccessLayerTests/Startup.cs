﻿using DataAccessLayerImplementation;
using Elasticsearch.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

namespace DataAccessLayerTests
{
    public class Startup
    {
        public IConfiguration AppConfiguration { get; set; }

        public Startup()
        {
            AppConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true)
                .Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            ElasticDataAccessLayerSettings dalSettings = new ElasticDataAccessLayerSettings();
            IConfigurationSection dalSettingsSection = AppConfiguration.GetSection("DataAccessLayerSettings");
            dalSettingsSection.Bind(dalSettings);

            services.AddElasticLoLBoostMarketDal();

            var uri = new Uri(dalSettings.ElasticsearchNode);
            var connectionPool = new SingleNodeConnectionPool(uri, DateTimeProvider.Default);
            var connectionSettings = new ConnectionSettings(connectionPool);
            connectionSettings.BasicAuthentication(dalSettings.Login, dalSettings.Password);
            connectionSettings.RequestTimeout(TimeSpan.FromMilliseconds(dalSettings.TimeOut));

            services.AddSingleton<IElasticClient>(
                new ElasticClient(connectionSettings)
            );
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

        }
    }
}
