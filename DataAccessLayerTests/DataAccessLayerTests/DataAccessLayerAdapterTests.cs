using DataAccessLayerImplementation;
using DataAccessLayerImplementation._ElasticIndexSettings;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DataAccessLayerTests.DataAccessLayerTests
{

    [TestClass]
    [TestCategory("DataAccessLayerTests")]
    public class DataAccessLayerAdapterTests
    {
        private ElasticDataAccessLayerSettings _dataAccessLayerSettings;
        private static TestServer _testServer;

        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            _testServer = TestsHelper.InitTestServer();
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            _testServer = null;
        }

        [TestInitialize]
        public void TestInit()
        {
            var options = (IOptions<ElasticDataAccessLayerSettings>)_testServer.Services.GetService(typeof(IOptions<ElasticDataAccessLayerSettings>));

            _dataAccessLayerSettings = new ElasticDataAccessLayerSettings
            {
                ElasticsearchNode = options.Value.ElasticsearchNode,
                Login = options.Value.Login,
                Password = options.Value.Password,
                TimeOut = options.Value.TimeOut,
                IndexesSettings = new IndexesSettings
                {
                    DefaultIndexName = options.Value.IndexesSettings.DefaultIndexName,
                    MarketDataIndexSettings = options.Value.IndexesSettings.MarketDataIndexSettings,
                    MarketSystemDataIndexSettings = options.Value.IndexesSettings.MarketSystemDataIndexSettings,
                    ChatDataIndexSettings = options.Value.IndexesSettings.ChatDataIndexSettings,
                    GameDataIndexSettings = options.Value.IndexesSettings.GameDataIndexSettings
                }
            };
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _dataAccessLayerSettings = null;
        }

        [TestMethod]
        public void Test_ElasticDataAccessLayer_Ctor()
        {
            var dal = new ElasticDataAccessLayer(_dataAccessLayerSettings);
            Assert.IsNotNull(dal);
        }

        #region Check settings tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_Settings_IsNull()
        {
            _ = new ElasticDataAccessLayer(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_ElasticsearchNode_IsNull()
        {
            _dataAccessLayerSettings.ElasticsearchNode = null;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_ElasticsearchNode_IsEmpty()
        {
            _dataAccessLayerSettings.ElasticsearchNode = string.Empty;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_Login_IsNull()
        {
            _dataAccessLayerSettings.Login = null;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_Login_IsEmpty()
        {
            _dataAccessLayerSettings.Login = string.Empty;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_Password_IsNull()
        {
            _dataAccessLayerSettings.Password = null;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_Password_IsEmpty()
        {
            _dataAccessLayerSettings.Password = string.Empty;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_CheckSettingCtor_TimeOut_OutOfRange()
        {
            _dataAccessLayerSettings.TimeOut = -1;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_IndexesSettings_DefaultIndexName_IsNull()
        {
            _dataAccessLayerSettings.IndexesSettings.DefaultIndexName = null;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_CheckSettingCtor_IndexesSettings_DefaultIndexName_IsEmpty()
        {
            _dataAccessLayerSettings.IndexesSettings.DefaultIndexName = string.Empty;
            _ = new ElasticDataAccessLayer(_dataAccessLayerSettings);
        }

        #endregion
    }
}