﻿namespace DataAccessLayerTests.DataAccessLayerTests
{
    public class EEMBTestShellModel
    {
        public string Id { get; set; }
        public int TestIntField { get; set; }
        public double TestDoubleField { get; set; }
        public decimal TestDecimalField { get; set; }
        public string TestStringField { get; set; }
        public string TestIgnoringField { get; set; }
    }
}
