﻿using DataAccessLayerImplementation;
using Elasticsearch.Net;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataAccessLayerTests
{
    [TestClass]
    [TestCategory("ElasticEntityManagerBaseTests")]
    public class ElasticEntityManagerBaseTests
    {
        private const string TestIndexName = "test-eemb-index";

        private static TestServer _testServer;
        private static ElasticClient _client;

        private static EEMBTestShell _managerBase;

        [ClassInitialize]
        public static void TestClassInit(TestContext testContext)
        {
            _testServer = TestsHelper.InitTestServer();
            var options = (IOptions<ElasticDataAccessLayerSettings>)_testServer.Services.GetService(typeof(IOptions<ElasticDataAccessLayerSettings>));

            var uri = new Uri(options.Value.ElasticsearchNode);
            var connectionPool = new SingleNodeConnectionPool(uri, DateTimeProvider.Default);
            var connectionSettings = new ConnectionSettings(connectionPool);
            connectionSettings.BasicAuthentication(options.Value.Login, options.Value.Password);
            connectionSettings.RequestTimeout(TimeSpan.FromMilliseconds(options.Value.TimeOut));

            connectionSettings.DefaultMappingFor<EEMBTestShellModel>(m => m
                .IdProperty(p => p.Id)
                .Ignore(p => p.TestIgnoringField)
                .IndexName(TestIndexName)
            );

            _client = new ElasticClient(connectionSettings);
            _managerBase = new EEMBTestShell(_client, TestIndexName);
        }

        [ClassCleanup]
        public static void TestClassCleanup()
        {
            _testServer = null;
            _client = null;
            _managerBase = null;
        }

        [TestMethod]
        public async Task Test_IsIndexExist_CrateIndex_DeleteIndex_Methods()
        {
            bool indexExistingStateAtStart = await _managerBase.IsIndexExistAsyncPublic();

            await _managerBase.CreateIndexAsyncPublic();
            bool indexExistingStateAfterCreate = await _managerBase.IsIndexExistAsyncPublic();

            await _managerBase.DeleteIndexAsyncPublic();
            bool indexExistingStateAfterDelete = await _managerBase.IsIndexExistAsyncPublic();

            Assert.IsFalse(indexExistingStateAtStart);
            Assert.IsTrue(indexExistingStateAfterCreate);
            Assert.IsFalse(indexExistingStateAfterDelete);
        }

        [TestMethod]
        public async Task Test_IsDocumentExist_IndexDocument_DeleteDocument_Methods()
        {
            await _managerBase.CreateIndexAsyncPublic();

            EEMBTestShellModel testDocument = new EEMBTestShellModel
            {
                TestStringField = "testStringField",
                TestIntField = 10,
                TestDoubleField = 20.0112415566761,
                TestDecimalField = 20.0111222333m,
                TestIgnoringField = "Ignoring string"
            };

            string createdDocumentId = await _managerBase.IndexDocumentAsyncPublic(testDocument);
            bool documentExistingStateAfterCreate = await _managerBase.IsDocumentExistAsyncPublic(createdDocumentId);

            await _managerBase.DeleteDocumentAsyncPublic(createdDocumentId);
            bool documentExistingStateAfterDelete = await _managerBase.IsDocumentExistAsyncPublic(createdDocumentId);

            Assert.IsTrue(documentExistingStateAfterCreate);
            Assert.IsFalse(documentExistingStateAfterDelete);
            await _managerBase.DeleteIndexAsyncPublic();
        }

        [TestMethod]
        public async Task Test_CrateManyDocument_DeleteManyDocument_Methods()
        {
            await _managerBase.CreateIndexAsyncPublic();

            List<EEMBTestShellModel> documentsList = new List<EEMBTestShellModel>
            {
                new EEMBTestShellModel
                {
                    TestStringField = "testStringField1",
                    TestIntField = 10,
                    TestDoubleField = 20.0112415566761,
                    TestDecimalField = 20.0111222333m,
                    TestIgnoringField = "Ignoring string2"
                },

                new EEMBTestShellModel
                {
                    TestStringField = "testStringField2",
                    TestIntField = 13,
                    TestDoubleField = 22.16761,
                    TestDecimalField = 24.33m,
                    TestIgnoringField = "Ignoring string3"
                }
            };

            List<string> createdDocumentsIds = await _managerBase.IndexManyDocumentsAsyncPublic(documentsList);
            List<bool> documentsExistingStatesAfterCreate = new List<bool>();

            foreach (var id in createdDocumentsIds)
            {
                documentsExistingStatesAfterCreate.Add(
                    await _managerBase.IsDocumentExistAsyncPublic(id)
                );
            }

            documentsList[0].Id = createdDocumentsIds[0];
            documentsList[1].Id = createdDocumentsIds[1];

            await _managerBase.DeleteManyDocumentsAsyncPublic(documentsList);
            List<bool> documentsExistingStatesAfterDelete = new List<bool>();

            foreach (var id in createdDocumentsIds)
            {
                documentsExistingStatesAfterDelete.Add(
                    await _managerBase.IsDocumentExistAsyncPublic(id)
                );
            }

            Assert.AreEqual(2, createdDocumentsIds.Count);

            foreach (var state in documentsExistingStatesAfterCreate)
            {
                Assert.IsTrue(state);
            }

            foreach (var state in documentsExistingStatesAfterDelete)
            {
                Assert.IsFalse(state);
            }

            await _managerBase.DeleteIndexAsyncPublic();
        }

        [TestMethod]
        public async Task Test_RefreshIndexAsync_Methods()
        {
            EEMBTestShellModel testDocument = new EEMBTestShellModel
            {
                TestStringField = "testStringField",
                TestIntField = 10,
                TestDoubleField = 20.0112415566761,
                TestDecimalField = 20.0111222333m,
                TestIgnoringField = "Ignoring string"
            };
            string createdDocumentId = await _managerBase.IndexDocumentAsyncPublic(testDocument);
            await _managerBase.RefreshIndexAsync();
            bool documentExistingStateAfterCreate = await _managerBase.IsDocumentExistAsyncPublic(createdDocumentId);

            await _managerBase.DeleteIndexAsyncPublic();

            Assert.IsTrue(documentExistingStateAfterCreate);
        }

        #region Test params

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_IsDocumentExistAsyncMethod_DocumentIdIsNull()
        {
            await _managerBase.IsDocumentExistAsyncPublic(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_IsDocumentExistAsync_Method_DocumentIdIsEmpty()
        {
            await _managerBase.IsDocumentExistAsyncPublic(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_DeleteDocumentAsync_Method_DocumentIdIsNull()
        {
            await _managerBase.DeleteDocumentAsyncPublic(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_DeleteDocumentAsync_Method_DocumentIdIsEmpty()
        {
            await _managerBase.DeleteDocumentAsyncPublic(string.Empty);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_DeleteManyDocumentsAsync_Method_DocumentsIsNull()
        {
            await _managerBase.DeleteManyDocumentsAsyncPublic(null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public async Task Test_IndexManyDocumentsAsync_Method_DocumentsIsNull()
        {
            await _managerBase.IndexManyDocumentsAsyncPublic(null);
        }

        #endregion
    }
}