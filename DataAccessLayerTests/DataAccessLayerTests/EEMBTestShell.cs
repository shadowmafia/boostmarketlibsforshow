﻿using DataAccessLayerImplementation;
using Nest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerTests.DataAccessLayerTests
{
    public class EEMBTestShell : ElasticsearchEntityManagerBase<EEMBTestShellModel>
    {
        public EEMBTestShell(ElasticClient client, string indexName) : base(client, indexName)
        {
        }

        public async Task<bool> IsIndexExistAsyncPublic()
        {
            return await IsIndexExistAsync();
        }

        public async Task<bool> IsDocumentExistAsyncPublic(string documentId)
        {
            return await IsDocumentExistAsync(documentId);
        }

        public async Task CreateIndexAsyncPublic()
        {
            await CreateIndexAsync();
        }

        public async Task DeleteIndexAsyncPublic()
        {
            await DeleteIndexAsync();
        }

        public async Task DeleteDocumentAsyncPublic(string documentId)
        {
            await DeleteDocumentAsync(documentId);
        }

        public async Task DeleteManyDocumentsAsyncPublic(IList<EEMBTestShellModel> documents)
        {
            await DeleteManyDocumentsAsync(documents);
        }

        public async Task<string> IndexDocumentAsyncPublic(EEMBTestShellModel document)
        {
            return await IndexDocumentAsync(document);
        }

        public async Task<List<string>> IndexManyDocumentsAsyncPublic(IList<EEMBTestShellModel> documents)
        {
            return await IndexManyDocumentsAsync(documents);
        }
    }
}